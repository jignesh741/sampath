export const Constants = {
  
API_URL: "http://localhost:10000/api/",
// DICOM_VIEWER:"http://192.168.64.2/SakshamViewer/",

//API_URL: "https://intelli.ibluesys.com:20000/api/",
// API_URL: "https://saksham.ibluesysdemoserver.com:20000/api/",
// DICOM_VIEWER:"https://saksham.ibluesysdemoserver.com/SakshamViewer/",
  PAGE_ITEM_SIZE: "20"
};

const token = localStorage.getItem("token");
export const header = {
  headers: { Authorization: "bearer " + token }
};

export default Constants;
