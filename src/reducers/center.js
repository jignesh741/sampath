function center(
  state = {
    isFetching: true,
    centerAddSucess: false,
    centerAddError: false,
    centerUpdateSucess: false,
    centerUpdateError: false,
    centerList: {},
    centerManagerAddSucess: false,
    centerManagerAddError: false,
    centerManagerUpdateSucess: false,
    centerManagerUpdateError: false,
    centerManagerList:{},
    modalityUpdateSucess: false,
    billingCodes:"",
    workingCenter:"",
  },
  action
) {
  switch (action.type) {
    case "ADD_CENTER_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        centerAddError: false,
        centerAddSucess: true
      });
    case "ADD_CENTER_FAILURE_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        centerAddError: true,
        centerAddSucess: false
      });
    case "RESET_CENTER_SUCCESS_FLAG_REQUEST":
      return Object.assign({}, state, {
          centerAddSucess: false
      });
    case "GET_CENTERLIST_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        centerList: action.response
      });
   
    case "UPDATE_CENTER_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        centerUpdateSucess: true
      });
    case "RESET_CENTER_UPDATE_REQUEST":
      return Object.assign({}, state, {
        isFetching: false,
        centerUpdateSucess: false
      });
    case "UPDATE_CENTER_FAILURE_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        centerUpdateError: true
      });
    case "GET_WORKING_CENTER_REQUEST":
      console.log("data", action.data)
      return Object.assign({}, state, {
        isFetching: false,
        workingCenter: action.data
      });
////////////////center Manager///////////////////////////
      case "UPDATE_MODALITY_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          isFetching: false,
          modalityUpdateSucess: true
        });
      case "RESET_MODALITY_REQUEST":
        return Object.assign({}, state, {
          isFetching: false,
          modalityUpdateSucess: false
        });
        case "GET_BILLING_CODES_SUCCESS_ASYNC":
          return Object.assign({}, state, {
            isFetching: false,
            billingCodes: action.response
          });
    default:
      return state;
  }
}

export default center;
