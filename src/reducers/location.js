function location(
  state = {
    isFetching: true,
    locationAddSucess: false,
    locationAddError: false,
    locationList: {}
  },
  action
) {
  switch (action.type) {
    case "ADD_LOCATION_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        locationAddError: false,
        locationAddSucess: true
      });
    case "ADD_LOCATION_FAILURE_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        locationAddError: true,
        locationAddSucess: false
      });
    case "GET_LOCATIONLIST_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        locationList: action.response
      });
    case "UPDATE_LOCATION_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        locationAddError: false,
        locationUpdateSucess: true
      });
    default:
      return state;
  }
}

export default location;
