function patient(
    state = {
      isFetching: true,
      patientList: {},
      confirmed:false,
      reportData:"",
      patientAddSucess:"",
      patientAddError:"",
      patientId:"",
      pname:"",
      regId:"",
      regDispId:"",
      dispPatId:"",
    },
    action
  ) {
    switch (action.type) {
      case "SET_PATIENT_NAME_REQUEST":
        return Object.assign({}, state, {
          pname: action.pname
        });
      case "ADD_PATIENT_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          isFetching: false,
          patientAddSucess: true,
          patientId:action.response.id,
          regId: action.response.regId,
          regDispId: action.response.regDispId,
          dispPatId: action.response.dispPatId,
        });
     
      case "ADD_PATIENT_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          isFetching: false,
          patientAddSucess: true
        });
      case "RESET_PATIENT_SUCESS_REQUEST":
        return Object.assign({}, state, {
          isFetching: false,
          patientAddSucess: false
        });
      case "RESET_PATIENT_FAIL_REQUEST":
        return Object.assign({}, state, {
          isFetching: false,
          patientAddError: false
        });
      case "GET_PATIENTLIST_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          isFetching: false,
          patientList: action.response
        });
     
  
      default:
        return state;
    }
  }
  
  export default patient;
  