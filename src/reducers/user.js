function user(
  state = {
    isFetching: true,
    userAddSucess: false,
    userAddError: false,
    userUpdateSucess: false,
    userUpdateError: false,
    userList:{},
    userData:"",
  },
  action
) {
  switch (action.type) {
    
      case "ADD_USER_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          isFetching: false,
          userAddSucess: true
        });
      case "ADD_USER_FAILURE_ASYNC":
          console.log("Reducer: Error;")
        return Object.assign({}, state, {
          isFetching: false,
          userAddError: true
        });
      case "RESET_USER_SUCESS_REQUEST":
        return Object.assign({}, state, {
          isFetching: false,
          userAddSucess: false
        });
      case "UPDATE_USER_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          isFetching: false,
          userUpdateSucess: true
        });
      case "RESET_UPDATE_USER_SUCESS_REQUEST":
        return Object.assign({}, state, {
          isFetching: false,
          userUpdateSucess: false
        });
      case "RESET_UPDATE_USER_FAIL_REQUEST":
        return Object.assign({}, state, {
          isFetching: false,
          userUpdateError: false
        });
    case "GET_USER_LIST_SUCCESS_ASYNC":
        return Object.assign({}, state, {
            isFetching: false,
            userList: action.response
        });
    
    case "GET_USER_SUCCESS_ASYNC":
        return Object.assign({}, state, {
            isFetching: false,
            userData: action.response
        });
    case "RESET_USER_FAIL_REQUEST":
        return Object.assign({}, state, {
            isFetching: false,
            userAddError: false
        });
    default:
      return state;
  }
}

export default user;
