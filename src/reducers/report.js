function report(
    state = {
        saveReportTemplateSuccess: false,
        saveDraftReportSuccess: false,
        saveReportSuccess: false,
        reporttemplates: [],
    },
    action
) {
    switch (action.type) {
        case "SAVE_REPORT_TEMPLATE_REQUEST_SUCCESS_ASYNC":
            return Object.assign({}, state, {
                saveReportTemplateSuccess: true
            });
        case "RESET_REPORT_TEMPLATE_REQUEST":
            return Object.assign({}, state, {
                saveReportTemplateSuccess: false
            });

        case "SAVE_REPORT_DRAFT_SUCCESS_ASYNC":
            return Object.assign({}, state, {
                saveDraftReportSuccess: true,
            });
        case "SAVE_REPORT_SUCCESS_ASYNC":
            return Object.assign({}, state, {
                saveReportSuccess: true,
            });
        case "RESET_REPORT_DRAFT_REQUEST":
            return Object.assign({}, state, {
                saveReportSuccess: false,
                saveDraftReportSuccess: false,
            });
        case "REPORT_TEMPLATE_LIST_SUCCESS_ASYNC":
            return Object.assign({}, state, {
                reporttemplates: action.response,
            });

        default:
            return state;
    }
}

export default report;
