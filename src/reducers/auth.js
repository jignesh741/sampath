
function auth(
  state = {
    userpermissions:"",
    isFetching: false,
    isAuthenticated: localStorage.getItem("token") ? true : false  
  },
  action
) {
  switch (action.type) {
    case "LOGIN_REQUEST_ASYNC":
      return Object.assign({}, state, {
        isFetching: true,
        isAuthenticated: false,
        user: action.creds
      });
    case "LOGIN_SUCCESS_ASYNC":
    //console.log("permissions from reducer:"+ JSON.stringify(action.formdata.permissions));
      return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: true,
        errorMessage: "",
        userpermissions:action.formdata.permissions
      });
    case "LOGIN_FAILURE_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: false,
        errorMessage: action.err.message
      });
    case "LOGOUT_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: true,
        isAuthenticated: false
      });
    case "LOGOUT_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: false,
        errorMessage: ""
      });
    default:
      return state;
  }
}

export default auth;
