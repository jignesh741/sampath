function schedule(
  state = {
    scheduleSuccess: false,
    scheduleTimeSlotsSuccess: false,
    scheduleList:[],
    roomList:{},
    savedSlots:[],
    savedSlotsStore:[],
    scanningInfo:"",
    scanfinisedSuccess: false,
    imagesPushedSuccess:false,
    saveDraftReportSuccess:false,
    saveReportSuccess:false,
  },
  action
) {
  switch (action.type) {
    case "PATIENT_SCHEDULE_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        scheduleSuccess: true
      });
    case "RESET_SCHEDULE_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        scheduleSuccess: false
      });
      case "GET_SCHEDULE_LIST_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          scheduleList: action.response
        });
      case "GET_ROOM_LIST_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          roomList: action.response
        });
      case "SAVE_SLOT_IN_STORE_REQUEST":
        return Object.assign({}, state, {
          savedSlots: action.data,
          //savedSlotsStore: action.data
        });
      case "SAVE_SCANNING_INFO_IN_STORE_REQUEST":
        return Object.assign({}, state, {
          scanningInfo: action.data,
          //savedSlotsStore: action.data
        });
      case "SCHEDULE_SAVE_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          scheduleTimeSlotsSuccess: true,
          //savedSlotsStore: action.data
        });
      case "RESET_SCHEDULE_SAVE_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          scheduleTimeSlotsSuccess: false,
          //savedSlotsStore: action.data
        });
      case "STORE_SCANNING_INFO":
        return Object.assign({}, state, {
          scanningInfo: action.data,
          //savedSlotsStore: action.data
        });
      case "RESET_SAVED_SLOT":
        return Object.assign({}, state, {
          savedSlots: [],
          //savedSlotsStore: action.data
        });
      case "SCAN_FINISHED_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          scanfinisedSuccess: true,
        });
      case "IMAGES_PUSHED_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          imagesPushedSuccess: true,
        });
      case "SAVE_REPORT_DRAFT_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          saveDraftReportSuccess: true,
        });
      case "SAVE_REPORT_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          saveReportSuccess: true,
        });
      case "RESET_REPORT_DRAFT_REQUEST":
        return Object.assign({}, state, {
          saveReportSuccess: false,
          saveDraftReportSuccess: false,
        });
    default:
      return state;
  }
}

export default schedule;
