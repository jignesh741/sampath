function payment(
    state = {
      isFetching: true,
      registrations: {},
      paymentSentSuccess: false,
      error: false,
      payments:[],
      },
    action
  ) {
    switch (action.type) {
      
       case "SUBMIT_PAYMENT_REQUEST_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          paymentSentSuccess: true
        });
       case "RESET_PAYMENT_REQUEST_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          paymentSentSuccess: false
        });
       case "SUBMIT_PAYMENT_REQUEST_FAILURE_ASYNC":
        return Object.assign({}, state, {
            paymentSentSuccess: false,
            error: true
        });
       case "RESET_PAYMENT_REQUEST_FAILURE_ASYNC":
        return Object.assign({}, state, {
            paymentSentSuccess: false,
            error: false
        });
      
        case "PAYMENTS_REQUEST_SUCCESS_ASYNC":
          return Object.assign({}, state, {
            payments: action.response
          });
  
      default:
        return state;
    }
  }
  
  export default payment;
  