function factory(
  state = {
    isFetching: true,
    factoryAddSucess: false,
    factoryAddError: false,
    factoryUpdateSucess: false,
    factoryUpdateError: false,
    factoryList: {},
    factoryManagerAddSucess: false,
    factoryManagerAddError: false,
    factoryManagerUpdateSucess: false,
    factoryManagerUpdateError: false,
    factoryManagerList:{},
  },
  action
) {
  switch (action.type) {
    case "ADD_FACTORY_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        factoryAddError: false,
        factoryAddSucess: true
      });
    case "ADD_FACTORY_FAILURE_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        factoryAddError: true,
        factoryAddSucess: false
      });
    case "GET_FACTORYLIST_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        factoryList: action.response
      });
    case "UPDATE_FACTORY_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        factoryUpdateSucess: true
      });
    case "RESET_FACTORY_UPDATE_REQUEST":
      return Object.assign({}, state, {
        isFetching: false,
        factoryUpdateSucess: false
      });
    case "UPDATE_FACTORY_FAILURE_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        factoryUpdateError: true
      });
////////////////factory Manager///////////////////////////
      case "ADD_FACTORY_MANAGER_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          isFetching: false,
          factoryManagerAddSucess: true
        });
      case "ADD_FACTORY_MANAGER_FAILURE_ASYNC":
          console.log("Reducer: Error;")
        return Object.assign({}, state, {
          isFetching: false,
          factoryManagerAddError: true
        });
      case "RESET_FACTORY_MANAGER_SUCESS_REQUEST":
        return Object.assign({}, state, {
          isFetching: false,
          factoryManagerAddSucess: false
        });
      case "UPDATE_FACTORY_MANAGER_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          isFetching: false,
          factoryManagerUpdateSucess: true
        });
    case "GET_FACTORY_MANAGER_LIST_SUCCESS_ASYNC":
        return Object.assign({}, state, {
            isFetching: false,
            factoryManagerList: action.response
        });
    case "RESET_FACTORY_MANAGER_FAIL_REQUEST":
        return Object.assign({}, state, {
            isFetching: false,
            factoryManagerAddError: false
        });
    default:
      return state;
  }
}

export default factory;
