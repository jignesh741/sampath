function order(
  state = {
    orderList: [],
  },
  action
) {
  switch (action.type) {
    case "GET_ORDER_LIST_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        orderList: action.response
      });
    default:
      return state;
  }
}

export default order;
