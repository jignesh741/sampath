import { combineReducers } from "redux";

// import { reducer as formReducer } from "redux-form";
// import registrations from "./registrations";
import auth from "./auth";
import location from "./location";
import center from "./center";
import factory from "./factory";
//import executive from "./executive";
import user from "./user";
import spectrum from "./spectrum";
import patient from "./patient";
import modality from "./modality";
import billingCode from "./billingCode";
import registration from "./registration";
import payment from "./payment";
import order from "./order";
import bill from "./bill";
import schedule from "./schedule";
import pacs from "./pacs";
import report from "./report";


const reducers = combineReducers({
  auth: auth,
  location: location,
  center: center,
  factory:  factory,
  user: user,
  spectrum: spectrum,
  patient:  patient,
  modality: modality,
  billingCode: billingCode,
  registration: registration,
  payment: payment,
  order: order,
  bill: bill,
  schedule: schedule,
  pacs: pacs,
  report: report,
});

export default reducers;
