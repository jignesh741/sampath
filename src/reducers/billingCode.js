function billingCode(
  state = {
    isFetching: true,
    billingCodeAddSucess: false,
    billingCodeAddError: false,
    billingCodeUpdateSucess: false,
    billingCodeUpdateError: false,
    billingCodes: {},
    billingCodeManagerAddSucess: false,
    billingCodeManagerAddError: false,
    billingCodeManagerUpdateSucess: false,
    billingCodeManagerUpdateError: false,
    billingCodeManagerList:{},
    modalityUpdateSucess: false,
    billingCodes:"",
    workingCenter:"",
    billingCodeDeleteSucess: false,
    filteredBillingCodes:[],
    totalBillingCodePrice:0,
  },
  action
) {
  switch (action.type) {
    case "ADD_BILLING_CODE_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        billingCodeAddError: false,
        billingCodeAddSucess: true
      });
    case "ADD_BILLING_CODE_FAILURE_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        billingCodeAddError: true,
       });
    case "RESET_ADD_BILLING_CODE_REQUEST":
      return Object.assign({}, state, {
        isFetching: false,
         billingCodeAddSucess: false
      });
    case "GET_BILLING_CODES_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        billingCodes: action.response
      });
   case "UPDATE_BILLING_CODE_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        billingCodeUpdateSucess: true
      });
    case "RESET_BILLING_CODE_UPDATE_REQUEST":
      return Object.assign({}, state, {
        isFetching: false,
        billingCodeUpdateSucess: false
      });
    case "UPDATE_BILLING_CODE_FAILURE_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        billingCodeUpdateError: true
      });
    case "DELETE_BILLING_CODE_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        billingCodeDeleteSucess: true
      });
    case "RESTE_DELETE_BILLING_CODE_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        billingCodeDeleteSucess: false
      });
      case "GET_FILTERED_BILLING_CODES_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          isFetching: false,
          filteredBillingCodes: action.response
        });
      case "ADD_BIILING_CODE_PRICE":
        //console.log( "totalBillingCodePrice",  state)
        return Object.assign({}, state, {
          totalBillingCodePrice: state.totalBillingCodePrice + action.price
        });
      case "DEDUCT_BIILING_CODE_PRICE":
        return Object.assign({}, state, {
          totalBillingCodePrice: state.totalBillingCodePrice - action.price
        });
      case "RESET_BIILING_CODE_PRICE":
        return Object.assign({}, state, {
          totalBillingCodePrice: 0
        });

    default:
      return state;
  }
}

export default billingCode;
