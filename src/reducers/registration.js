function registration(
    state = {
      isFetching: true,
      registrations: {},
      sendBillSuccess: false,
      registrationSuccess: false,
      billId:"",
      orderId:"",
      regId:"",
      patientId:"",
      pname:"",
      dispBillId:"",
      dispOrderId:"",
      regDispId:"",
      },
    action
  ) {
    switch (action.type) {
      
       case "GET_REGISTRATIONS_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          isFetching: false,
          registrations: action.response
        });
       case "SEND_ORDER_BILL_REQUEST_SUCCESS_ASYNC":
        return Object.assign({}, state, {
            sendBillSuccess: true,
            billId: action.response.billId,
            orderId: action.response.orderId,
            dispBillId: action.response.dispBillId,
            dispOrderId: action.response.dispOrderId,
        });
       case "RESET_SEND_ORDER_BILL_REQUEST_SUCCESS_ASYNC":
        return Object.assign({}, state, {
            sendBillSuccess: false,
        });
        case "REGISTER_PATIENT_REQUEST_SUCCESS_ASYNC":
          return Object.assign({}, state, {
              registrationSuccess: true,              
              regId: action.response.regId,              
              regDispId: action.response.regDispId,              
              patientId: action.response.patientId,              
              dispPatId: action.response.dispPatId,              
              pname: action.response.pname,              
          });
        case "RESET_REGISTER_PATIENT_REQUEST":
          return Object.assign({}, state, {
              registrationSuccess: false,              
          });
  
      default:
        return state;
    }
  }
  
  export default registration;
  