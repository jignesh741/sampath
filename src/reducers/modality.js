function modality(
  state = {
    isFetching: true,
    modalityAddSucess: false,
    modalityAddError: false,
    modalityUpdateSucess: false,
    modalityUpdateError: false,
    modalityList: {},
    modalityUpdateSucess: false,
    modalitySettingsSucess: false,
    modalitySettingsError: false,
  },
  action
) {
  switch (action.type) {
    case "ADD_MODALITY_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,       
        modalityAddSucess: true
      });
    case "RESET_ADD_MODALITY_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,  
        modalityAddSucess: false
      });
    case "ADD_MODALITY_FAILURE_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        modalityAddError: true,
       
      });
    case "RESET_ADD_MODALITY_FAILURE_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        modalityAddError: false,
      });
    case "MODALITY_SETTINGS_REQUEST_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        modalitySettingsSucess: true,
       
      });
    case "RESET_MODALITY_SETTINGS_REQUEST_SUCCESS":
      return Object.assign({}, state, {
        isFetching: false,
        modalitySettingsSucess: false,
      });
    case "RESET_MODALITY_SETTINGS_FLAG_REQUEST":
      return Object.assign({}, state, {
        modalitySettingsSucess: false,
      });
    case "MODALITY_SETTINGS_REQUEST_FAILURE_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        modalitySettingsError: true,
       
      });
    case "RESET_MODALITY_SETTINGS_REQUEST_FAILURE":
      return Object.assign({}, state, {
        isFetching: false,
        modalitySettingsError: false,
      });
    case "GET_MODALITYLIST_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        modalityList: action.response
      });
    case "UPDATE_MODALITY_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        modalityUpdateSucess: true
      });
    case "RESET_MODALITY_UPDATE_REQUEST":
      return Object.assign({}, state, {
        isFetching: false,
        modalityUpdateSucess: false
      });
    case "UPDATE_MODALITY_FAILURE_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        modalityUpdateError: true
      });
    case "RESET_MODALITY_UPDATE_FAIL_REQUEST":
      return Object.assign({}, state, {
        isFetching: false,
        modalityUpdateError: false
      });

     
    default:
      return state;
  }
}

export default modality;
