function pacs(
  state = {
    pacsList:[],
   
  },
  action
) {
  switch (action.type) {
    case "GET_PACS_LIST_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        pacsList: action.response
      });

    default:
      return state;
  }
}

export default pacs;
