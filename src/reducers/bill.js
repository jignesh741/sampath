function bill(
  state = {
    bills: [],
  },
  action
) {
  switch (action.type) {
    case "BILLS_REQUEST_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        bills: action.response
      });
    default:
      return state;
  }
}

export default bill;
