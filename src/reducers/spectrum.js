function spectrum(
  state = {
    isFetching: true,
    spectrumAddSucess: false,
    spectrumAddError: false,
    spectrumUpdateSucess: false,
    spectrumUpdateError: false,
    spectrumList:"",
    spectrumData:"",
    spectrumTestList:"",
    spectrumDataRequestSucess: false,
    spectrumParameterReport:"",
    normalranges:"",
  },
  action
) {
  switch (action.type) {
    
      case "ADD_SPECTRUM_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          isFetching: false,
          spectrumAddSucess: true
        });
      case "ADD_SPECTRUM_FAILURE_ASYNC":
          console.log("Reducer: Error;")
        return Object.assign({}, state, {
          isFetching: false,
          spectrumAddError: true
        });
      case "RESET_SPECTRUM_SUCESS_REQUEST":
        return Object.assign({}, state, {
          isFetching: false,
          spectrumAddSucess: false
        });
      case "GET_SPECTRUM_DATA_REQUEST_SUCCESS_ASYNC":
         // console.log("Reducer: Error;")
        return Object.assign({}, state, {
          isFetching: false,
          spectrumDataRequestSucess: true
        });
      case "RESET_GET_SPECTRUM_DATA_REQUEST_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          isFetching: false,
          spectrumDataRequestSucess: false
        });
      case "UPDATE_SPECTRUM_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          isFetching: false,
          spectrumUpdateSucess: true
        });
      case "RESET_UPDATE_SPECTRUM_SUCESS_REQUEST":
        return Object.assign({}, state, {
          isFetching: false,
          spectrumUpdateSucess: false
        });
      case "RESET_UPDATE_SPECTRUM_FAIL_REQUEST":
        return Object.assign({}, state, {
          isFetching: false,
          spectrumUpdateError: false
        });
    case "GET_SPECTRUM_LIST_SUCCESS_ASYNC":
        return Object.assign({}, state, {
            isFetching: false,
            spectrumList: action.response
        });
    case "GET_SPECTRUM_TEST_LIST_SUCCESS_ASYNC":
        return Object.assign({}, state, {
            isFetching: false,
            spectrumTestList: action.response
        });
    
    case "GET_SPECTRUM_SUCCESS_ASYNC":
        return Object.assign({}, state, {
            isFetching: false,
            spectrumData: action.response
        });
    case "RESET_SPECTRUM_FAIL_REQUEST":
        return Object.assign({}, state, {
            isFetching: false,
            spectrumAddError: false
        });
    case "GET_SPECTRUM_PARAM_REQUEST_SUCCESS_ASYNC":
        return Object.assign({}, state, {
            isFetching: false,
            spectrumParameterReport: action.response
        });
    case "GET_NORMAL_RANGES_SUCCESS_ASYNC":
        return Object.assign({}, state, {
            isFetching: false,
            normalranges: action.response
        });
    default:
      return state;
  }
}

export default spectrum;
