function executive(
  state = {
    isFetching: true,
    executiveAddSucess: false,
    executiveAddError: false,
    executiveUpdateSucess: false,
    executiveUpdateError: false,
    executiveList:{},
    executiveData:"",
  },
  action
) {
  switch (action.type) {
    
      case "ADD_EXECUTIVE_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          isFetching: false,
          executiveAddSucess: true
        });
      case "ADD_EXECUTIVE_FAILURE_ASYNC":
          console.log("Reducer: Error;")
        return Object.assign({}, state, {
          isFetching: false,
          executiveAddError: true
        });
      case "RESET_EXECUTIVE_SUCESS_REQUEST":
        return Object.assign({}, state, {
          isFetching: false,
          executiveAddSucess: false
        });
      case "UPDATE_EXECUTIVE_SUCCESS_ASYNC":
        return Object.assign({}, state, {
          isFetching: false,
          executiveUpdateSucess: true
        });
      case "RESET_UPDATE_EXECUTIVE_SUCESS_REQUEST":
        return Object.assign({}, state, {
          isFetching: false,
          executiveUpdateSucess: false
        });
      case "RESET_UPDATE_EXECUTIVE_FAIL_REQUEST":
        return Object.assign({}, state, {
          isFetching: false,
          executiveUpdateError: false
        });
    case "GET_EXECUTIVE_LIST_SUCCESS_ASYNC":
        return Object.assign({}, state, {
            isFetching: false,
            executiveList: action.response
        });
    
    case "GET_EXECUTIVE_SUCCESS_ASYNC":
        return Object.assign({}, state, {
            isFetching: false,
            executiveData: action.response
        });
    case "RESET_EXECUTIVE_FAIL_REQUEST":
        return Object.assign({}, state, {
            isFetching: false,
            executiveAddError: false
        });
    default:
      return state;
  }
}

export default executive;
