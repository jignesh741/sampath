function employee(
  state = {
    isFetching: true,
    employeeAddSucess: false,
    employeeAddError: false,
    employeeList: {},
    employeeListFilter:{},
    managerList:{},
    meetingAllowedEmployeeList:{},
    employee: false,
    passwordUpdatSucess:false,
    fileDownloadCount:false,
    allowMeetingResponse:false,
    inductionList:{},
    set:{}
  },
  action
) {
  switch (action.type) {
    case "ADD_EMPLOYEE_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        employeeAddError: false,
        employeeAddSucess: true
      });
    case "ADD_EMPLOYEE_FAILURE_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        employeeAddError: true,
        employeeAddSucess: false
      });
    case "GET_EMPLOYEELIST_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        employeeList: action.response
      });
    case "GET_MEETING_ALLOWED_EMPLOYEE_LIST_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        meetingAllowedEmployeeList: action.response
      });
    case "GET_EMPLOYEELIST_FILTER_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        employeeListFilter: action.response
      });
    case "UPDATE_EMPLOYEE_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        employeeUpdatError: false,
        employeeUpdatSucess: true
      });
    case "RESET_UPDATE_EMPLOYEE_SUCCESS_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        employeeUpdatError: false,
        employeeUpdatSucess: false
      });
    case "RESET_ADD_SUCCESS_FLAG":
      return Object.assign({}, state, {
        employeeAddSucess: false
      });
    case "GET_EMPLOYEE_SUCCESS_ASYNC":
       return Object.assign({}, state, {
        isFetching: false,
        employee: action.response
      });
    case "GET_EMPLOYEE_FAILURE_ASYNC":
      return Object.assign({}, state, {
        isFetching: false,
        employee: false
      });
    case "GET_MANAGERLIST_SUCCESS_ASYNC":
    return Object.assign({}, state, {
      isFetching: false,
      managerList: action.response
    });
    case "UPDATE_PASSWORD_SUCCESS_ASYNC":
    return Object.assign({}, state, {
      passwordUpdatSucess: true
    });
    case "UPDATE_EMPLOYEE_INDUCTION_SUCCESS_ASYNC":
    return Object.assign({}, state, {
      fileDownloadCount: true
    });
    case "UPDATE_EMPLOYEE_ALLOW_MEETING_SUCCESS_ASYNC":
    return Object.assign({}, state, {
      allowMeetingResponse: true
    });
    case "RESET_EMPLOYEE_ALLOW_MEETING_SUCCESS_ASYNC":
    return Object.assign({}, state, {
      allowMeeting: false
    });
    case "GET_INDUCTION_SUCCESS_ASYNC":
    return Object.assign({}, state, {
      inductionList: action.response
    });
    case "UPDATE_TRAINING_SUCCESS_ASYNC":
    return Object.assign({}, state, {
      set: {set1:true}
    });
    case "GET_TRAINING_SET_SUCCESS_ASYNC":
    return Object.assign({}, state, {
      set: action.response
    });
    default:
      return state;
  }
}

export default employee;
