import React, { Component } from "react";
import { Redirect, Route,Link } from "react-router-dom";
import {
  Badge,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  NavItem,
  NavLink
} from "reactstrap";
import PropTypes from "prop-types";

import {
  AppAsideToggler,
  AppHeaderDropdown,
  AppNavbarBrand,
  AppSidebarToggler
} from "@coreui/react";
import logo from "../../assets/img/logo.png";
import sygnet from "../../assets/img/brand/sygnet.svg";
import { locale } from "core-js";
import { connect } from "react-redux";

const propTypes = {
  children: PropTypes.node
};


const defaultProps = {};

class DefaultHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      userid:""
    }
  }
  handleLogout = e => {
    
    localStorage.clear();
    this.props.LogoutRequest();
  };
  
  componentDidUpdate(prevProps) {
    if (this.props.authenticated) {
      return <Redirect to="/login" />;
    }
  }
componentDidMount() {
  this.setState({username:localStorage.getItem("fname")+" "+localStorage.getItem("lname"), roles: localStorage.getItem("roles")});
}


  render() {
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;
    const {userid} = this.state;
    const profileLink = "/masters/updateProfile?id="+userid;
    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand style={{padding:"0", margin:"0"}}
          full={{ src: "/ris/assets/img/logo.png", width: 50, height: 50, alt: "" }}
          minimized={{ src: "/ris/assets/img/logo.png", width: 50, height: 50, alt: "" }}
        />
        <h3 href="/" className="ml-2 text-primary ">
          intelli <small className="text-secondary">PACS</small>
        </h3>
        <AppSidebarToggler className="d-md-down-none " display="lg" />

        {/* <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink href="/">Dashboard</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <NavLink href="/users">Users</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <NavLink href="#">Settings</NavLink>
          </NavItem>
        </Nav> */}
        <Nav className="ml-auto " navbar>
          {/* <NavItem className="d-md-down-none">
            <NavLink href="#">
              <i className="icon-bell" />
              <Badge pill color="danger">
                5
              </Badge>
            </NavLink>
          </NavItem> 
          <NavItem className="d-md-down-none">
            <NavLink href="#">
              <i className="icon-list" />
            </NavLink>
          </NavItem>*/}
          <NavItem className="d-md-down-none">
            <NavLink href="#" className="text-white">
              {/* <i className="icon-location-pin" /> */}
              Hi {this.state.username}
            </NavLink>
          </NavItem>
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <img
                src={"/ris/favicon.png"}
                className="img-avatar"
                alt=""
              />
            </DropdownToggle>
            <DropdownMenu right style={{ right: "auto" }}>
              { this.state.roles == "administrator" && 
              <React.Fragment>
              <DropdownItem header tag="div" className="text-center">
                <strong>Main Settings</strong>
              </DropdownItem>
              <DropdownItem>
              <NavLink href="/ris/#/center"> <i className="fa fa-wrench" />Center</NavLink>
              </DropdownItem>
              <DropdownItem>
              <NavLink href="/ris/#/userlist" exact="true"> <i className="fa fa-user" /> User List</NavLink>
              </DropdownItem>
              <DropdownItem>
              <NavLink href="/ris/#/users" exact="true"> <i className="fa fa-user" /> Users: Add User</NavLink>
              </DropdownItem>
              </React.Fragment>
              }
               <DropdownItem onClick={this.handleLogout}>
                <i className="fa fa-lock" /> Logout
              </DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>
        {/* <AppAsideToggler className="d-md-down-none" /> */}
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

const mapStateToProps = state => {
  return {
    authenticated: state.auth.isAuthenticated,
    errorMessage: state.auth.errorMessage
  };
};

const mapDispachToProps = dispatch => {
  return {
    LogoutRequest: () => dispatch({ type: "LOGOUT_REQUEST" })
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(DefaultHeader);
