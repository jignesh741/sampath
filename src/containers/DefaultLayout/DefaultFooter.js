import React, { Component } from "react";
import PropTypes from "prop-types";

const propTypes = {
  children: PropTypes.node
};

const defaultProps = {};

class DefaultFooter extends Component {
  render() {
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <div className="container">
        <div className="row justify-content-between">
          <div className="col-6"><small>&copy; 2020 intelli PACS </small></div>
          {/* <div className="col-6 text-right"><small>Maintained By: IBLUESYS COMPUTECH PVT. LTD.</small></div> */}
        </div>
        </div>
      </React.Fragment>
    );
  }
}

DefaultFooter.propTypes = propTypes;
DefaultFooter.defaultProps = defaultProps;

export default DefaultFooter;
