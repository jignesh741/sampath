import React, { Component } from "react";
import { Redirect, Link } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Alert
} from "reactstrap";
import { connect } from "react-redux";
import bg from "../../../assets/img/bg7.jpg";

class AdminLogin extends Component {
  constructor(props) {
    super(props);
    if (this.props.authenticated) {
      this.props.history.push("/dashboard");
    }
    this.state = {
      username: "",
      password: ""
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    const formdata = {
      username: this.state.username,
      password: this.state.password
    };
    this.props.LoginRequest(formdata);
  };
  componentDidUpdate(prevProps) {
    if (this.props.authenticated) {
      this.props.history.push("/locations");
    }
  }

  render() {
    const { LoginRequest, authenticated, errorMessage } = this.props;

    if (this.props.authenticated) return <Redirect to="/dashboard" />;

    return (
      <div className="app flex-row align-items-center loginpage">
        <div className="loginpageLogo fixed-top mt-5px" fixedtop />
        <Container>
          <Row className="justify-content-left">
            <Col md="4">
              <CardGroup>
                <Card className="p-1 shadow-lg">
                  <CardBody>
                    {errorMessage ? (
                      <Alert color="danger">
                        Invalid Credentials — Please try again!
                      </Alert>
                    ) : null}
                    <Form onSubmit={this.handleSubmit}>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="text"
                          placeholder="Username"
                          autoComplete="username"
                          name="username"
                          onChange={this.handleChange}
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          placeholder="Password"
                          autoComplete="current-password"
                          name="password"
                          onChange={this.handleChange}
                        />
                      </InputGroup>
                      <Row>
                        <Col>
                          <Button block color="danger" className="px-4">
                            Login
                          </Button>
                        </Col>
                      </Row>

                      {/* <Row>
                        <Col className="text-center">
                          <Button color="link" className="px-0">
                            Forgot password?
                          </Button>
                        </Col>
                      </Row> */}
                    </Form>
                  </CardBody>
                </Card>
                {/* <Card
                  className="text-white bg-primary py-5 d-md-down-none"
                  style={{ width: "44%" }}
                >
                  <CardBody className="text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua.
                      </p>
                      <Link to="/register">
                        <Button
                          color="primary"
                          className="mt-3"
                          active
                          tabIndex={-1}
                        >
                          Register Now!
                        </Button>
                      </Link>
                    </div>
                  </CardBody>
                </Card> */}
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    authenticated: state.auth.isAuthenticated,
    errorMessage: state.auth.errorMessage
  };
};

const mapDispachToProps = dispatch => {
  return {
    LoginRequest: data => dispatch({ type: "ADMIN_LOGIN_REQUEST", formdata: data })
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(Login);
