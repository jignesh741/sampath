import React, { Component } from "react";
import { Redirect, Link } from "react-router-dom";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Alert
} from "reactstrap";
import { connect } from "react-redux";
import logo from "../../../assets/img/logo-login.png";
//import logo from "/favicon.png";

class Login extends Component {
  constructor(props) {
    super(props);
    if (this.props.authenticated) {
      this.props.history.push("/");
    }
    this.state = {
      username: "",
      password: ""
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    const formdata = {
      username: this.state.username,
      password: this.state.password
    };
    this.props.LoginRequest(formdata);
  };
  componentDidUpdate(prevProps) {
    if (this.props.authenticated) {
      this.props.history.push("/");
    }
  }

  render() {
    const { LoginRequest, authenticated, errorMessage } = this.props;

    if (this.props.authenticated) return <Redirect to="/center" />;

    return (
      <div className="app flex-row align-items-center loginpage">
        <div className=" fixed-top mt-5px" fixedtop />
        
       
        <Container>
          {/* <Row className="justify-content-center ">
            <Col md="1" sm="12">
            <div className=""><img src={logo} className="rounded-circle" style={{height:"200px", width:"300"}}/></div>
            </Col>
          </Row> */}
          <Row className="justify-content-center ">
            {/* <Col md="1" className="ml-4"> </Col> */}          
            <Col md="5" sm="12">
              <CardGroup>
                <div className="border p-2 w-100" > 
                <Card className=" border bg-white mb-0">
                <CardHeader style={{backgroundColor:"#E4E5E5"}}>
                  <div className="d-flex align-items-center" style={{backgroundColor: "#E4E5E5"}} > 
                    <div className=""><img src="/ris/assets/img/logo.png" className="img-thumbnail rounded-circle" style={{height:"65px"}}/></div>
                    {/* <div className="mt-1"><img src="/assets/img/logo-name.png" className="" style={{height:"30px"}}/></div>  */}
                    <div className="align-baseline ml-1" ><h1 className="ml-1" style={{color:"#2E61A1"}} >Intelli <small style={{fontSize:"20px"}}>PACS</small></h1></div>
                  </div>
                
                </CardHeader>
                  <CardBody>
                    {errorMessage ? (
                      <Alert color="danger">
                        Invalid Credentials — Please try again!
                      </Alert>
                    ) : null}
                    <Form onSubmit={this.handleSubmit}>
                      
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="text"
                          placeholder="Email Id"
                          autoComplete="username"
                          name="username"
                          onChange={this.handleChange}
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          placeholder="Password"
                          autoComplete="current-password"
                          name="password"
                          onChange={this.handleChange}
                        />
                      </InputGroup>
                      <Row>
                        <Col>
                          <Button block color="purple" className="px-4 bg-blue font-weight-bold">
                            LOGIN
                          </Button>
                        </Col>
                      </Row>

                      {/* <Row>
                        <Col className="text-center">
                          <Button color="link" className="px-0">
                            Forgot password?
                          </Button>
                        </Col>
                      </Row> */}
                    </Form>
                  </CardBody>
                </Card>
                </div>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    authenticated: state.auth.isAuthenticated,
    errorMessage: state.auth.errorMessage
  };
};

const mapDispachToProps = dispatch => {
  return {
    LoginRequest: data => dispatch({ type: "LOGIN_REQUEST", formdata: data })
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(Login);
