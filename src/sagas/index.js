import { takeLatest } from "redux-saga";
import { fork, all } from "redux-saga/effects";

import { watchLoginRequest } from "./auth";
import { watchLogoutRequest } from "./logout";

import {
  watchAddLocationRequest,
  watchgetLocationListRequest,
  watchUpdateLocationRequest,
  watchDeleteLocationRequest
} from "./location";
import {
  watchAddCenterRequest,
  watchgetCenterListRequest,
  watchUpdateCenterRequest,
  watchDeleteCenterRequest,
  watchAddCenterManagerRequest,
  watchUpdateCenterManagerRequest,
  watchgetCenterManagerListRequest,
  watchUpdateCenterModalityRequest,
 } from "./center";
import {
  watchAddModalityRequest,
  watchgetModalityListRequest,
  watchUpdateModalityRequest,
  watchDeleteModalityRequest,
  watchSendModalitySettingsRequest,
 } from "./modality";

import {
  watchAddUserRequest,
  watchUpdateUserRequest,
  watchgetUserListRequest,
  watchgetUserRequest,
} from "./users";

import {
  watchgetregistrationsRequest,
  watchsendOrdeBillRequest,
  watchRegisterPatientRequest
} from "./registration";

import {
  watchAddBillingCodeRequest,
  watchgetBillingCodesRequest,
  watchUpdateBillingCodeRequest,
  watchDeleteBillingCodeRequest,
  watchgetFilteredBillingCodesRequest
} from "./billingCode"

import {
  watchAddPatientRequest,
  watchgetPatientListRequest,
  watchgetConfirmRequest,
  watchgetReportDatarequest,
  watchgetPatientFilterListRequest
} from "./patient";
import {
  watchsubmitPaymentRequest,
  watchPaymentsRequest,
} from "./payment";
import {
  watchBillsRequest,
} from "./bill";

import {
  watchgetOrderListRequest,
} from "./order";
import {
  watchPACSListRequest,
} from "./pacs";
import {
  watchsaveReportTemplateRequest,
  watchSaveDraftReportRequest,
  watchSaveReportRequest ,
  watchreportTemplateListRequest,
} from "./report";

import {
  watchScheduleRequest,
  watchScheduleListRequest,
  watchRoomListRequest,
  watchSaveScheduleRequest,
  watchScanFinishedRequest,
  watchImagesPushedRequest,

} from "./schedule";


export function* rootSaga() {
  yield [
    fork(watchLoginRequest),
    fork(watchLogoutRequest),
    fork(watchAddLocationRequest),
    fork(watchgetLocationListRequest),
    fork(watchUpdateLocationRequest),
    fork(watchDeleteLocationRequest),

    fork(watchAddCenterRequest),
    fork(watchgetCenterListRequest),
    fork(watchUpdateCenterRequest),
    fork(watchDeleteCenterRequest),
    fork(watchAddCenterManagerRequest),
    fork(watchUpdateCenterManagerRequest),
    fork(watchgetCenterManagerListRequest),
    fork(watchUpdateCenterModalityRequest),

    
    
    fork(watchAddUserRequest),
    fork(watchUpdateUserRequest),
    fork(watchgetUserListRequest),
    fork(watchgetUserRequest),

   
    fork(watchAddPatientRequest),
    fork(watchgetPatientListRequest),
    fork(watchgetConfirmRequest),
    fork(watchgetReportDatarequest),
    fork(watchgetPatientFilterListRequest),

    fork(watchAddModalityRequest),
    fork(watchSendModalitySettingsRequest),
    fork(watchgetModalityListRequest),
    fork(watchUpdateModalityRequest),
    fork(watchDeleteModalityRequest),

    fork(watchgetregistrationsRequest),
    fork(watchAddBillingCodeRequest),
    fork(watchgetBillingCodesRequest),
    fork(watchUpdateBillingCodeRequest),
    fork(watchDeleteBillingCodeRequest),
    fork(watchgetFilteredBillingCodesRequest),
    
    fork(watchsendOrdeBillRequest),
    fork(watchRegisterPatientRequest),
    
    fork(watchsubmitPaymentRequest),
    fork(watchgetOrderListRequest),
    fork(watchBillsRequest),
    fork(watchPaymentsRequest),
    
    fork(watchScheduleRequest),
    fork(watchScheduleListRequest),
    fork(watchRoomListRequest),
    fork(watchSaveScheduleRequest),
    fork(watchScanFinishedRequest),
    fork(watchImagesPushedRequest),
    fork(watchSaveDraftReportRequest),
    fork(watchSaveReportRequest),
    
    fork(watchPACSListRequest),
    fork(watchsaveReportTemplateRequest),
    fork(watchreportTemplateListRequest),

  ];
}
