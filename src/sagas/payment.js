import axios from "axios";
import { delay } from "redux-saga";
import Constants from "../config";
import { takeLatest, put, call } from "redux-saga/effects";



const center = localStorage.getItem("center");

const apiUrl = Constants.API_URL;

/////////////////bof Submit Payment Request //////////////////////////////

const sendPaymentRequestAPI = data => {
  return axios
    .post(
      apiUrl + "payment/submitPayment",
      { formdata: data.formdata},
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* sendPaymentRequestAsync(data) {
  try {
    const result = yield call(sendPaymentRequestAPI, data);
    yield put({ type: "SUBMIT_PAYMENT_REQUEST_SUCCESS_ASYNC", response: result });
     } catch (err) {
    yield put({ type: "SUBMIT_PAYMENT_REQUEST_FAILURE_ASYNC", err });
  }
}

export function* watchsubmitPaymentRequest(data) {
  yield takeLatest("SUBMIT_PAYMENT_REQUEST", sendPaymentRequestAsync);
}
/////////////////eof Submit Payment Request //////////////////////////////

const sendPaymentsRequestAPI = data => {
  console.log("Payment Saga", data.formdata)
return axios
  .get(
    apiUrl + "payment/payments",
    { params: data.formdata},
    {
      headers: {
        Accept: "application/json"
      }
    }
  )
  .then(response => response.data)
  .catch(err => {
    throw err;
  });
};

function* sendPaymeantsRequestAsync(data) {
try {
  const result = yield call(sendPaymentsRequestAPI, data);
  yield put({ type: "PAYMENTS_REQUEST_SUCCESS_ASYNC", response: result });
   } catch (err) {
  yield put({ type: "PAYMENTS_REQUEST_FAILURE_ASYNC", err });
}
}

export function* watchPaymentsRequest(data) {
yield takeLatest("PAYMENTS_REQUEST", sendPaymeantsRequestAsync);
}