import axios from "axios";
import { delay } from "redux-saga";
import Constants from "../config";
import { takeLatest, put, call } from "redux-saga/effects";

const apiUrl = Constants.API_URL;

const addLocationRequestAPI = data => {
  return axios
    .post(
      apiUrl + "location/addLocation",
      { location: data.formdata.location, locationIn:data.formdata.locationIn, },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* addLocationRequestAsync(data) {
  try {
    const result = yield call(addLocationRequestAPI, data);
    yield put({ type: "ADD_LOCATION_SUCCESS_ASYNC", response: result });
    yield put({ type: "GET_LOCATIONLIST_REQUEST" });
  } catch (err) {
    yield put({ type: "ADD_LOCATION_FAILURE_ASYNC", err });
  }
}

export function* watchAddLocationRequest(data) {
  yield takeLatest("ADD_LOCATION_REQUEST", addLocationRequestAsync);
}

/////////////////Get Bonustype List GET_BONUSTYPELIST_REQUEST //////////////////////////////

const getLocationListRequestAPI = data => {
  console.log("Data", data);
  return axios
    .get(apiUrl + "location/locationlist?locationIn="+data.locationIn, {
      headers: {
        Accept: "application/json"
      }
    })
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* getLocationListRequestAsync(data) {
  try {
    const result = yield call(getLocationListRequestAPI, data);
    yield put({ type: "GET_LOCATIONLIST_SUCCESS_ASYNC", response: result });
  } catch (err) {
    yield put({ type: "GET_LOCATIONLIST_FAILURE_ASYNC", err });
  }
}

export function* watchgetLocationListRequest(data) {
  yield takeLatest("GET_LOCATIONLIST_REQUEST", getLocationListRequestAsync);
}
////////////update Location/////////////////////////////

const updateLocationRequestAPI = data => {
  return axios
    .post(
      apiUrl + "location/putLocation",
      { formdata: data.formdata },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* updateLocationRequestAsync(data) {
  try {
    const result = yield call(updateLocationRequestAPI, data);
    yield put({ type: "UPDATE_LOCATION_SUCCESS_ASYNC", response: result });
    yield put({ type: "GET_LOCATIONLIST_REQUEST" });
  } catch (err) {
    yield put({ type: "UPDATE_LOCATION_FAILURE_ASYNC", err });
  }
}

export function* watchUpdateLocationRequest(data) {
  yield takeLatest("UPDATE_LOCATION_REQUEST", updateLocationRequestAsync);
}

////////////Delete Location/////////////////////////////

const deleteLocationRequestAPI = data => {
  return axios
    .post(
      apiUrl + "location/deleteLocation",
      { id: data.id },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* deleteLocationRequestAsync(data) {
  try {
    const result = yield call(deleteLocationRequestAPI, data);
    yield put({ type: "DELETE_LOCATION_SUCCESS_ASYNC", response: result });
    yield put({ type: "GET_LOCATIONLIST_REQUEST" });
  } catch (err) {
    yield put({ type: "UPDATE_LOCATION_FAILURE_ASYNC", err });
  }
}

export function* watchDeleteLocationRequest(data) {
  yield takeLatest("DELETE_LOCATION_REQUEST", deleteLocationRequestAsync);
}
