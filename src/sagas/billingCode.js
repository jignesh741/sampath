import axios from "axios";
import { delay } from "redux-saga";
import Constants from "../config";
import { takeLatest, put, call } from "redux-saga/effects";

const apiUrl = Constants.API_URL;

///////adding billlingCode///////////////////////////////////

const AddBillingCodeRequestAPI = data => {
    return axios
      .post(
        apiUrl + "billingCode/addBillingCode",
        { formdata: data.formdata },
        {
          headers: {
            Accept: "application/json"
          }
        }
      )
      .then(response => response.data)
      .catch(err => {
        throw err;
      });
  };
  
  function* AddBillingCodeRequestAsync(data) {
    try {
      const result = yield call(AddBillingCodeRequestAPI, data);
      yield put({ type: "ADD_BILLING_CODE_SUCCESS_ASYNC", response: result });
     // yield put({ type: "GET_BILLING_CODES_REQUEST" });
    } catch (err) {
      yield put({ type: "ADD_BILLING_CODE_FAILURE_ASYNC", err });
    }
  }
  
  export function* watchAddBillingCodeRequest(data) {
    yield takeLatest("ADD_BILLING_CODE_REQUEST", AddBillingCodeRequestAsync);
  }
  ///////////adding billingcode end////////////////////////////////
const getBillingCodesRequestAPI = data => {
   
  return axios
    .get(apiUrl + "billingCode/getBillingCodes?centerId="+data.data, {
      headers: {
        Accept: "application/json"
      }
    })
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* getBillingCodesRequestAsync(data) {
  try {
    const result = yield call(getBillingCodesRequestAPI, data);
    yield put({ type: "GET_BILLING_CODES_SUCCESS_ASYNC", response: result });
  } catch (err) {
    yield put({ type: "GET_BILLING_CODES_FAILURE_ASYNC", err });
  }
}

export function* watchgetBillingCodesRequest(data) {
  yield takeLatest("GET_BILLING_CODES_REQUEST", getBillingCodesRequestAsync);
}
////////////////////beof Filtered billing codes for auto complete box/////////////////
const getFilteredBillingCodesRequestAPI = data => {
   
  return axios
    .get(apiUrl + "billingCode/getFilteredBillingCodes", {
      params: {data: data.data},
      headers: {
        Accept: "application/json"
      }
    })
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* getFilteredBillingCodesRequestAsync(data) {
  try {
    const result = yield call(getFilteredBillingCodesRequestAPI, data);
    yield put({ type: "GET_FILTERED_BILLING_CODES_SUCCESS_ASYNC", response: result });
  } catch (err) {
    yield put({ type: "GET_FILTERED_BILLING_CODES_FAILURE_ASYNC", err });
  }
}

export function* watchgetFilteredBillingCodesRequest(data) {
  yield takeLatest("GET_FILTERED_BILLING_CODES_REQUEST", getFilteredBillingCodesRequestAsync);
}
////////////////////eof Filtered billing codes for auto complete box/////////////////
////////////update BillingCode/////////////////////////////

const updateBillingCodeRequestAPI = data => {
  return axios
    .post(
      apiUrl + "billingCode/putBillingCode",
      { formdata: data.formdata },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* updateBillingCodeRequestAsync(data) {
  try {
    const result = yield call(updateBillingCodeRequestAPI, data);
    yield put({ type: "UPDATE_BILLING_CODE_SUCCESS_ASYNC", response: result });
    yield put({ type: "GET_BILLING_CODES_REQUEST", data: this.props.workingCenter });
  } catch (err) {
    yield put({ type: "UPDATE_BILLING_CODE_FAILURE_ASYNC", err });
  }
}

export function* watchUpdateBillingCodeRequest(data) {
  yield takeLatest("UPDATE_BILLING_CODE_REQUEST", updateBillingCodeRequestAsync);
}

////////////Delete BillingCode/////////////////////////////

const deleteBillingCodeRequestAPI = data => {
  return axios
    .post(
      apiUrl + "billingCode/deleteBillingCode",
      { id: data.id },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* deleteBillingCodeRequestAsync(data) {
  try {
    const result = yield call(deleteBillingCodeRequestAPI, data);
    yield put({ type: "DELETE_BILLING_CODE_SUCCESS_ASYNC", response: result });
    yield put({ type: "GET_BILLING_CODES_REQUEST", data: this.props.workingCenter });
  } catch (err) {
    yield put({ type: "UPDATE_BILLING_CODE_FAILURE_ASYNC", err });
  }
}

export function* watchDeleteBillingCodeRequest(data) {
  yield takeLatest("DELETE_BILLING_CODE_REQUEST", deleteBillingCodeRequestAsync);
}

