import axios from "axios";
import { delay } from "redux-saga";
import { Constants, header } from "../config";
import { takeLatest, put, call } from "redux-saga/effects";

const apiUrl = Constants.API_URL;

const addEmployeeRequestAPI = data => {
  return axios
    .post(
      apiUrl + "employee/addEmployee",
      { employee: data.formdata },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* addEmployeeRequestAsync(data) {
  try {
    const result = yield call(addEmployeeRequestAPI, data);
    yield put({ type: "ADD_EMPLOYEE_SUCCESS_ASYNC", response: result });
  } catch (err) {
    yield put({ type: "ADD_EMPLOYEE_FAILURE_ASYNC", err });
  }
}

export function* watchAddEmployeeRequest(data) {
  yield takeLatest("ADD_EMPLOYEE_REQUEST", addEmployeeRequestAsync);
}

//////////////////////////////////////////////////////////////////////////
/////////////////get employ by id////////////////////////////////
const getEmployeeRequestAPI = data => {
  
  return axios
    .post(
      apiUrl + "employee/getEmployee",
      { id: data },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* getEmployeeRequestAsync(data) {
  try {
    const result = yield call(getEmployeeRequestAPI, data);
    yield put({ type: "GET_EMPLOYEE_SUCCESS_ASYNC", response: result });
  } catch (err) {
    yield put({ type: "GET_EMPLOYEE_FAILURE_ASYNC", err });
  }
}

export function* watchgetEmployeeRequest(data) {
  yield takeLatest("GET_EMPLOYEE_REQUEST", getEmployeeRequestAsync);
}
/////////////////get induction by id////////////////////////////////
const getInductionRequestAPI = data => {
  
  return axios
    .post(
      apiUrl + "employee/getInduction",
      { id: data },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* getInductionRequestAsync(data) {
  try {
    const result = yield call(getInductionRequestAPI, data);
    yield put({ type: "GET_INDUCTION_SUCCESS_ASYNC", response: result });
  } catch (err) {
    yield put({ type: "GET_INDUCTION_FAILURE_ASYNC", err });
  }
}

export function* watchgetInductionRequest(data) {
  yield takeLatest("GET_INDUCTION_DOWNLOAD_REQUEST", getInductionRequestAsync);
}
/////////////////Get List  //////////////////////////////

const getEmployeeListRequestAPI = data => {
  return axios
    .get(apiUrl + "employee/employeelist", {
      headers: {
        Accept: "application/json"
      }
    })
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* getEmployeeListRequestAsync(data) {
  try {
    const result = yield call(getEmployeeListRequestAPI, data);
    yield put({ type: "GET_EMPLOYEELIST_SUCCESS_ASYNC", response: result });
  } catch (err) {
    yield put({ type: "GET_EMPLOYEELIST_FAILURE_ASYNC", err });
  }
}

export function* watchgetEmployeeListRequest(data) {
  yield takeLatest("GET_EMPLOYEELIST_REQUEST", getEmployeeListRequestAsync);
}
/////////////////Get List for filters //////////////////////////////

const getEmployeeListFilterRequestAPI = data => {
  return axios
    .get(apiUrl + "employee/employeelistFiletrs", {
      headers: {
        Accept: "application/json"
      }
    })
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* getEmployeeListFilterRequestAsync(data) {
  try {
    const result = yield call(getEmployeeListFilterRequestAPI, data);
    yield put({ type: "GET_EMPLOYEELIST_FILTER_SUCCESS_ASYNC", response: result });
  } catch (err) {
    yield put({ type: "GET_EMPLOYEELIST_FILTER_FAILURE_ASYNC", err });
  }
}

export function* getEmployeeListFilterRequest(data) {
  yield takeLatest("GET_EMPLOYEELIST_FILETR_REQUEST", getEmployeeListFilterRequestAsync);
}
////////////update Employee/////////////////////////////

const updateEmployeeRequestAPI = data => {
  return axios
    .post(
     // apiUrl + "employee/putEmployee",
     apiUrl + "employee/updateEmployee",
      { formdata: data.formdata },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* updateEmployeeRequestAsync(data) {
  try {
    const result = yield call(updateEmployeeRequestAPI, data);
    yield put({ type: "UPDATE_EMPLOYEE_SUCCESS_ASYNC", response: result });
    if( data.formdata.employeeId){ data.formdata.id = data.formdata.employeeId; }
    yield put({ type: "GET_EMPLOYEE_REQUEST", data:data.formdata.id});
  } catch (err) {
    yield put({ type: "UPDATE_EMPLOYEE_FAILURE_ASYNC", err });
  }
}

export function* watchUpdateEmployeeRequest(data) {
  yield takeLatest("UPDATE_EMPLOYEE_REQUEST", updateEmployeeRequestAsync);
}
////////////update Induction /////////////////////////////

const updateEmployeeInductionRequestAPI = data => {
  return axios
    .post(    
     apiUrl + "employee/putInduction",
      { formdata: data.formdata },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* updateEmployeeInductionRequestAsync(data) {
  try {
    
    const result = yield call(updateEmployeeInductionRequestAPI, data);
    yield put({ type: "UPDATE_EMPLOYEE_INDUCTION_SUCCESS_ASYNC", response: result });    
  } catch (err) {
    yield put({ type: "UPDATE_EMPLOYEE_INDUCTION_FAILURE_ASYNC", err });
  }
}

export function* watchUpdateEmployeeInductionRequest(data) {
  yield takeLatest("POST_INDUCTION_DOWNLOAD_REQUEST", updateEmployeeInductionRequestAsync);
}

////////////update Meeting request /////////////////////////////

const updateMeetingAllowequestAPI = data => {
  return axios
    .post(    
     apiUrl + "employee/putAllowMeeting",
      { formdata: data.formdata },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* updateMeetingAllowequestAsync(data) {
  try {
    //console.log("data", data);
    const result = yield call(updateMeetingAllowequestAPI, data);
    yield put({ type: "UPDATE_EMPLOYEE_ALLOW_MEETING_SUCCESS_ASYNC", response: result });    
  } catch (err) {
    yield put({ type: "UPDATE_EMPLOYEE_ALLOW_MEETING_FAILURE_ASYNC", err });
  }
}

export function* updateMeetingAllowequest(data) {
  yield takeLatest("POST_EMPLOYEE_MEETING_ALLOW_REQUEST", updateMeetingAllowequestAsync);
}
////////////update Training questionset request /////////////////////////////

const updateTrainingRquestAPI = data => {
  return axios
    .post(    
     apiUrl + "employee/putTraining",
      { formdata: data.formdata },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* updateTrainingRequestAsync(data) {
  try {
    //console.log("data", data);
    const result = yield call(updateTrainingRquestAPI, data);
    yield put({ type: "UPDATE_TRAINING_SUCCESS_ASYNC", response: result });    
  } catch (err) {
    yield put({ type: "UPDATE_TRAINING_FAILURE_ASYNC", err });
  }
}

export function* updateTrainingRequest(data) {
  yield takeLatest("UPDATE_TRAINING_REQUEST", updateTrainingRequestAsync);
}
////////////update Password/////////////////////////////

const updateEmployeePasswordRequestAsyncAPI = data => {
  return axios
    .post(
     // apiUrl + "employee/putEmployee",
     apiUrl + "employee/resetPassword",
      { formdata: data.formdata },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* updateEmployeePasswordRequestAsync(data) {
  try {
   // console.log("data", data);
    const result = yield call(updateEmployeePasswordRequestAsyncAPI, data);
    yield put({ type: "UPDATE_PASSWORD_SUCCESS_ASYNC", response: result });
  } catch (err) {
    yield put({ type: "UPDATE_PASSWORD_FAILURE_ASYNC", err });
  }
}

export function* watchResetPasswordRequest(data) {
  yield takeLatest("UPDATE_PASSWORD_REQUEST", updateEmployeePasswordRequestAsync);
}

////////////Delete Employee/////////////////////////////

const deleteEmployeeRequestAPI = data => {
  return axios
    .post(
      apiUrl + "employee/deleteEmployee",
      { id: data.id },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* deleteEmployeeRequestAsync(data) {
  try {
    const result = yield call(deleteEmployeeRequestAPI, data);
    yield put({ type: "DELETE_EMPLOYEE_SUCCESS_ASYNC", response: result });
    yield put({ type: "GET_EMPLOYEELIST_REQUEST" });
  } catch (err) {
    yield put({ type: "UPDATE_EMPLOYEE_FAILURE_ASYNC", err });
  }
}

export function* watchDeleteEmployeeRequest(data) {
  yield takeLatest("DELETE_EMPLOYEE_REQUEST", deleteEmployeeRequestAsync);
}

////////////Delete Employee/////////////////////////////

/////////////////Get manager List GET_manager_REQUEST //////////////////////////////

const getManagerListRequestAPI = data => {
  return axios
    .get(apiUrl + "employee/managerlist", {
      headers: {
        Accept: "application/json"
      }
    })
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* getManagerListRequestAsync(data) {
  try {
    const result = yield call(getManagerListRequestAPI, data);
    yield put({ type: "GET_MANAGERLIST_SUCCESS_ASYNC", response: result });
  } catch (err) {
    yield put({ type: "GET_MANAGERLIST_FAILURE_ASYNC", err });
  }
}

export function* watchgetManagerListRequest(data) {
  yield takeLatest("GET_MANAGERLIST_REQUEST", getManagerListRequestAsync);
}
/////////////////Get manager List GET_manager_REQUEST //////////////////////////////

const getMeetingAllowedEmployeeListRequestAPI = data => {
  return axios
    .get(apiUrl + "employee/meetingAllowedEmployeeList", {
      headers: {
        Accept: "application/json"
      }
    })
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* getMeetingAllowedEmployeeListRequestAsync(data) {
  try {
    const result = yield call(getMeetingAllowedEmployeeListRequestAPI, data);
    yield put({ type: "GET_MEETING_ALLOWED_EMPLOYEE_LIST_SUCCESS_ASYNC", response: result });
  } catch (err) {
    yield put({ type: "GET_MEETING_ALLOWED_EMPLOYEE_LIST_FAILURE_ASYNC", err });
  }
}

export function* watchgetMeetingAllowedEmployeeListRequest(data) {
  yield takeLatest("GET_MEETING_ALLOWED_EMPLOYEE_LIST_REQUEST", getMeetingAllowedEmployeeListRequestAsync);
}

//////////////////////get training set /////////////////////////////////////////


const getTrainingSetRequestAsyncAPI = data => {
  //console.log("Saga for traing set", data);
    return axios
      .get(apiUrl + "employee/getTrainingSet?employee="+data.employee, {
        headers: {
          Accept: "application/json"
        }
      })
      .then(response => response.data)
      .catch(err => {
        throw err;
      });
  };
  
  function* getTrainingSetRequestAsync(data) {
    
    try {
      const result = yield call(getTrainingSetRequestAsyncAPI, data);
      yield put({ type: "GET_TRAINING_SET_SUCCESS_ASYNC", response: result });
    } catch (err) {
      yield put({ type: "GET_TRAINING_SET_FAILURE_ASYNC", err });
    }
  }
  
  export function* watchgetTrainingSetRequest(data) {
    
    yield takeLatest("GET_TRAINING_SET_REQUEST", getTrainingSetRequestAsync);
  }