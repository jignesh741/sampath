import axios from "axios";
import { delay } from "redux-saga";
import Constants from "../config";
import { takeLatest, put, call } from "redux-saga/effects";

const apiUrl = Constants.API_URL;

const getLoginRequestAPI = data => {
  return axios
    .post(
      apiUrl + "users/login",
      { email: data.formdata.username, password: data.formdata.password },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* loginRequestAsync(data) {
  try {
    const result = yield call(getLoginRequestAPI, data);
    localStorage.setItem("token", result.token);
    localStorage.setItem("fname", result.userdata.fname);
    localStorage.setItem("lname", result.userdata.lname);
    localStorage.setItem("roles", result.userdata.roles);
    localStorage.setItem("userid", result.userdata._id);
    localStorage.setItem("center", result.userdata.centers[0].id._id);
    localStorage.setItem("centerCode", result.userdata.centers[0].id.code);
 
    yield put({ type: "LOGIN_SUCCESS_ASYNC", formdata: result });
  } catch (err) {
    yield put({ type: "LOGIN_FAILURE_ASYNC", err });
  }

  //localStorage.setItem("token", data.data.userName);
}

export function* watchLoginRequest(data) {
  yield takeLatest("LOGIN_REQUEST", loginRequestAsync);
}
