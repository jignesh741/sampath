import axios from "axios";
import { delay } from "redux-saga";
import Constants from "../config";
import { takeLatest, put, call } from "redux-saga/effects";



const center = localStorage.getItem("center");

const apiUrl = Constants.API_URL;

  ///////////////////// Save Report Template ///////////////////////////////////////////////////////////////////////////
  const saveReportTemplateRequestAPI = data => {
    return axios
      .post(
        apiUrl + "reporttemplate/saveReportTemplate",
        { formdata: data.formdata},
        {
          headers: {
            Accept: "application/json",
           // Authorization: `Bearer ${token}`
          }
        }
      )
      .then(response => response.data)
      .catch(err => {
        throw err;
      });
  };
  
  function* saveReportTemplateRequestAsync(data) {
    try {
      const result = yield call(saveReportTemplateRequestAPI, data);
      yield put({ type: "SAVE_REPORT_TEMPLATE_REQUEST_SUCCESS_ASYNC", response: result });
       } catch (err) {
      yield put({ type: "SAVE_REPORT_TEMPLATE_REQUEST_FAILURE_ASYNC", err });
    }
  }
  
  export function* watchsaveReportTemplateRequest(data) {
    yield takeLatest("SAVE_REPORT_TEMPLATE_REQUEST", saveReportTemplateRequestAsync);
  }
////////////////SAVE DRAFT REPORT////////////////////////////////////////
const saveReportDraftRequestAPI = data => {
    return axios
      .post(
        apiUrl + "report/saveDraftReport",
        { formdata: data.formdata},
        {
          headers: {
            Accept: "application/json"
          }
        }
      )
      .then(response => response.data)
      .catch(err => {
        throw err;
      });
    };
    
    function* saveDraftReportRequestAsync(data) {
    try {
      const result = yield call(saveReportDraftRequestAPI, data);
      yield put({ type: "SAVE_REPORT_DRAFT_SUCCESS_ASYNC", response: result });
       } catch (err) {
      yield put({ type: "SAVE_REPORT_DRAFT_FAILURE_ASYNC", err });
    }
    }
    
    export function* watchSaveDraftReportRequest(data) {
    yield takeLatest("SAVE_REPORT_DRAFT_REQUEST", saveDraftReportRequestAsync);
    }
    ////////////////SAVE  REPORT////////////////////////////////////////
    const saveReportRequestAPI = data => {
    return axios
      .post(
        apiUrl + "report/saveReport",
        { formdata: data.formdata},
        {
          headers: {
            Accept: "application/json"
          }
        }
      )
      .then(response => response.data)
      .catch(err => {
        throw err;
      });
    };
    
    function* saveReportRequestAsync(data) {
    try {
      const result = yield call(saveReportRequestAPI, data);
      yield put({ type: "SAVE_REPORT_SUCCESS_ASYNC", response: result });
       } catch (err) {
      yield put({ type: "SAVE_REPORT_FAILURE_ASYNC", err });
    }
    }
    
    export function* watchSaveReportRequest(data) {
    yield takeLatest("SAVE_REPORT_REQUEST", saveReportRequestAsync);
    }

    ///////get report templates/////////////
    const reportTemplateListRequestAsyncAPI = data => {
      return axios
        .get(
          apiUrl + "reporttemplate/getReportTemplateList",
          { params: data.formdata || ""},
          {
            headers: {
              Accept: "application/json"
            }
          }
        )
        .then(response => response.data)
        .catch(err => {
          throw err;
        });
      };
      
      function* reportTemplateListRequestAsync(data) {
      try {
        const result = yield call(reportTemplateListRequestAsyncAPI, data);
        yield put({ type: "REPORT_TEMPLATE_LIST_SUCCESS_ASYNC", response: result });
         } catch (err) {
        yield put({ type: "REPORT_TEMPLATE_LIST_FAILURE_ASYNC", err });
      }
      }
      
      export function* watchreportTemplateListRequest(data) {
      yield takeLatest("REPOR_TEMPLATE_LIST_REQUEST", reportTemplateListRequestAsync);
      }