import axios from "axios";
import { delay } from "redux-saga";
import Constants from "../config";
import { takeLatest, put, call } from "redux-saga/effects";



const center = localStorage.getItem("center");

const apiUrl = Constants.API_URL;

/////////////////bof Send Schedule Request //////////////////////////////

const sendScheduleRequestAPI = data => {
  return axios
    .post(
      apiUrl + "schedule/schedulePatient",
      { formdata: data.formdata},
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* sendScheduleRequestAsync(data) {
  try {
    const result = yield call(sendScheduleRequestAPI, data);
    yield put({ type: "PATIENT_SCHEDULE_SUCCESS_ASYNC", response: result });
     } catch (err) {
    yield put({ type: "PATIENT_SCHEDULE_FAILURE_ASYNC", err });
  }
}

export function* watchScheduleRequest(data) {
  yield takeLatest("SEND_PATIENT_SCHEDULE_REQUEST", sendScheduleRequestAsync);
}
/////////////////eof Send Schedule Request //////////////////////////////
/////////////////bof Save Schedule Time Slots Request //////////////////////////////

const saveScheduleRequestAPI = data => {
  return axios
    .post(
      apiUrl + "schedule/savePatientTimeSlots",
      { formdata: data.formdata},
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* saveScheduleRequestAsync(data) {
  try {
    const result = yield call(saveScheduleRequestAPI, data);
    yield put({ type: "SCHEDULE_SAVE_SUCCESS_ASYNC", response: result });
     } catch (err) {
    yield put({ type: "SCHEDULE_SAVE_FAILURE_ASYNC", err });
  }
}

export function* watchSaveScheduleRequest(data) {
  yield takeLatest("SAVE_PATIENT_SCHEDULE_REQUEST", saveScheduleRequestAsync);
}
/////////////////eof Send Schedule  Time Slots Request //////////////////////////////

const scheduleListRequestAPI = data => {
  console.log("Order Saga", data.formdata)
return axios
  .get(
    apiUrl + "schedule/getScheduleList",
    { params: data.formdata},
    {
      headers: {
        Accept: "application/json"
      }
    }
  )
  .then(response => response.data)
  .catch(err => {
    throw err;
  });
};

function* scheduleListRequestAsync(data) {
try {
  const result = yield call(scheduleListRequestAPI, data);
  yield put({ type: "GET_SCHEDULE_LIST_SUCCESS_ASYNC", response: result });
   } catch (err) {
  yield put({ type: "GET_SCHEDULE_LIST_FAILURE_ASYNC", err });
}
}

export function* watchScheduleListRequest(data) {
yield takeLatest("SCHEDULE_LIST_REQUEST", scheduleListRequestAsync);
}
/////////////////bof Room List Request //////////////////////////////

const roomListRequestAPI = data => {
  console.log("Order Saga", data.formdata)
return axios
  .get(
    apiUrl + "schedule/getRoomList",
    { params: data.formdata},
    {
      headers: {
        Accept: "application/json"
      }
    }
  )
  .then(response => response.data)
  .catch(err => {
    throw err;
  });
};

function* roomListRequestAsync(data) {
try {
  const result = yield call(roomListRequestAPI, data);
  yield put({ type: "GET_ROOM_LIST_SUCCESS_ASYNC", response: result });
   } catch (err) {
  yield put({ type: "GET_ROOM_LIST_FAILURE_ASYNC", err });
}
}

export function* watchRoomListRequest(data) {
yield takeLatest("MODALITY_ROOM_LIST_REQUEST", roomListRequestAsync);
}
////////////////SCAN FINISHED SAVE////////////////////////////////////////
const scanFinishedRequestAPI = data => {
  console.log("Order Saga", data.formdata)
return axios
  .post(
    apiUrl + "schedule/scanFinished",
    { scheduleId: data.scheduleId},
    {
      headers: {
        Accept: "application/json"
      }
    }
  )
  .then(response => response.data)
  .catch(err => {
    throw err;
  });
};

function* scanFinishedRequestAsync(data) {
try {
  const result = yield call(scanFinishedRequestAPI, data);
  yield put({ type: "SCAN_FINISHED_SUCCESS_ASYNC", response: result });
   } catch (err) {
  yield put({ type: "SCAN_FINISHED_FAILURE_ASYNC", err });
}
}

export function* watchScanFinishedRequest(data) {
yield takeLatest("SCAN_FINISHED_REQUEST", scanFinishedRequestAsync);
}
////////////////IMAGES PUSHED STATUS SAVE////////////////////////////////////////
const imagesPushedRequestAPI = data => {
return axios
  .post(
    apiUrl + "schedule/imagesPushed",
    { scheduleId: data.scheduleId},
    {
      headers: {
        Accept: "application/json"
      }
    }
  )
  .then(response => response.data)
  .catch(err => {
    throw err;
  });
};

function* imagesPushedRequestAsync(data) {
try {
  const result = yield call(imagesPushedRequestAPI, data);
  yield put({ type: "IMAGES_PUSHED_SUCCESS_ASYNC", response: result });
   } catch (err) {
  yield put({ type: "IMAGES_PUSHED_FAILURE_ASYNC", err });
}
}

export function* watchImagesPushedRequest(data) {
yield takeLatest("IMAGES_PUSHED_REQUEST", imagesPushedRequestAsync);
}
