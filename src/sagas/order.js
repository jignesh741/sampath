import axios from "axios";
import { delay } from "redux-saga";
import Constants from "../config";
import { takeLatest, put, call } from "redux-saga/effects";



const center = localStorage.getItem("center");

const apiUrl = Constants.API_URL;

/////////////////bof Submit Payment Request //////////////////////////////

const sendOrderListRequestAPI = data => {
    console.log("Order Saga", data.formdata)
  return axios
    .get(
      apiUrl + "order/getOrderList",
      { params: data.formdata},
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* sendOrderListRequestAsync(data) {
  try {
    const result = yield call(sendOrderListRequestAPI, data);
    yield put({ type: "GET_ORDER_LIST_SUCCESS_ASYNC", response: result });
     } catch (err) {
    yield put({ type: "GET_ORDER_LIST_FAILURE_ASYNC", err });
  }
}

export function* watchgetOrderListRequest(data) {
  yield takeLatest("ORDER_LIST_REQUEST", sendOrderListRequestAsync);
}
/////////////////eof Submit Payment Request //////////////////////////////
