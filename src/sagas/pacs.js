import axios from "axios";
import { delay } from "redux-saga";
import Constants from "../config";
import { takeLatest, put, call } from "redux-saga/effects";



const center = localStorage.getItem("center");

const apiUrl = Constants.API_URL;

  ///////////////////// PACS LIST START ///////////////////////////////////////////////////////////////////////////
const pacsListRequestAPI = data => {
     return axios
    .get(
      apiUrl + "pacs/getPacsList",
      { params: data.formdata},
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
  };
  
  function* pacsListRequestAsync(data) {
  try {
    const result = yield call(pacsListRequestAPI, data);
    yield put({ type: "GET_PACS_LIST_SUCCESS_ASYNC", response: result });
     } catch (err) {
    yield put({ type: "GET_PACS_LIST_FAILURE_ASYNC", err });
  }
  }
  
  export function* watchPACSListRequest(data) {
  yield takeLatest("PACS_LIST_REQUEST", pacsListRequestAsync);
  }

  ///////////////////// PACS LIST END ///////////////////////////////////////////////////////////////////////////