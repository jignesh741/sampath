import axios from "axios";
import { delay } from "redux-saga";
import Constants from "../config";
import { takeLatest, put, call } from "redux-saga/effects";

const apiUrl = Constants.API_URL;

const addModalityRequestAPI = data => {
  return axios
    .post(
      apiUrl + "modality/addModality",
      { modality: data.formdata.modality, description: data.formdata.description },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* addModalityRequestAsync(data) {
  try {
    const result = yield call(addModalityRequestAPI, data);
    yield put({ type: "ADD_MODALITY_SUCCESS_ASYNC", response: result });
    yield put({ type: "GET_MODALITYLIST_REQUEST" });
  } catch (err) {
    yield put({ type: "ADD_MODALITY_FAILURE_ASYNC", err });
  }
}

export function* watchAddModalityRequest(data) {
  yield takeLatest("ADD_MODALITY_REQUEST", addModalityRequestAsync);
}


/////////////////bof modality Setting request //////////////////////////////

const sendModalitySettingsRequestAPI = data => {
  return axios
    .post(
      apiUrl + "modality/modalitySettings",
      { formdata: data.formdata },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* sendModalitySettingsRequestAsync(data) {
  try {
    const result = yield call(sendModalitySettingsRequestAPI, data);
    yield put({ type: "MODALITY_SETTINGS_REQUEST_SUCCESS_ASYNC", response: result });
   } catch (err) {
    yield put({ type: "MODALITY_SETTINGS_REQUEST_FAILURE_ASYNC", err });
  }
}

export function* watchSendModalitySettingsRequest(data) {
  yield takeLatest("SEND_MODALITY_SETTINGS_REQUEST", sendModalitySettingsRequestAsync);
}

/////////////////eof modality Setting request //////////////////////////////

const getModalityListRequestAPI = data => {
   
  return axios
    .get(apiUrl + "modality/modalitylist?locationId="+data.data, {
      headers: {
        Accept: "application/json"
      }
    })
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* getModalityListRequestAsync(data) {
  try {
    const result = yield call(getModalityListRequestAPI, data);
    yield put({ type: "GET_MODALITYLIST_SUCCESS_ASYNC", response: result });
  } catch (err) {
    yield put({ type: "GET_MODALITYLIST_FAILURE_ASYNC", err });
  }
}

export function* watchgetModalityListRequest(data) {
  yield takeLatest("GET_MODALITYLIST_REQUEST", getModalityListRequestAsync);
}
////////////update Modality/////////////////////////////

const updateModalityRequestAPI = data => {
  return axios
    .post(
      apiUrl + "modality/putModality",
      { formdata: data.formdata },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* updateModalityRequestAsync(data) {
  try {
    const result = yield call(updateModalityRequestAPI, data);
    yield put({ type: "UPDATE_MODALITY_SUCCESS_ASYNC", response: result });
    yield put({ type: "GET_MODALITYLIST_REQUEST" });
  } catch (err) {
    yield put({ type: "UPDATE_MODALITY_FAILURE_ASYNC", err });
  }
}

export function* watchUpdateModalityRequest(data) {
  yield takeLatest("UPDATE_MODALITIES_MODALITY_REQUEST", updateModalityRequestAsync);
}

////////////Delete Modality/////////////////////////////

const deleteModalityRequestAPI = data => {
  return axios
    .post(
      apiUrl + "modality/deleteModality",
      { id: data.id },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* deleteModalityRequestAsync(data) {
  try {
    const result = yield call(deleteModalityRequestAPI, data);
    yield put({ type: "DELETE_MODALITY_SUCCESS_ASYNC", response: result });
    yield put({ type: "GET_MODALITYLIST_REQUEST" });
  } catch (err) {
    yield put({ type: "UPDATE_MODALITY_FAILURE_ASYNC", err });
  }
}

export function* watchDeleteModalityRequest(data) {
  yield takeLatest("DELETE_MODALITY_REQUEST", deleteModalityRequestAsync);
}

