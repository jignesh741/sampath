import axios from "axios";
import { delay } from "redux-saga";
import Constants from "../config";
import { takeLatest, put, call } from "redux-saga/effects";


var localData = localStorage.getItem('userdata');
const userdata = JSON.parse(localData);
const center = userdata && userdata.center ?  userdata.center[0].id : "";

const apiUrl = Constants.API_URL;

const addPatientRequestAPI = data => {
  return axios
    .post(
      apiUrl + "patient/addPatient",
      { formdata: data.formdata },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* addPatientRequestAsync(data) {
  try {
    const result = yield call(addPatientRequestAPI, data);
    yield put({ type: "ADD_PATIENT_SUCCESS_ASYNC", response: result });
    //yield put({ type: "GET_PATIENTLIST_REQUEST" });
  } catch (err) {
    yield put({ type: "ADD_PATIENT_FAILURE_ASYNC", err });
  }
}

export function* watchAddPatientRequest(data) {
  yield takeLatest("ADD_PATIENT_REQUEST", addPatientRequestAsync);
}

///////////////// //////////////////////////////

const getPatientListRequestAPI = data => {
   console.log(data.data)
  return axios
    .get(apiUrl + "patient/patientlist", {
      params: data.data,
    }, 
    {
      headers: {
        Accept: "application/json"
      }
    })
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* getPatientListRequestAsync(data) {
  try {
    const result = yield call(getPatientListRequestAPI, data);
    yield put({ type: "GET_PATIENTLIST_SUCCESS_ASYNC", response: result });
  } catch (err) {
    yield put({ type: "GET_PATIENTLIST_FAILURE_ASYNC", err });
  }
}

export function* watchgetPatientListRequest(data) {
  yield takeLatest("GET_PATIENTLIST_REQUEST", getPatientListRequestAsync);
}
///////////////// //////////////////////////////
/////////////////bof Filter Patient for counts//////////////////////////////

const getPatientFilterListRequestAPI = data => {
   console.log(data.data)
  return axios
    .post(apiUrl + "patient/patientCountFilters", {
      filters: data.data,
    }, 
    {
      headers: {
        Accept: "application/json"
      }
    })
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* getPatientFilterListRequestAsync(data) {
  try {
    const result = yield call(getPatientFilterListRequestAPI, data);
    yield put({ type: "GET_PATIENTLIST_SUCCESS_ASYNC", response: result });
  } catch (err) {
    yield put({ type: "GET_PATIENTLIST_FAILURE_ASYNC", err });
  }
}

export function* watchgetPatientFilterListRequest(data) {
  yield takeLatest("GET_PATIENT_FILTERS_REQUEST", getPatientFilterListRequestAsync);
}
/////////////////eof Filter Patient for counts//////////////////////////////

const getConfirmRequestAPI = data => {
   console.log(data.data)
  return axios
    .post(apiUrl + "patient/confirmReportTransfer", {
      ids: data.data,
    }, 
    {
      headers: {
        Accept: "application/json"
      }
    })
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* getConfirmRequestAsync(data) {
  try {
    const result = yield call(getConfirmRequestAPI, data);
    yield put({ type: "CONFIRM_DATA_TRANSFER_SUCCESS_ASYNC", response: result });
    yield put({ type: "GET_PATIENTLIST_REQUEST", data: {executive: "5e1ad26973d99923bd12bc7b",filterStr:{} } });
  } catch (err) {
    yield put({ type: "CONFIRM_DATA_TRANSFER_FAILURE_ASYNC", err });
  }
}

export function* watchgetConfirmRequest(data) {
  yield takeLatest("CONFIRM_MANY_REQUEST", getConfirmRequestAsync);
}
/////////////////get Report //////////////////////////////

const getReportDatarequestAPI = data => {
   console.log(data.data)
  return axios
    .get(apiUrl + "patient/getReport?id="+data.data,
    {
      headers: {
        Accept: "application/json"
      }
    })
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* getReportDatarequestAsync(data) {
  try {
    const result = yield call(getReportDatarequestAPI, data);
    yield put({ type: "GET_REPORT_DATA_SUCCESS_ASYNC", response: result });
  } catch (err) {
    yield put({ type: "GET_REPORT_DATA_FAILUER_ASYNC", err });
  }
}

export function* watchgetReportDatarequest(data) {
  yield takeLatest("GET_REPORT_DATA_REQUEST", getReportDatarequestAsync);
}
