import axios from "axios";
import { delay } from "redux-saga";
import Constants from "../config";
import { takeLatest, put, call } from "redux-saga/effects";

const apiUrl = Constants.API_URL;

const addCenterRequestAPI = data => {
  return axios
    .post(
      apiUrl + "center/addCenter",
      { center: data.formdata.center, code: data.formdata.code, address: data.formdata.address },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* addCenterRequestAsync(data) {
  try {
    const result = yield call(addCenterRequestAPI, data);
    yield put({ type: "ADD_CENTER_SUCCESS_ASYNC", response: result });
    yield put({ type: "GET_CENTERLIST_REQUEST" });
  } catch (err) {
    yield put({ type: "ADD_CENTER_FAILURE_ASYNC", err });
  }
}

export function* watchAddCenterRequest(data) {
  yield takeLatest("ADD_CENTER_REQUEST", addCenterRequestAsync);
}

/////////////////Get Bonustype List GET_BONUSTYPELIST_REQUEST //////////////////////////////

const getCenterListRequestAPI = data => {
   
  return axios
    .get(apiUrl + "center/centerlist?centerId="+data.data, {
      headers: {
        Accept: "application/json"
      }
    })
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* getCenterListRequestAsync(data) {
  try {
    const result = yield call(getCenterListRequestAPI, data);
    yield put({ type: "GET_CENTERLIST_SUCCESS_ASYNC", response: result });
  } catch (err) {
    yield put({ type: "GET_CENTERLIST_FAILURE_ASYNC", err });
  }
}

export function* watchgetCenterListRequest(data) {
  yield takeLatest("GET_CENTERLIST_REQUEST", getCenterListRequestAsync);
}

////////////update Center/////////////////////////////

const updateCenterRequestAPI = data => {
  return axios
    .post(
      apiUrl + "center/putCenter",
      { formdata: data.formdata },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* updateCenterRequestAsync(data) {
  try {
    const result = yield call(updateCenterRequestAPI, data);
    yield put({ type: "UPDATE_CENTER_SUCCESS_ASYNC", response: result });
    yield put({ type: "GET_CENTERLIST_REQUEST" });
  } catch (err) {
    yield put({ type: "UPDATE_CENTER_FAILURE_ASYNC", err });
  }
}

export function* watchUpdateCenterRequest(data) {
  yield takeLatest("UPDATE_CENTER_REQUEST", updateCenterRequestAsync);
}

////////////Delete Center/////////////////////////////

const deleteCenterRequestAPI = data => {
  return axios
    .post(
      apiUrl + "center/deleteCenter",
      { id: data.id },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* deleteCenterRequestAsync(data) {
  try {
    const result = yield call(deleteCenterRequestAPI, data);
    yield put({ type: "DELETE_CENTER_SUCCESS_ASYNC", response: result });
    yield put({ type: "GET_CENTERLIST_REQUEST" });
  } catch (err) {
    yield put({ type: "UPDATE_CENTER_FAILURE_ASYNC", err });
  }
}

export function* watchDeleteCenterRequest(data) {
  yield takeLatest("DELETE_CENTER_REQUEST", deleteCenterRequestAsync);
}


///////////////Center Manager/////////////////////////////////////////////

const addCenterManagerRequestAPI = data => {
    return axios
      .post(
        apiUrl + "center/addCenterManager",
        { formdata: data.formdata},
        {
          headers: {
            Accept: "application/json"
          }
        }
      )
      .then(response => response.data)
      .catch(err => {
        throw err;
      });
  };
  
  function* addCenterManagerRequestAsync(data) {
    try {
      const result = yield call(addCenterManagerRequestAPI, data);
      yield put({ type: "ADD_CENTER_MANAGER_SUCCESS_ASYNC", response: result });
      yield put({ type: "GET_CENTER_MANAGER_LIST_REQUEST" });
    } catch (err) {
      yield put({ type: "ADD_CENTER_MANAGER_FAILURE_ASYNC", err });
    }
  }
  
  export function* watchAddCenterManagerRequest(data) {
    yield takeLatest("ADD_CENTER_MANAGER_REQUEST", addCenterManagerRequestAsync);
  }
  
const updateCenterManagerRequestAPI = data => {
    return axios
      .post(
        apiUrl + "center/putCenterManager",
        { formdata: data.formdata },
        {
          headers: {
            Accept: "application/json"
          }
        }
      )
      .then(response => response.data)
      .catch(err => {
        throw err;
      });
  };
  
  function* updateCenterManagerRequestAsync(data) {
    try {
      const result = yield call(updateCenterManagerRequestAPI, data);
      yield put({ type: "UPDATE_CENTER_MANAGER_SUCCESS_ASYNC", response: result });
      yield put({ type: "GET_CENTER_MANAGER_LIST_REQUEST" });
    } catch (err) {
      yield put({ type: "UPDATE_CENTER_MANAGER_FAILURE_ASYNC", err });
    }
  }
  
  export function* watchUpdateCenterManagerRequest(data) {
    yield takeLatest("UPDATE_CENTER_MANAGER_REQUEST", updateCenterManagerRequestAsync);
  }
  


const getCenterManagerListRequestAPI = data => {
   
    return axios
      .get(apiUrl + "center/centerManagerList?managerId="+data.data, {
        headers: {
          Accept: "application/json"
        }
      })
      .then(response => response.data)
      .catch(err => {
        throw err;
      });
  };
  
  function* getCenterManagerListRequestAsync(data) {
    try {
      const result = yield call(getCenterManagerListRequestAPI, data);
      yield put({ type: "GET_CENTER_MANAGER_LIST_SUCCESS_ASYNC", response: result });
    } catch (err) {
      yield put({ type: "GET_CENTER_MANAGER_LIST_FAILURE_ASYNC", err });
    }
  }
  
  export function* watchgetCenterManagerListRequest(data) {
    yield takeLatest("GET_CENTER_MANAGER_LIST_REQUEST", getCenterManagerListRequestAsync);
  }

  ////////////update Modality/////////////////////////////

const updateModalityRequestAPI = data => {
  return axios
    .post(
      apiUrl + "center/putModality",
      { formdata: data.formdata },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* updateModalityRequestAsync(data) {
  try {
    const result = yield call(updateModalityRequestAPI, data);
    yield put({ type: "UPDATE_MODALITY_SUCCESS_ASYNC", response: result });
    yield put({ type: "GET_CENTERLIST_REQUEST" });
  } catch (err) {
    yield put({ type: "UPDATE_MODALITY_FAILURE_ASYNC", err });
  }
}

export function* watchUpdateCenterModalityRequest(data) {
  yield takeLatest("UPDATE_MODALITY_REQUEST", updateModalityRequestAsync);
}
