import axios from "axios";
import { delay } from "redux-saga";
import Constants from "../config";
import { takeLatest, put, call } from "redux-saga/effects";


var localData = localStorage.getItem('userdata');
const userdata = JSON.parse(localData);
const center = userdata && userdata.center ?  userdata.center[0].id : "";
const token = localStorage.getItem('token');

const apiUrl = Constants.API_URL;


const getRegistrationsRequestAPI = data => {
  console.log("token", token);
  return axios
    .get(
      apiUrl + "registrations/fetchregistrations", 
      {
      params: data.data,
      headers: {
        Authorization: `Bearer ${token}`
      }
      }, 
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* getRegistrationsRequestAsync(data) {
  try {
    const result = yield call(getRegistrationsRequestAPI, data);
    yield put({ type: "GET_REGISTRATIONS_SUCCESS_ASYNC", response: result });
  } catch (err) {
    yield put({ type: "GET_REGISTRATIONS_FAILURE_ASYNC", err });
  }
}

export function* watchgetregistrationsRequest(data) {
  yield takeLatest("GET_REGISTRATIONS_REQUEST", getRegistrationsRequestAsync);
}

/////////////////bof Send Order Bill Request //////////////////////////////
const sendOrdeBillRequestAPI = data => {
  return axios
    .post(
      apiUrl + "registrations/orderBillRequest",
      { formdata: data.formdata},
      {
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token}`
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* sendOrdeBillRequestAsync(data) {
  try {
    const result = yield call(sendOrdeBillRequestAPI, data);
    yield put({ type: "SEND_ORDER_BILL_REQUEST_SUCCESS_ASYNC", response: result });
     } catch (err) {
    yield put({ type: "SEND_ORDER_BILL_REQUEST_FAILURE_ASYNC", err });
  }
}

export function* watchsendOrdeBillRequest(data) {
  yield takeLatest("SEND_ORDER_BILL_REQUEST", sendOrdeBillRequestAsync);
}
/////////////////eof Send Order Bill Request //////////////////////////////
/////////////////bof Register Patent Request. Request from Patient List //////////////////////////////
const registerPatientAPI = data => {
  return axios
    .post(
      apiUrl + "registrations/registerPatient",
      { formdata: data.formdata},
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* registerPatientRequestAsync(data) {
  try {
    const result = yield call(registerPatientAPI, data);
    yield put({ type: "REGISTER_PATIENT_REQUEST_SUCCESS_ASYNC", response: result });
     } catch (err) {
    yield put({ type: "REGISTER_PATIENT_REQUEST_FAILURE_ASYNC", err });
  }
}

export function* watchRegisterPatientRequest(data) {
  yield takeLatest("REGISTER_PATIENT_REQUEST", registerPatientRequestAsync);
}
/////////////////eof Register Patent Request. Request from Patient List //////////////////////////////
