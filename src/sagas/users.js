import axios from "axios";
import { delay } from "redux-saga";
import Constants from "../config";
import { takeLatest, put, call } from "redux-saga/effects";

const apiUrl = Constants.API_URL;


///////////////Factory Users/////////////////////////////////////////////

const addUserRequestAPI = data => {
    return axios
      .post(
        apiUrl + "users/addUser",
        { formdata: data.formdata},
        {
          headers: {
            Accept: "application/json"
          }
        }
      )
      .then(response => response.data)
      .catch(err => {
        throw err;
      });
  };
  
  function* addUserRequestAsync(data) {
    try {
      const result = yield call(addUserRequestAPI, data);
      yield put({ type: "ADD_USER_SUCCESS_ASYNC", response: result });
      yield put({ type: "GET_USER_LIST_REQUEST" });
    } catch (err) {
      yield put({ type: "ADD_USER_FAILURE_ASYNC", err });
    }
  }
  
  export function* watchAddUserRequest(data) {
    yield takeLatest("ADD_USER_REQUEST", addUserRequestAsync);
  }
  
const updateUserRequestAPI = data => {
    return axios
      .post(
        apiUrl + "users/putUser",
        { formdata: data.formdata },
        {
          headers: {
            Accept: "application/json"
          }
        }
      )
      .then(response => response.data)
      .catch(err => {
        throw err;
      });
  };
  
  function* updateUserRequestAsync(data) {
    try {
      const result = yield call(updateUserRequestAPI, data);
      yield put({ type: "UPDATE_USER_SUCCESS_ASYNC", response: result });
      yield put({ type: "GET_USER_LIST_REQUEST" });
    } catch (err) {
      yield put({ type: "UPDATE_USER_FAILURE_ASYNC", err });
    }
  }
  
  export function* watchUpdateUserRequest(data) {
    yield takeLatest("UPDATE_USER_REQUEST", updateUserRequestAsync);
  }
  


const getUserListRequestAPI = data => {
   
    return axios
      .get(apiUrl + "users/usersList?usersId="+data.data, {
        headers: {
          Accept: "application/json"
        }
      })
      .then(response => response.data)
      .catch(err => {
        throw err;
      });
  };
  
  function* getUserListRequestAsync(data) {
    try {
      const result = yield call(getUserListRequestAPI, data);
      yield put({ type: "GET_USER_LIST_SUCCESS_ASYNC", response: result });
    } catch (err) {
      yield put({ type: "GET_USER_LIST_FAILURE_ASYNC", err });
    }
  }
  
  export function* watchgetUserListRequest(data) {
    yield takeLatest("GET_USER_LIST_REQUEST", getUserListRequestAsync);
  }
////////////////////////////////////////////////////////////////////////////////
const getUserRequestAPI = data => {
   console.log("SAGA", data.Id)
    return axios
      .get(apiUrl + "users/user?id="+data.Id, {
        headers: {
          Accept: "application/json"
        }
      })
      .then(response => response.data)
      .catch(err => {
        throw err;
      });
  };
  
  function* getUserRequestAsync(data) {
    try {
      const result = yield call(getUserRequestAPI, data);
      yield put({ type: "GET_USER_SUCCESS_ASYNC", response: result });
    } catch (err) {
      yield put({ type: "GET_USER_FAILURE_ASYNC", err });
    }
  }
  
  export function* watchgetUserRequest(data) {
    yield takeLatest("GET_USER_REQUEST", getUserRequestAsync);
  }