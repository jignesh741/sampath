import axios from "axios";
import { delay } from "redux-saga";
import Constants from "../config";
import { takeLatest, put, call } from "redux-saga/effects";

const apiUrl = Constants.API_URL;


///////////////Factory Executives/////////////////////////////////////////////

const addExecutiveRequestAPI = data => {
    return axios
      .post(
        apiUrl + "executive/addExecutive",
        { formdata: data.formdata},
        {
          headers: {
            Accept: "application/json"
          }
        }
      )
      .then(response => response.data)
      .catch(err => {
        throw err;
      });
  };
  
  function* addExecutiveRequestAsync(data) {
    try {
      const result = yield call(addExecutiveRequestAPI, data);
      yield put({ type: "ADD_EXECUTIVE_SUCCESS_ASYNC", response: result });
      yield put({ type: "GET_EXECUTIVE_LIST_REQUEST" });
    } catch (err) {
      yield put({ type: "ADD_EXECUTIVE_FAILURE_ASYNC", err });
    }
  }
  
  export function* watchAddExecutiveRequest(data) {
    yield takeLatest("ADD_EXECUTIVE_REQUEST", addExecutiveRequestAsync);
  }
  
const updateExecutiveRequestAPI = data => {
    return axios
      .post(
        apiUrl + "executive/putExecutive",
        { formdata: data.formdata },
        {
          headers: {
            Accept: "application/json"
          }
        }
      )
      .then(response => response.data)
      .catch(err => {
        throw err;
      });
  };
  
  function* updateExecutiveRequestAsync(data) {
    try {
      const result = yield call(updateExecutiveRequestAPI, data);
      yield put({ type: "UPDATE_EXECUTIVE_SUCCESS_ASYNC", response: result });
      yield put({ type: "GET_EXECUTIVE_LIST_REQUEST" });
    } catch (err) {
      yield put({ type: "UPDATE_EXECUTIVE_FAILURE_ASYNC", err });
    }
  }
  
  export function* watchUpdateExecutiveRequest(data) {
    yield takeLatest("UPDATE_EXECUTIVE_REQUEST", updateExecutiveRequestAsync);
  }
  


const getExecutiveListRequestAPI = data => {
   
    return axios
      .get(apiUrl + "executive/executiveList?executiveId="+data.data, {
        headers: {
          Accept: "application/json"
        }
      })
      .then(response => response.data)
      .catch(err => {
        throw err;
      });
  };
  
  function* getExecutiveListRequestAsync(data) {
    try {
      const result = yield call(getExecutiveListRequestAPI, data);
      yield put({ type: "GET_EXECUTIVE_LIST_SUCCESS_ASYNC", response: result });
    } catch (err) {
      yield put({ type: "GET_EXECUTIVE_LIST_FAILURE_ASYNC", err });
    }
  }
  
  export function* watchgetExecutiveListRequest(data) {
    yield takeLatest("GET_EXECUTIVE_LIST_REQUEST", getExecutiveListRequestAsync);
  }
////////////////////////////////////////////////////////////////////////////////
const getExecutiveRequestAPI = data => {
   console.log("SAGA", data.Id)
    return axios
      .get(apiUrl + "executive/executive?id="+data.Id, {
        headers: {
          Accept: "application/json"
        }
      })
      .then(response => response.data)
      .catch(err => {
        throw err;
      });
  };
  
  function* getExecutiveRequestAsync(data) {
    try {
      const result = yield call(getExecutiveRequestAPI, data);
      yield put({ type: "GET_EXECUTIVE_SUCCESS_ASYNC", response: result });
    } catch (err) {
      yield put({ type: "GET_EXECUTIVE_FAILURE_ASYNC", err });
    }
  }
  
  export function* watchgetExecutiveRequest(data) {
    yield takeLatest("GET_EXECUTIVE_REQUEST", getExecutiveRequestAsync);
  }