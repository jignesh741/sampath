import axios from "axios";
import { delay } from "redux-saga";
import { Constants, header } from "../config";

import { takeLatest, put, call } from "redux-saga/effects";


const apiUrl = Constants.API_URL;

const getLoginRequestAPI = data => {
  return axios
    .post(
      apiUrl + "users/login",
      { email: data.formdata.username, password: data.formdata.password },
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};
// const getLoginRequestAPI = data => {
//   return axios.request({
//     method: "post",
//     url: apiUrl + "/users/login",
//     data: data
//   });
// };
function* logoutRequestAsync(data) {
  try {
    //const result = yield call(getLoginRequestAPI, data);
    //localStorage.setItem("token", result.token);
    yield put({ type: "LOGOUT_SUCCESS_ASYNC", formdata: data });
  } catch (err) {
    yield put({ type: "LOGOUT_FAILURE_ASYNC", err });
  }

  //localStorage.setItem("token", data.data.userName);
}

export function* watchLogoutRequest(data) {
  yield takeLatest("LOGOUT_REQUEST", logoutRequestAsync);
}
