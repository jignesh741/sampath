import axios from "axios";
import { delay } from "redux-saga";
import Constants from "../config";
import { takeLatest, put, call } from "redux-saga/effects";

const center = localStorage.getItem("center");

const apiUrl = Constants.API_URL;

const sendBillsRequestAPI = data => {
    console.log("Bill Saga", data.formdata)
  return axios
    .get(
      apiUrl + "bill/bills",
      { params: data.formdata},
      {
        headers: {
          Accept: "application/json"
        }
      }
    )
    .then(response => response.data)
    .catch(err => {
      throw err;
    });
};

function* sendBillsRequestAsync(data) {
  try {
    const result = yield call(sendBillsRequestAPI, data);
    yield put({ type: "BILLS_REQUEST_SUCCESS_ASYNC", response: result });
     } catch (err) {
    yield put({ type: "BILLS_REQUEST_FAILURE_ASYNC", err });
  }
}

export function* watchBillsRequest(data) {
  yield takeLatest("BILLS_REQUEST", sendBillsRequestAsync);
}
/////////////////eof Submit Payment Request //////////////////////////////
