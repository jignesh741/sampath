import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

const AccessControl = ({
  userPermissions,
  allowedPermissions,
  children,
  renderNoAccess
}) => {
  const permitted = checkPermissions(userPermissions, allowedPermissions);
  if (permitted) {
    return children;
  }
  return renderNoAccess();
};

const checkPermissions = (userPermissions, allowedPermissions) => {
  const found = userPermissions.find(val => allowedPermissions.includes(val));
  if (found) {
    return true;
  }
  userPermissions.map(permission => {
    console.log("userPermissions" + userPermissions);
    if (permission == allowedPermissions) {
      return true;
    }
  });
};

function mapStateToProps(state) {
  return {
    userPermissions: state.auth.userpermissions,
  };
}

export default connect(mapStateToProps)(AccessControl);
