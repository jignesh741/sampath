import React, { Component } from 'react';
import {
    Row,
    Col,
    Form,
    Input,
    Button,
    Table,
    Badge,
    Alert,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
  } from "reactstrap";
  import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
  import { connect } from "react-redux";
  import moment from "moment";
  import Editable from "react-x-editable";
  import AccessControl from "../utils/accessControl";
  import Constants from "../config";
import { element } from 'prop-types';

var localData = localStorage.getItem('userdata');
const userdata = JSON.parse(localData);
//const center = userdata.center[0].id;

const ParametersBox = (props) =>{
    console.log("parameterBox")

   return  (<div className="d-md-flex align-items-center justify-content-start mb-2">
       <div>
       <Input
          type="select"
          name="params"
          autoComplete="off"
          value={props.value}
          onChange={(e)=>props.handleChange(props.group, props.indexNumber, e)}
          key={Math.random(1,10)}
          required="true"
        >
        <option value="">Select Parameter</option>
        {props.parameterKeys.map((parametr,i)=>{
          return (
            <option value={parametr.value} key={parametr.value}>{parametr.name}</option>
          )
        })}
        </Input>
       </div>
      
      <div>
         
        <Button className="bg-pink font-weight-bold text-white" onClick={()=>props.removeBox(props.group, props.indexNumber)}>X</Button>
    </div>
    </div>)
}

export default ParametersBox;
