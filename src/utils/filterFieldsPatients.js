import React, { Component } from 'react';
import {
    Row,
    Col,
    Form,
    Input,
    Button,
    Table,
    Badge,
    Alert,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
  } from "reactstrap";
  import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
  import { connect } from "react-redux";
  import moment from "moment";
  import Editable from "react-x-editable";
  import AccessControl from "../utils/accessControl";
  import Constants from "../config";

var localData = localStorage.getItem('userdata');
const userdata = JSON.parse(localData);
//const center = userdata.center[0].id;


class FilterFieldsPatients extends Component {
    constructor(props) {
        super(props);
        this.state ={}
    }
    
      handleFilterChange = e => {
        console.log("e.target.name", e.target.name, "State Value=", e.target.value);
        this.setState({
          [e.target.name]: e.target.value
        });
      };
      handleSubmit = (e) =>{
        e.preventDefault();
        var data = {};
        if(this.state.name != ""){
            data = {...data, name: this.state.name}
        }
       
        if(this.state.patId != ""){
            data = { ...data, patId: this.state.patId}
        }
        if(this.state.email != ""){
            data = { ...data, email: this.state.email}
        }
        if(this.state.mobile != ""){
            data = { ...data, mobile: this.state.mobile}
        }
        if(this.state.dateFrom && !this.state.dateTo){
            alert("Please select both date fields");
            return;
        }
        if(!this.state.dateFrom && this.state.dateTo){
            alert("Please select both date fields");
            return;
        }
        if(this.state.dateFrom != ""){
            data = { ...data, dateFrom: this.state.dateFrom}
        }
        if(this.state.dateTo != ""){
            data = { ...data, dateTo: this.state.dateTo}
        }
       

        this.props.handleFilterSelected(this.props.selectedPage, data);
      }
      resetForm = (e) =>{
        e.preventDefault();
        
        this.setState({
            name: "",
            patId:"",
            email:"",
            mobile:"",
            dateFrom:"",
            dateTo:"",
           
        });
        this.props.handleFilterSelected(this.props.selectedPage ,"");
      }
      render() {
        return (
            <Form onSubmit={this.handleSubmit} autoComplete="off">
            <div className="d-md-flex align-items-center justify-content-start">
                    <div className="p-2">
                        <label htmlFor="name" >Name:</label>
                        <Input
                        type="text"
                        name="name"
                        id="name"
                        autoComplete="new-name"
                        value={this.state.name? this.state.name: ""}
                        onChange={this.handleFilterChange}
                      /></div>
                   
                    <div  className="p-2">
                        <label htmlFor="patId">PatId:</label>
                        <Input
                        type="text"
                        name="patId"
                        id="patId"
                        autoComplete="off"
                        value={this.state.patId? this.state.patId: ""}
                        onChange={this.handleFilterChange}
                      /></div>
                    <div  className="p-2">
                        <label htmlFor="email">Email ID:</label>
                        <Input
                        type="text"
                        name="email"
                        id="email"
                        autoComplete="new-name"
                        value={this.state.email? this.state.email: ""}
                        onChange={this.handleFilterChange}
                      /></div>
                    <div  className="p-2">
                        <label htmlFor="mobile">Mobile No.:</label>
                        <Input
                        type="text"
                        name="mobile"
                        id="mobile"
                        autoComplete="off"
                        value={this.state.mobile? this.state.mobile: ""}
                        onChange={this.handleFilterChange}
                      /></div>
                    <div  className="p-2">
                        <label htmlFor="dateFrom">Date From:</label>
                        <Input
                        type="date"
                        name="dateFrom"
                        id="dateFrom"
                        autoComplete="off"
                        value={this.state.dateFrom? this.state.dateFrom: ""}
                        onChange={this.handleFilterChange}
                      /></div>
                    <div  className="p-2">
                        <label htmlFor="dateTo">Date To:</label>
                        <Input
                        type="date"
                        name="dateTo"
                        id="dateTo"
                        autoComplete="off"
                        value={this.state.dateTo? this.state.dateTo: ""}
                        onChange={this.handleFilterChange}
                      /></div>
                   
                    <div  className="p-2 align-self-end">
                        <Button className="bg-pink font-weight-bold text-white" >Search</Button>
                    </div>
                    <div  className="p-2 align-self-end">
                        <Button className="bg-purple font-weight-bold text-white" onClick={this.resetForm}>Reset</Button>
                    </div>
                    
            </div>
            </Form>
        );
    }
}



  
  const mapDispachToProps = dispatch => {
    return {
      getRegistrationRequest: (data) =>
        dispatch({ type: "GET_REGISTRATIONS_REQUEST", data: data }),
    };
  };
  
  export default connect(
    null,
    mapDispachToProps
  )(FilterFieldsPatients);
