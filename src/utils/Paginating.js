import React, { Component } from "react";
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";

const hasPrevPage = (props)=>{
    if(props.hasPrevPage){
      return(
        <PaginationItem>
            <PaginationLink previous tag="button" />
        </PaginationItem>
      )  
    }else{
        return(
        <PaginationItem disabled>
            <PaginationLink previous tag="button" />
        </PaginationItem>)
    }
}

const middlePages = (props)=>{
    var i ;
    var pageranges = [];
    for(i=1; i < props.totalPages; i++){
        if(i == props.page){
        pageranges[i] = (<PaginationItem active>
            <PaginationLink tag="button">
             {i}
            </PaginationLink>
            </PaginationItem>)
        }else{
            pageranges[i] = (<PaginationItem >
            <PaginationLink tag="button">
              {i}
            </PaginationLink>
          </PaginationItem>)
        }       
    }
   
    return pageranges;
}
const hasNextPage = (props)=>{
    if(props.hasNextPage){
        return(
          <PaginationItem>
              <PaginationLink next tag="button" />
          </PaginationItem>
        )  
      }else{
          return(
          <PaginationItem disabled>
              <PaginationLink next tag="button" />
          </PaginationItem>)
      }
}


const Paginating = (props)=> {  
    return (
        <React.Fragment>
          <Pagination>
          {hasPrevPage(props)}  
          {middlePages(props)}  
          {hasNextPage(props)} 
        </Pagination>
     </React.Fragment>
    );
  }


  export default Paginating;
