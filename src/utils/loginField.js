import React from "react";

import {
  ListGroup,
  ListGroupItem,
  FormGroup,
  Label,
  Input,
  Button
} from "reactstrap";

const Loginfield = props => (
  <FormGroup row>
    <label className="col-sm-4 col-form-label">{props.label}</label>
    <input
      {...props.input}
      name={props.name}
      placeholder={props.label}
      type={props.type}
      className="col-sm-7 col-form-input"
      readOnly={props.readOnly}
      onChange={props.handleChange}
      required={props.required}
      value={props.initialValue}
      onBlur={props.onBlur}
    />
  </FormGroup>
);

export default Loginfield;
