import React, { Component } from "react";

import {
  ListGroup,
  ListGroupItem,
  FormGroup,
  Label,
  Input,
  Button,
  Form,
} from "reactstrap";
import moment from "moment";

export default class EditableField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEditing: false
    }
  }
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );
  
  handleEdit = ()=>{
    this.setState({
      isEditing: true
    })
  }

  handleCancelEdit = ()=>{
    this.setState({
      isEditing: false
    })
  }

  handleChange = (e) =>{
   
    // console.log(selectedOption.value); // 123
    // console.log(selectedOption.text); // TEXT
   
   this.setState({
        [e.target.name]:e.target.value
    });
    if(this.props.type == "select"){
      var selectedOption = e.target.selectedOptions[0];
      this.setState({
        SelecteText:selectedOption.text
    })
    }
  }
handleSubmit=(submitid, name)=>{
   if(this.state[this.props.name]){
    this.props.handleSubmit(submitid, this.props.name, this.state[this.props.name]);
    this.setState({
      isEditing: false
    })
  }else{
    alert("Please enter value") 
  }
}


  render() {
   const {isEditing} = this.state;
   const {handleSubmit} = this.props;
   let inputField = "";
   let dispalyvalue = ""; 
   if(this.props.type != "select"){      
      inputField =  (
      <Form inline>
        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">                         
        <Input
            name={this.props.name}
            defaultValue={this.props.defaultValue }
            placeholder={this.props.label}
            type={this.props.type}           
            readOnly={this.props.readOnly}
            onChange={this.handleChange}            
            required={this.props.required}           
            onBlur={this.props.onBlur}
          />
        </FormGroup>                       
        {/* <Button color="danger" onClick={(e)=>this.props.handleSubmit(this.props.submitid,this.props.name,this.state[this.props.name]?this.state[this.props.name]:alert("Please enter value"))}>Update</Button> */}
        <Button color="danger" onClick={()=>this.handleSubmit(this.props.submitid,this.props.name)}>Update</Button>
        <Button color="grey" onClick={this.handleCancelEdit} >Cancel</Button>
      </Form>
      )
    }

   if(this.props.type == "select"){      
      inputField =  (<Form inline>
        <FormGroup className="mb-2 mr-sm-2 mb-sm-0"> 
                           
        <Input          
             name={this.props.name}
             defaultValue={this.props.defaultValue }
             placeholder={this.props.label}
             type={this.props.type}           
             readOnly={this.props.readOnly}
             onChange={this.handleChange}            
             required={this.props.required}           
             onBlur={this.props.onBlur}
          >
          {this.props.children}
          </Input>
        </FormGroup>                       
        <Button color="danger" onClick={()=>this.handleSubmit(this.props.submitid,this.props.name)}>Update</Button>
        <Button color="grey" onClick={this.handleCancelEdit} >Cancel</Button>
      </Form>)
    }

    if(this.props.type == 'date'){
      dispalyvalue = moment(this.state[this.props.name]).format('DD-MMM-YYYY')
      
    }else if(this.props.type == 'select'){   
     
      dispalyvalue = this.state.SelecteText? this.state.SelecteText : this.props.defaultValue;
      
    }else{
      dispalyvalue = this.state[this.props.name]      
    }


    if(this.props.formattedValue){
     // console.log("formatted value", this.props.formattedValue);
      dispalyvalue = this.props.formattedValue;
      //console.log("dispalyvalue", dispalyvalue);
    }
    return (
      <React.Fragment>
       {isEditing ? 
        inputField       
      :
      <React.Fragment>
      { this.state[this.props.name]? 
          dispalyvalue 
        : 
        this.props.formattedValue? dispalyvalue : this.props.defaultValue}
      <a  onClick={this.handleEdit} className="ml-2" >
            <i className="fa fa-edit fa-lg mt-1 text-primary" />
      </a>
      </React.Fragment>
       }
        
        </React.Fragment>
    );
  }
}

