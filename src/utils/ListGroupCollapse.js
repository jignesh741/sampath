import React from 'react';
import { ListGroupItem, Collapse } from 'reactstrap';

class ListGroupCollapse extends React.Component {
  constructor(props) {
    super(props);
    
    this.toggle = this.toggle.bind(this);
    this.state = {collapse: false};
  }
  
  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }
  
  render() {
    const employee = this.props.employee;
    
    return (
      <ListGroupItem>        
          <p onClick={this.toggle}>
            <button>{employee.name}</button> 
          </p>
          <Collapse isOpen={this.state.collapse}>{employee.name}</Collapse>        
      </ListGroupItem>
    );
  }
}

export default ListGroupCollapse