import React, { Component } from 'react';

class AutoCompleteSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            suggestions: [],
        }
        this.renderSuggestions = this.renderSuggestions.bind(this);
    }
    componentDidMount() {
            if(this.props.defaultValue){
                //console.log("defaultValue CDM", this.props.defaultValue);
                let suggestions = [];
                if(this.props.optionsValues.length > 0){
                    //const regex = new RegExp(`^${this.props.defaultValue}`, 'i');
                    suggestions = this.props.optionsValues.filter(v => v.value == this.props.defaultValue);
                   // console.log("Testing..", suggestions)
                }
                this.setState({
                    text: suggestions.length > 0 ? suggestions[0].name : "",
                  //  suggestions
                }) 
            }
           
    }
    componentDidUpdate(prevProps, prevState) {
        if(prevProps.defaultValue !== this.props.defaultValue){
            let suggestions = [];
            if(this.props.optionsValues.length > 0){
                //const regex = new RegExp(`^${this.props.defaultValue}`, 'i');
                suggestions = this.props.optionsValues.filter(v => v.value == this.props.defaultValue);
                //console.log("Testing..", suggestions)
            }
            this.setState({
                text: suggestions,
              //  suggestions
            }) 
        }
         
    }
    onTextChange = e =>{
        const value = e.target.value.split("--")[0];
        console.log("TEXTVALUE:", value)
        let suggestions = [];
        if(value.length > 0){
           // const regex = new RegExp(`^${value}`, 'i');
            const regex = new RegExp(`${value}`, 'i');
            suggestions = this.props.optionsValues.sort().filter(v => regex.test(v.value));
            //console.log("Testing..", suggestions)
        }
        this.setState({
            suggestions,
            text: value
        },()=>console.log("TEXT:", this.state.text));
        
    }
    suggestionSelected = (e, value)=>{
        console.log("suggestionSelected", value);
        this.setState(()=>({
            text:value,
            suggestions: []
        }));
        // const regex = new RegExp(`^${value}`, 'i');
        // const selectedVal = this.props.optionsValues.sort().filter(v => regex.test(v.name));
        this.props.handleChange(e, value);
    }

    renderSuggestions = ()=>{
      
        if(this.state.suggestions.length === 0){
            return null;
        }else{
           // console.log("Here")
            return (
                <ul className="">
                    { this.state.suggestions.map((item) => <li key={item.value} onClick={(e)=>this.suggestionSelected(e, item.name)} className="autoCompletItems">{item.name}</li> )}
                </ul>
            )
        }
    }
    render() {
        const { optionsValues, defaultValue, required } = this.props;
        console.log("optionsValues", optionsValues);
        return (
            <div className="autoCompleteText">
                 <input  
                type="text" 
                placeholder="Type initial letters"
                onChange={ this.onTextChange } 
                value={ this.state.text }
                required={required}
                className="form-control"
                 />
                 { this.renderSuggestions() }
            </div>
        );
    }
}


export default AutoCompleteSelect;