import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

const AccessControl = ({  
  allowedPermissions,
  children,
  renderNoAccess
}) => {
  const permitted = checkPermissions( allowedPermissions);  
  if (permitted) {
    return children;
  }
  return renderNoAccess();
};

const checkPermissions = (allowedPermissions) => {
  const Permissions = localStorage.getItem('permissions');
  const PermissionsObject = JSON.parse(Permissions);
  let found=false;
 
 const got = allowedPermissions.map((key, value)=>{ 
  if(PermissionsObject[key]){     
     found=true;
     return found;
   }
   
})
return found;
}

function mapStateToProps(state) {
  return {
    userPermissions: state.auth.userpermissions,
  };
}

export default connect(mapStateToProps)(AccessControl);
