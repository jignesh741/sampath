import React, { Component } from 'react';
import {
    Row,
    Col,
    Form,
    Input,
    Button,
    Table,
    Badge,
    Alert,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
  } from "reactstrap";
  import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
  import { connect } from "react-redux";
  import moment from "moment";
  import Editable from "react-x-editable";
  import AccessControl from "../utils/accessControl";
  import Constants from "../config";
import { element } from 'prop-types';
import ParametersBox from "./parametersBox";

var localData = localStorage.getItem('userdata');
const userdata = JSON.parse(localData);
//const center = userdata.center[0].id;


class ConditionCreator extends Component {
    constructor(props) {
        super(props);
        this.state ={
        parameterKeys : [
        {value: "medicalHistory.diabetesTypeI", name: "Diabetes Type I"},
        {value: "medicalHistory.diabetesTypeII", name:  "Diabetes Type II"},
        {value: "medicalHistory.highBlood" , name: "High Blood"},
        {value: "medicalHistory.hypertension", name:"Hypertension"}, 
        {value: "medicalHistory.thyroidHyper", name:"ThyroidHyper"},
        {value: "medicalHistory.thyroidHypo", name:"ThyroidHypo"},
        {value: "centralNervous.nervousDiseaseDisease", name:"Nervous Disease"},
        {value: "centralNervous.panicAttack", name:"PanicAttack"},
        {value: "centralNervous.CNSdisorder", name:"CNSdisorder"},
        {value: "gastroIntenstinal.intenstineDisease", name:"Intenstine Disease"},
        {value: "gastroIntenstinal.liverDisease", name:"Liver Disease"},
        {value: "gastroIntenstinal.abdomenDisease", name:"Abdomen Disease" } 
      ],
      formGroups:[{step:1, params:[{value:""}], paramOperator: "", operator:""}] 
        }
    }
    

addStep = () =>{
  this.setState(prevState => ({ 
    formGroups: [...prevState.formGroups, { step: this.state.formGroups.length + 1, params:[{value:""}], operator:"" }]
  }),()=>console.log("AddStep", this.state.formGroups));
}
addMore = (groupstep )=>{
  let groups = [...this.state.formGroups];
    groups.map(pms=>{
      if(pms.step == groupstep){
        pms.params.push({value:""})
      }
    })
    this.setState({ formGroups: groups});
}
removeSelectedBox=(groupstep,i)=>{
  let groups = [...this.state.formGroups];
    console.log("groupstep", groupstep, "i", i);
    groups.map(pms=>{
      if(pms.step == groupstep){
        pms.params.splice(i, 1);
      }
    })
    this.setState({ formGroups: groups });
}
handleParamChange=(step, paramIndex, e)=>{
let groups = [...this.state.formGroups];
   // console.log("groupstep", step, "i", i);
   groups[step-1].params[paramIndex].value = e.target.value;
  
  this.setState({ formGroups: groups },()=>console.log("formGroup", this.state.formGroups));
}
handleOperatorChange =(step, e)=>{
let groups = [...this.state.formGroups];
   // console.log("groupstep", step, "i", i);
   groups[step-1].paramOperator = e.target.value;
  
  this.setState({ formGroups: groups },()=>console.log("formGroup", this.state.formGroups));
}
handleMainOperatorChange =(step, e)=>{
let groups = [...this.state.formGroups];
   // console.log("groupstep", step, "i", i);
   groups[step-1].operator = e.target.value;
  this.setState({ formGroups: groups },()=>console.log("formGroup", this.state.formGroups));
}
createUI(){
  let html = [];
 return this.state.formGroups.map((group, index)=>{
  let elementGroup = [];

    for(let i = 0; i < group.params.length; i++){   
      // elementGroup.push(<ParametersBox parameterKeys={this.state.parameterKeys} value="" key={i} group={group.step} addMoreBox={this.addMore} />)     
      elementGroup.push(<ParametersBox handleChange={this.handleParamChange} parameterKeys={this.state.parameterKeys} value={group.params[i].value} key={i} group={group.step} indexNumber={i} removeBox={this.removeSelectedBox} />)     
    }

    return(
      <React.Fragment>
      <div className="ml-2 d-flex">
        { index > 0 && 
        // <div className="d-flex align-self-center p-2">
        <div className="align-self-center">
          <Input
            type="select"
            name="operator"
            autoComplete="off"
            value={group.operator}
            onChange={(e)=>this.handleMainOperatorChange(group.step,e)}
            required="true"
          >
          <option value="">Select Operator</option>
          <option value="and">AND </option>
          <option value="or">OR </option>
          </Input>
        </div>
        // </div>
        }
      </div>
      
      <div className="flex-column align-items-center justify-content-start bg-white p-2 rounded ml-2 shadow"> 
      
      <div className="mb-2">
      <Input
        type="select"
        name="paramOperator"
        autoComplete="off"
        value={group.paramOperator}
        onChange={(e)=>this.handleOperatorChange(group.step,e)}
        required="true"
      >
      <option value="">Select Operator</option>
      <option value="and">AND </option>
      <option value="or">OR </option>
      </Input>
      </div>
        {  elementGroup.map(gr=>gr)}
        <div className="text-center">
           <Button className="bg-green font-weight-bold text-white" onClick={()=>this.addMore(group.step)}>Add more</Button>
        </div>
      </div>
    
    </React.Fragment>)
})
}

resetForm=()=>{
  this.setState({formGroups:[{step:1, params:[{value:""}], paramOperator: "", operator:""}] });
  this.props.handleFilterSubmitt(this.props.page, "");

}

handleStepChange = (e) =>{
  let steps = {...this.state.selectedStep, param: e.target.value };
  this.setState({selectedSteps: steps},()=>console.log("steps",steps));
}
handleSubmit =(e)=>{
  e.preventDefault();
  console.log("Submiting", this.state.formGroups);
 // console.log("this.props.page", this.props.page);
  this.props.handleFilterSubmitt(this.props.page, this.state.formGroups);
}

      render() {
          const { selectedSteps, paramSelectbox, formGroups , fieldGroups} = this.state;
        return (
            <Form onSubmit={this.handleSubmit} autoComplete="off">
           
            <div className="d-flex  p-2">
            {this.createUI()} 
            <div className="align-self-center flex-column align-items-center justify-content-center ml-4 border-left border-info p-2" >
                  {formGroups.length <= 1 && 
                   <div><Button className="bg-green font-weight-bold text-white" onClick={this.addStep}>Add Group</Button> </div>                     
                  } 
                    <div  className="p-2">
                        <Button className="bg-purple font-weight-bold text-white">Search</Button>
                    </div>
                    <div  className="p-2 ">
                        <Button className="bg-warning font-weight-bold text-white" onClick={this.resetForm}>Reset</Button>
                    </div>
                    
            </div>
            </div>
            
            </Form>
        );
    }
}



  
  const mapDispachToProps = dispatch => {
    return {
      // getPatientListRequest: (data) =>
      //   dispatch({ type: "GET_PATIENT_FILTERS_REQUEST", data: data }),
    };
  };
  
  export default connect(
    null,
    mapDispachToProps
  )(ConditionCreator);
