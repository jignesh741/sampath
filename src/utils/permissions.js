import React from 'react';
import { ListGroupItem, Collapse,Button, FormGroup,Input, Label, Col, Grid, ListGroup,Alert  } from 'reactstrap';
import {  CSSTransition, TransitionGroup} from 'react-transition-group';
import '../style.css';
import { connect } from "react-redux";



class Permissions extends React.Component {
  constructor(props) {
    super(props);
    
    this.toggle = this.toggle.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      collapse: false, 
      style:{display:'block'},
      formdata:{},
      dsrview:false,
      dsrenter:false,
      dsrfeedback:false,
      damview:false,
      damenter:false,
      damfeedback:false,
      hrview:false,
      hrenter:false,
      masterview:false,
      masterenter:false,
      reports:false,
      othersPipesheet:false,
      othersRouteplan:false,
      othersProposals:false,
    };
  }
  componentDidMount() {
    const permissions = this.props.employee.permissions[0];
      this.setState({
      dsrview:permissions.dsrview?true:false,
      dsrenter:permissions.dsrenter,
      dsrfeedback:permissions.dsrfeedback,
      damview:permissions.damview?true:false,
      damenter:permissions.damenter,
      damfeedback:permissions.damfeedback,
      hrview:permissions.hrview,
      hrenter:permissions.hrenter,
      masterview:permissions.masterview,
      masterenter:permissions.masterenter,
      reports:permissions.reports,
      othersPipesheet: permissions.othersPipesheet,
      othersRouteplan:permissions.othersRouteplan,
      othersProposals:permissions.othersProposals
    });
  }
  
  
  toggle() {
    this.setState({ collapse: !this.state.collapse, style:{display:'none'} });
  }
  handleChange = e => {
    this.setState({
    [e.target.name]: e.target.checked
    });
  }

  handleSubmit = e => {
      e.preventDefault();
      this.toggle();
      this.setState({  style:{display:'block'} });
     
      this.setState({  
        formdata:{      
        newpermissions:{ 
          dsrview:this.state.dsrview,
          dsrenter:this.state.dsrenter,
          dsrfeedback:this.state.dsrfeedback,
          damview:this.state.damview,
          damenter:this.state.damenter,
          damfeedback:this.state.damfeedback,
          hrview:this.state.hrview,
          hrenter:this.state.hrenter,
          masterview:this.state.masterview,
          masterenter:this.state.masterenter,          
          reports:this.state.reports,
          othersPipesheet: this.state.othersPipesheet,
          othersRouteplan:this.state.othersRouteplan,
          othersProposals:this.state.othersProposals,
          
        },
        employeeId:this.props.employee._id
        }},() => this.props.updateEmployeeRequest(this.state.formdata) );
       // console.log("formdata:"+JSON.stringify(this.state.formdata))
    //   this.props.updateEmployeeRequest(this.state.formdata);
  }
  
 
  render() {
 
    const {     
      employeeUpdateError,employeeUpdatSucess,employee } = this.props;
    
    return (
      <TransitionGroup className="todo-list">
      
      <CSSTransition
          key={employee._id}
          timeout={500}
          classNames="move"
        >
      <ListGroupItem>      
      
        <Button outline color="info" onClick={this.toggle} size="sm" style={this.state.style}>Show</Button>
       
        <Collapse isOpen={this.state.collapse} >        
            <FormGroup row>
                <Col md="3">
                    <Label>DSR</Label>
                </Col>
                <Col md="9">
                    <FormGroup check inline>
                    <Input className="form-check-input" type="checkbox" id="dsrview" name="dsrview"  defaultChecked={this.props.employee.permissions[0].dsrview} onChange={this.handleChange}/>
                    <Label className="form-check-label" check htmlFor="dsrview">View</Label>
                    </FormGroup>
                    <FormGroup check inline>
                    <Input className="form-check-input" type="checkbox" id="dsrenter" name="dsrenter" defaultChecked={this.props.employee.permissions[0].dsrenter}  onChange={this.handleChange}/>
                    <Label className="form-check-label" check htmlFor="dsrenter">Enter</Label>
                    </FormGroup>
                    <FormGroup check inline>
                    <Input className="form-check-input" type="checkbox" id="dsrfeedback" name="dsrfeedback" defaultChecked={this.props.employee.permissions[0].dsrfeedback}  onChange={this.handleChange}/>
                    <Label className="form-check-label" check htmlFor="dsrfeedback">Feedback</Label>
                    </FormGroup>
                    <FormGroup check inline>
                    <Input className="form-check-input" type="checkbox" id="othersPipesheet" name="othersPipesheet" defaultChecked={this.props.employee.permissions[0].othersPipesheet}  onChange={this.handleChange}/>
                    <Label className="form-check-label" check htmlFor="othersPipesheet">Others Pipesheet</Label>
                    </FormGroup>
                    <FormGroup check inline>
                    <Input className="form-check-input" type="checkbox" id="othersRouteplan" name="othersRouteplan" defaultChecked={this.props.employee.permissions[0].othersRouteplan}  onChange={this.handleChange}/>
                    <Label className="form-check-label" check htmlFor="othersRouteplan">Others Routeplan</Label>
                    </FormGroup>
                    <FormGroup check inline>
                    <Input className="form-check-input" type="checkbox" id="othersProposals" name="othersProposals" defaultChecked={this.props.employee.permissions[0].othersProposals}  onChange={this.handleChange}/>
                    <Label className="form-check-label" check htmlFor="othersProposals">Others Proposals</Label>
                    </FormGroup>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Col md="3">
                    <Label>DAM</Label>
                </Col>
                <Col md="9">
                    <FormGroup check inline>
                    <Input className="form-check-input" type="checkbox" id="damview" name="damview"  defaultChecked={this.props.employee.permissions[0].damview} onChange={this.handleChange}/>
                    <Label className="form-check-label" check htmlFor="damview">View</Label>
                    </FormGroup>
                    <FormGroup check inline>
                    <Input className="form-check-input" type="checkbox" id="damenter" name="damenter" defaultChecked={this.props.employee.permissions[0].damenter}  onChange={this.handleChange}/>
                    <Label className="form-check-label" check htmlFor="damenter">Enter</Label>
                    </FormGroup>
                    <FormGroup check inline>
                    <Input className="form-check-input" type="checkbox" id="damfeedback" name="damfeedback" defaultChecked={this.props.employee.permissions[0].damfeedback}  onChange={this.handleChange}/>
                    <Label className="form-check-label" check htmlFor="damfeedback">Feedback</Label>
                    </FormGroup>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Col md="3">
                    <Label>HR</Label>
                </Col>
                <Col md="9">
                    <FormGroup check inline>
                    <Input className="form-check-input" type="checkbox" id="hrview" name="hrview" defaultChecked={this.props.employee.permissions[0].hrview}  onChange={this.handleChange}/>
                    <Label className="form-check-label" check htmlFor="hrview" >View</Label>
                    </FormGroup>
                    <FormGroup check inline>
                    <Input className="form-check-input" type="checkbox" id="hrenter" name="hrenter" defaultChecked={this.props.employee.permissions[0].hrenter}  onChange={this.handleChange} />
                    <Label className="form-check-label" check htmlFor="hrenter">Enter</Label>
                    </FormGroup>                   
                </Col>
            </FormGroup>
            <FormGroup row>
                <Col md="3">
                    <Label>Master</Label>
                </Col>
                <Col md="9">
                    <FormGroup check inline>
                    <Input className="form-check-input" type="checkbox" id="masterview" name="masterview"  defaultChecked={this.props.employee.permissions[0].masterview} onChange={this.handleChange}/>
                    <Label className="form-check-label" check htmlFor="masterview">View</Label>
                    </FormGroup>
                    <FormGroup check inline>
                    <Input className="form-check-input" type="checkbox" id="masterenter" name="masterenter"  defaultChecked={this.props.employee.permissions[0].masterenter} onChange={this.handleChange}/>
                    <Label className="form-check-label" check htmlFor="masterenter">Enter</Label>
                    </FormGroup>                   
                </Col>
            </FormGroup>
            <FormGroup row>
                <Col md="3">
                    <Label>Reports</Label>
                </Col>
                <Col md="9">
                    <FormGroup check inline>
                    <Input className="form-check-input" type="checkbox" id="reports" name="reports"  defaultChecked={this.props.employee.permissions[0].reports} onChange={this.handleChange}/>
                    <Label className="form-check-label" check htmlFor="reports">View</Label>
                    </FormGroup>                                       
                </Col>
            </FormGroup>
            <Button color="info" onClick={this.handleSubmit} size="sm" >Save</Button>
        </Collapse>        
      </ListGroupItem>
      </CSSTransition>
      </TransitionGroup>
    );
  }
}
const mapStateToProps = state => {
  return {
    employeeUpdatError: state.employee.employeeUpdatError,
    employeeUpdatSucess: state.employee.employeeUpdatSucess,
  };
};

const mapDispachToProps = dispatch => {
  return {
    updateEmployeeRequest: data =>
      dispatch({ type: "UPDATE_EMPLOYEE_REQUEST", formdata: data }) ,
     
  };
}
export default connect(
  mapStateToProps,
  mapDispachToProps
)(Permissions);
