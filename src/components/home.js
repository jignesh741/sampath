import React, { Component, Suspense } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import {
    Row,
    Col,
    Form,
    Input,
    Button,
    Table,
    Badge,
    Alert,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Card,
    CardBody,
    NavLink
  } from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";

const CenterList = React.lazy(() => import("./centerlist"));

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {location:null}
  }
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );
  
  componentDidMount() {
   // this.props.getLocationListRequest();
  }
  componentDidUpdate(prevProps) {
    if (this.props.centerList) {
      // this.props.history.push("/dashboard");
      console.log("Component did update");
    }
  }
  
  render() {
    const { centerAddError, centerAddSucess, locationList } = this.props;
    return (
     
        <Row className="animated fadeIn">
            
        <Col xs="12" sm="6" lg="3">
            <NavLink href="/#/Regmenu"> 
            <Card className="text-white bg-1">
              <CardBody className="">
                <div className="dashboard-title-1">
                <div className="text-value text-center"><h2>Registration</h2></div></div>
              </CardBody>
            </Card>
            </NavLink>
          </Col>
          <Col xs="12" sm="6" lg="3">
          <NavLink href="/#/patientList"> 
            <Card className="text-white bg-2">
              <CardBody className="">
              <div className="dashboard-title-2">
                <div className="text-value text-center"><h2>Patient View</h2></div></div>
              </CardBody>
            </Card>
            </NavLink>
          </Col>
        <Col xs="12" sm="6" lg="3">
        <NavLink href="/#/bills"> 
            <Card className="text-white bg-1">
              <CardBody className="">
                <div className="dashboard-title-1">
                <div className="text-value text-center"><h2>Billing</h2></div></div>
              </CardBody>
            </Card>
            </NavLink>
          </Col>
          <Col xs="12" sm="6" lg="3">
          <NavLink href="/#/payments"> 
            <Card className="text-white bg-2">
              <CardBody className="">
              <div className="dashboard-title-2">
                <div className="text-value text-center"><h2>Payments</h2></div></div>
              </CardBody>
            </Card>
            </NavLink>
          </Col>
        </Row>
    
    );
  }
}

const mapStateToProps = state => {
  return {
    // locationList: state.location.locationList,
    // centersAddSucess: state.center.centersAddSucess,
    // centersAddError: state.center.centersAddError
  };
};

const mapDispachToProps = dispatch => {
  return {
    // getLocationListRequest: (data) =>
    //   dispatch({ type: "GET_LOCATIONLIST_REQUEST", locationIn: data }),
    // addCenterRequest: data =>
    //   dispatch({ type: "ADD_CENTER_REQUEST", formdata: data })
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(Home);
