import React, { Component, Suspense } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import { Row, Col, Form, Input, Button, Table, Badge, Alert } from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const Modalities = React.lazy(() => import("./Modalities"));

class Modality extends Component {
  constructor(props) {
    super(props);
    this.state = {location:null}
  }
  loading = () => (
    <div className="animated fadeIn pt-1 text-modality">Loading...</div>
  );
  handleChange = e => {
    console.log(e.target.name)
    this.setState({
      [e.target.name]: e.target.value
    });
  };
 
  handleSubmit = e => {
    e.preventDefault();
    const formdata = {
      modality: this.state.modality,
      description: this.state.description,     
    };
    console.log("SubmitData=" + JSON.stringify(formdata));
    this.props.addModalityRequest(formdata);
  };
  componentDidMount() {
   // this.props.getLocationListRequest();
  }
  componentDidUpdate(prevProps) {
    if(this.props.modalityAddSucess){
        toast.success("Data updated successfuly");  
        this.props.resetAddRequest();      
      }
      if(this.props.modalityAddError){
        toast.warning("Something Wrong. Please try again.");  
        this.props.resetAddModalityErrorRequest();
      }
  }
  
  render() {
    const { modalityAddError, modalityAddSucess, locationList } = this.props;
    return (
      <div className="animated fadeIn">
           <ToastContainer autoClose={2000} />
        <Row>
          <Col md="6">
            <div className="card">
              <div className="card-header">
                <i className="icon-drop" />
                Add Modality
              </div>
              <div className="card-body">
              
                <Form onSubmit={this.handleSubmit}>
                  <Input
                    type="text"
                    placeholder="Enter Modality"
                    autoComplete="Modality"
                    name="modality"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  <Input
                    type="textarea"
                    placeholder="Enter Modality Description"
                    autoComplete="modality-description"
                    name="description"
                    onChange={this.handleChange}
                    className="mb-2"
                  />
                  <Row>
                    <Col>
                      <Button color="primary" className="px-4 mt-2">
                        Add Modality
                      </Button>
                    </Col>
                  </Row>
                </Form>
              </div>
            </div>
          </Col>
          </Row>
          <Row>
          <Col md="12">
            <div className="card">
              <div className="card-header">
                <i className="icon-drop" />
                Modality List
              </div>
              <div className="card-body">
                <Suspense fallback={this.loading()}>
                  <Modalities />
                </Suspense>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    modalityAddSucess: state.modality.modalityAddSucess,
    modalityAddError: state.modality.modalityAddError
  };
};

const mapDispachToProps = dispatch => {
  return {
    addModalityRequest: data =>
      dispatch({ type: "ADD_MODALITY_REQUEST", formdata: data }),
      resetAddRequest: () =>
      dispatch({ type: "RESET_ADD_MODALITY_SUCCESS_ASYNC" }),
      resetAddModalityErrorRequest: () =>
      dispatch({ type: "RESET_ADD_MODALITY_FAILURE_ASYNC" }),
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(Modality);
