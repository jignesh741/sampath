import React, { Component } from "react";
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Table,
  Badge,
  Alert,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import moment from "moment";
import Editable from "react-x-editable";
import AccessControl from "../utils/accessControl";
import { Link } from 'react-router-dom'
import Constants from "../config";
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import PaginationComponent from "react-reactstrap-pagination";
import ConditionCreator from "../utils/conditionCreator";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {Animated} from "react-animated-css";

var localData = localStorage.getItem('userdata');
const userdata = JSON.parse(localData);
const center = null;

class SearchPatientCounts extends Component {
  constructor(props) {
    super(props);
    this.toggleDanger = this.toggleDanger.bind(this);
    this.handleSelected = this.handleSelected.bind(this);

    this.state = {
      modal: false,
      danger: false,
      delid: "",
      selectedPage: 1,
      allIds:[],
      checkall: false,
      primary: false,
    };
  }

  handleSelected(selectedPage, filterStr = this.state.filterStr) {
    console.log("selectedPage",selectedPage, "filterStr", filterStr);

    this.setState({ selectedPage: selectedPage, filterStr: filterStr }, () => {
     // console.log("filterStr", filterStr);
      const data = {
        //executive: userdata._id,
        center : center,
        page: this.state.selectedPage ? this.state.selectedPage: 1,
        limit: Constants.PAGE_ITEM_SIZE,
        filterStr: this.state.filterStr? this.state.filterStr : {},
      };
      this.props.getPatientListRequest(data);
    });
  }
  

componentDidMount() {
 const data = {
  //executive: userdata._id,
  center: center,
  filterStr: {}
  }
  //this.props.getPatientListRequest(data);
}
  componentDidUpdate(prevProps) {
    if (prevProps.patientList !== this.props.patientList) {
      // this.props.history.push("/dashboard");
      let patientArray = [];
      console.log("Component did update");
      if(this.props.patientList.docs && this.props.patientList.docs.length > 0){
        this.props.patientList.docs.map(patient=>{
          patientArray.push({id:patient._id, isChecked: false})
        });
      }
     console.log("patientArray", patientArray);
    }
  }
  
  toggleDanger = delid => {
    this.setState({
      danger: !this.state.danger,
      delid: delid
    });
  };
  
  togglePrimary=() =>{
    this.setState({
      primary: !this.state.primary
    });
  }
  ResultModal = pid => {
    this.togglePrimary();
    this.props.getReportData(pid);
    this.setState({ pid: pid });
  };

  render() {
    const { patientList, reportData } = this.props;
    console.log("reportData", reportData);
    return (
    <div className="p-2 rounded" >
      
              <ToastContainer autoClose={2000} />
              
    <div className="font-weight-bold bg-white p-2 d-flex justify-content-between">
       <div>Query Filters </div>
       
    </div>
      <div className="bg-secondary d-flex justify-content-between">
      <ConditionCreator   handleFilterSubmitt= {this.handleSelected}  page={this.state.selectedPage ? this.state.selectedPage: 1}/>
      <div className="align-self-center flex-column align-items-center justify-content-center ml-4  p-2">
      {patientList.totalDocs? <div className="rounded-circle bg-white shadow" style={{height:"120px",width:"120px", lineHeight:"120px"}}><div style={{fontSize: "2.2em"}} className="text-center">{patientList.totalDocs}</div></div> : null}
        </div>
      </div>
      <Table responsive  className="bg-white border" >
        <thead className="bg-primary">
          <tr>
            <th className="text-center">ID</th>
            <th className="text-center">Ref. Number</th>
            <th className="text-center">Patient</th>
            <th className="text-center">DOB</th>
            <th className="text-center">Gender</th>
            <th className="text-center">Email</th>
            <th className="text-center">Mobile</th>
            <th className="text-center">Consent</th>
            {/* <th className="text-center">Data Transfer</th>
            <th className="text-center">Action</th> */}
          </tr>
        </thead>
        <tbody>
          { patientList.docs && !patientList.docs.length > 0
            ? <tr><td colSpan="9" className="text-center text-dark" style={{height: "100px", lineHeight:"100px"}}>Search parameters not matching any data. Please try again with different criteria</td></tr>
            : patientList.docs &&  patientList.docs.map((patient, idx) => {
                const myid = patient._id;
                // console.log("this.state.allIds[myid]", this.state.allIds[myid]);
                // let checkedMe = false;
                // if(this.state.allIds.includes(myid) ){
                //   checkedMe = true;
                // }
                return (
                  <tr key={idx}>
                    <td>
                    { patient.pensieveid } 
                    </td>
                    <td>
                    { patient.refbarcode } 
                    </td>
                    <td>
                    { patient.name } 
                    </td>
                    <td className="text-center">{ patient.dob? moment(patient.dob).format("DD-MM-YYYY") : ""}</td>
                    <td className="text-center">{ patient.gender }</td>
                    <td className="text-center">{ patient.email }</td>
                    <td className="text-center">{ patient.mobile }</td>
                    <td className="text-center">{ 
                      patient.consentForm && patient.consentForm.map((consent, i) => {
                        var hrefurl = "https://pensievehealth.com:10000/static/"+consent;
                        return( <div className="d-flex mb-2" key={i}><div><a href={hrefurl} download target="_blank">Consent</a></div>
                        {/* <div className="ml-2 border text-center bg-red text-white" style={{width:"20px", height:"23px"}} onClick={()=>this.confirmDelete(patient._id, consent)}>X</div> */}
                         </div>)
                      })
                     }</td>
                    {/* <td>
                    <div className="justify-content-left ">
                      { patient.reportstatus == 1? "Waiting for confirmation": "Confirmed"}
                    </div> 
                    </td>
                    <td>
                    { patient.reportTransferred == 2 ?
                    <div className="justify-content-left ">
                    <Button
                      className="bg-pink border border-warning text-white"
                      onClick={() =>
                        this.ResultModal(myid)
                      }
                      className="mr-1 mb-2 block"
                    >
                    <i className="fa fa-eye" styles={{fontSize:"36px", backgroundColor:"green"}}></i>
                    </Button>

                    </div> 
                    :
                    ""
                  }
                    </td> */}
                  </tr>
                );
              })}
        </tbody>
      </Table>
      <PaginationComponent
                  totalItems={patientList.totalDocs}
                  pageSize={Constants.PAGE_ITEM_SIZE}
                  onSelect={this.handleSelected}
                  maxPaginationNumbers={10}
                  activePage={patientList.page}
                  
                />
      <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={"modal-primary " + this.props.className} size="lg">
            <ModalHeader toggle={this.togglePrimary}>Report Data: Imported from Spectrum</ModalHeader>
        <ModalBody>
          {reportData && reportData.map(report =>{
            var keyName = Object.keys(report.result);
            var valueName = Object.values(report.result);
            // console.log("Key Length",keyName.length);
            // console.log("Values Length",valueName.length);
            return (
              <div className="mb-4 bg-light p-2 shadow rounded">
              <div className="border-bottom d-flex justify-content-between align-items-center"> <h4 className="text-center text-primary "> Test : {report.test_name}</h4><small>Date: {moment(report.resultDate).format("DD-MM-YYYY")}</small></div>
              <div className="border-bottom d-flex justify-content-between align-items-center p-2"><strong>Parameter Name</strong><strong>Result Value</strong></div>
              {
                keyName.map((key,i)=>{
                  return ( <div className="d-flex justify-content-between align-items-center p-2 border-bottom" key={i}><div className="min-vw-50">{key.replace(/_/g, ' ')}:</div><div>{valueName[i]}</div></div>)
                })
              }
              </div>
            )
          })}
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.togglePrimary}>Close</Button>{' '}
        </ModalFooter>
      </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    patientList: state.patient.patientList,
    confirmed: state.patient.confirmed,
    reportData: state.patient.reportData,
  };
};

const mapDispachToProps = dispatch => {
  return {
    getPatientListRequest: (data) =>
        dispatch({ type: "GET_PATIENT_FILTERS_REQUEST", data: data }),
    confirmMany: (data) =>dispatch({ type: "CONFIRM_MANY_REQUEST", data: data }),
    resetConfirmationFlag: (data) =>dispatch({ type: "RESET_CONFIRM_DATA_TRANSFER_SUCCESS_ASYNC" }),
    getReportData: (id) =>dispatch({ type: "GET_REPORT_DATA_REQUEST", data: id }),

  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(SearchPatientCounts);
