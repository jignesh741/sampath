import React, { Component, Suspense } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import { Row, Col, Form, Input, Button, Table, Badge, Alert, Card, CardHeader,CardBody } from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import moment from "moment";
import { Redirect } from 'react-router-dom'


const CenterList = React.lazy(() => import("./centerlist"));
//const CenterManagerlist = React.lazy(() => import("./centerManagerlist"));
//const PatientList = React.lazy(() => import("./patientList"));

class PatientRegistration extends Component {
    intervalID = 0;
  constructor(props) {
    super(props);
    this.state = {location:null, center: localStorage.getItem("center")}
  }
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  
  handleSubmit = e => {
    e.preventDefault();
    const formdata = {
      centerId: this.state.center,
      fname: this.state.fname,
      mname: this.state.mname,
      lname: this.state.lname,
      gender: this.state.gender,
      dob: this.state.dob,
      age: this.state.age,
      email: this.state.email,
      mobile: this.state.mobile,      
      aadhar: this.state.aadhar,    
      enteredBy: localStorage.getItem("userid")  
    };
    console.log("Formdata", formdata);
    this.props.addPatientRequest(formdata);
  };
  componentDidMount() {
  }
  componentDidUpdate(prevProps, prevState) {
    console.log("componentDidUpdate");
    if(prevState.dob !== this.state.dob){
        var age = moment().diff(this.state.dob, 'years');
        console.log("DOB changed",age);
        this.setState({age: age > 0 ? age : "0"});
      }
    if (this.props.patientAddSucess == true) {
        toast.info("Patient Registered Succesfuly");  
        this.props.resetAddPatientSuccessRequest();
      // console.log("patientId", this.props.patientId);
       this.intervalID = setInterval(()=>this.props.history.push({
        pathname:"/PatientBilling",
        state:{
          patid:this.props.patientId,
          pname: this.state.fname+" "+this.state.mname+" "+this.state.lname,
         }
       }), 2000 );
    }
    if (this.props.patientAddError ) {
        toast.warning("Something Wrong. Please try again.");  
        this.props.resetAddPatientFailRequest();
    }
  }
  componentWillUnmount(){
    clearInterval(this.intervalID);
  }
  render() {
    const { locationList, centerList } = this.props;
    return (
      <div className="animated fadeIn">
          <ToastContainer autoClose={2000} />
          <Card sm="6" md="6" style={{ width:"50%"}} >
          <CardHeader>Enter Patient Details</CardHeader>
          <CardBody>
          <Form onSubmit={this.handleSubmit} autoComplete="off">
            <div className="form-group row">
              <label htmlFor="name" className="col-sm-3 col-form-label">First Name:</label>
              <div className="col-sm-9">
                <Input type="text" className="form-control" name="fname" placeholder="First Name" onChange={this.handleChange} required="required" />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="name" className="col-sm-3 col-form-label">Middle Name:</label>
              <div className="col-sm-9">
                <Input type="text" className="form-control" name="mname" placeholder="Middle Name" onChange={this.handleChange} required="required" autoComplete="new-mname" />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="name" className="col-sm-3 col-form-label">Last Name:</label>
              <div className="col-sm-9">
                <Input type="text" className="form-control" name="lname" placeholder="Last Name" onChange={this.handleChange} required="required" autoComplete="new-mname" />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="gender" className="col-sm-3 col-form-label">Gender:</label>
              <div className="col-sm-9">
                <Input type="select" name="gender" className="form-control" onChange={this.handleChange} required="required" autoComplete="new-gender" >
                  <option value="">Please Select</option>
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
                 </Input>
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="dob" className="col-sm-3 col-form-label">DOB:</label>
              <div className="col-sm-9">
                <Input type="date" className="form-control" name="dob" placeholder="DOB" onChange={this.handleChange}   autoComplete="new-date" />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="age" className="col-sm-3 col-form-label">Age:</label>
              <div className="col-sm-9">
                <Input type="number" className="form-control" name="age" min="0" max="110" placeholder="Age" value={this.state.age? this.state.age: ""}  onChange={this.handleChange} autoComplete="new-date" />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="email" className="col-sm-3 col-form-label">Email:</label>
              <div className="col-sm-9">
                <Input type="email" className="form-control" name="email" placeholder="Email" onChange={this.handleChange}  autoComplete="new-date" />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="mobile" className="col-sm-3 col-form-label">Mobile:</label>
              <div className="col-sm-9">
                <Input type="text" className="form-control" name="mobile" placeholder="Mobile" onChange={this.handleChange}   autoComplete="new-date" />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="aadhar" className="col-sm-3 col-form-label">Addhar Number:</label>
              <div className="col-sm-9">
                <Input type="text" className="form-control" name="aadhar" placeholder="Aadhar" onChange={this.handleChange}   autoComplete="new-aadhar" />
              </div>
            </div>
           
            <div className="form-group row">
            <Button color="primary" size="lg" className="mb-4 btn-block" >Register Patient </Button>
            </div>
           </Form>   
          </CardBody>
          </Card>

        
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    locationList: state.location.locationList,
    // centerList: state.center.centerList,
    patientId: state.patient.patientId,
    patientAddSucess: state.patient.patientAddSucess,
    patientAddError: state.patient.patientAddError
  };
};

const mapDispachToProps = dispatch => {
  return {
    addPatientRequest: data =>dispatch({ type: "ADD_PATIENT_REQUEST", formdata: data }),
    resetAddPatientSuccessRequest: ()=>dispatch({ type: "RESET_PATIENT_SUCESS_REQUEST"}),
    resetAddPatientFailRequest: ()=>dispatch({ type: "RESET_PATIENT_FAIL_REQUEST"}),
      
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(PatientRegistration);
