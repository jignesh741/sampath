import React, { Component } from 'react';
import { Row, Col, Form, Input, Button, Table, Badge, Alert, Card, CardHeader,CardBody } from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AppointmentPicker from 'react-appointment-picker';

import moment from "moment";

class PatientScheduling extends Component {
    constructor(props) {
        super(props);
        const query = new URLSearchParams(this.props.location.query);
        const orderInfo = JSON.parse(query.get("orderInfo"));
        const scanInfo = orderInfo.scan;
        console.log("orderInfo", orderInfo);
        this.state = {
           scanInfo: scanInfo,
           loading: false,
           days:[],
           initailDayStr: "",
           orderInfo: orderInfo,
        }
    }
    componentDidMount() {
        let formdata = {
            center: localStorage.getItem("center"),
            scan: this.state.scanInfo
        };
        this.props.roomListRequest(formdata);
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.roomList !== prevProps.roomList){
        if(this.props.roomList.roomArryFilter.length > 0){
            let allrooms = [];
            this.props.roomList.roomArryFilter.map(room=>{
                allrooms.push(room.rooms)
            });
            allrooms = allrooms.flat();
            this.setState({
                counts: this.props.roomList.counts,
                allrooms: allrooms
                })
        }
        }
    }

   addAppointmentCallback = ({ day, number, time, id }, addCb) => {
    this.setState(
      {
        loading: true,
      },
      async () => {
        await new Promise(resolve => setTimeout(resolve, 2500));
        console.log(
          `Added appointment ${number}, day ${day}, time ${time}, id ${id}`
        );
        addCb(day, number, time, id);
        this.setState({ loading: false });
      }
    );
  };

  addAppointmentCallbackContinuousCase = (
    { day, number, time, id },
    addCb,
    params,
    removeCb
  ) => {
    this.setState(
      {
        loading: true,
      },
      async () => {
        if (removeCb) {
          await new Promise(resolve => setTimeout(resolve, 1250));
          console.log(
            `Removed appointment ${params.number}, day ${params.day}, time ${params.time}, id ${params.id}`
          );
          removeCb(params.day, params.number);
        }
        await new Promise(resolve => setTimeout(resolve, 1250));
        console.log(
          `Added appointment ${number}, day ${day}, time ${time}, id ${id}`
        );
        addCb(day, number, time, id);
        this.setState({ loading: false });
      }
    );
  };
  removeAppointmentCallback = ({ day, number, time, id }, removeCb) => {
    this.setState(
      {
        loading: true,
      },
      async () => {
        await new Promise(resolve => setTimeout(resolve, 2500));
        console.log(
          `Removed appointment ${number}, day ${day}, time ${time}, id ${id}`
        );
        removeCb(day, number);
        this.setState({ loading: false });
      }
    );
  };

  getDays=(room)=>{
      if(room == null && this.props.roomList){
          console.log("Default ROOM:", )
      }
  }

  handleModRoomChange = (e)=>{
    console.log("e.target.name", e.target.name,  e.target.value);
    if(e.target.value == ""){
      return;
    }
    const filterRoom = this.state.allrooms.filter(room=>room.roomName == e.target.value);
    console.log("filterRoom", filterRoom);
   let todayTime = moment().format("HH:mm");
   let startTime = moment(filterRoom[0].startTime, "HH:mm");
   let endTime = moment(filterRoom[0].endTime, "HH:mm");
   let initailDay = moment().format("YYYY-MM-DD");
   let initailDayStr = initailDay +" "+ filterRoom[0].startTime;
   //let initialDayStart = moment(initailDayStr).format("YYYY-MM-DD HH:mm");
   console.log("initailDayStr", initailDayStr);

    let i = 0;
    do{
      startTime = moment(startTime, "HH:mm").add(filterRoom[0].scheduleInterval,"minutes");
        i++;
        console.log("todayTime", moment(startTime).format("HH:mm"));
    }
    while(moment(startTime, "HH:mm").isBefore(endTime, "HH:mm"));

    let newDays = [];    
    //console.log("newDays",newDays);
    for(let s=0; s <= 5; s++){
      let setdays = [];
      for(let j = 0; j<=i; j++){
        setdays.push({id: s+j, number: j, isSelected: true, periods: 1});
      }
      newDays[s]= setdays;
    }
    console.log("newDays",newDays);
    this.setState({ days: newDays, initailDayStr: initailDayStr, roomSelected:e.target.value,  [e.target.name]: e.target.value});
    
  }

render() {
    const {roomList} = this.props;
    const {allrooms} = this.state;
    console.log("roomList", roomList);
    console.log("allrooms", allrooms);
    console.log("this.state.roomSelected", this.state.roomSelected);
    console.log("this.state.orderInfo", this.state.orderInfo);
return (
  
    <div  className="animated fadeIn">
    <div className="bg-light-blue p-2">
    <h5>Scheduling {this.state.orderInfo.patientId.fname+" "+this.state.orderInfo.patientId.mname+" "+this.state.orderInfo.patientId.lname}</h5>
    <div className="">SCAN: 
    {this.state.orderInfo.scan.map(sc=>{
         return <span className="p-2"><i>{sc}</i></span>
       })}
    </div>
    </div>
    <div className="d-md-flex align-items-center justify-content-start bg-white shadow rounded">
        {
        roomList.roomArryFilter && roomList.roomArryFilter.map((modRooms, index)=>{
          return (
            <div className="p-2" key={index}>
                <label htmlFor="name" >Select {modRooms.modality} Room:</label>
                <Input
                type="select"
                name="modRoom"          
                autoComplete="new-modRoom"
                onChange={this.handleModRoomChange}
                >
                <option value="">Please Select</option>   
                {
                   modRooms.rooms.map(room=>{
                   return <option value={room.roomName}>{room.roomName}</option>
                   }) 
                }
                </Input>
            </div>
          )  
        })
        }
       
    </div>
    <div className="animated fadeIn mt-2 bg-white border">
      { this.state.days.length > 0 && 
      <>
      {/* <p>{this.state.initailDayStr} </p> */}
      <AppointmentPicker      
      addAppointmentCallback={this.addAppointmentCallbackContinuousCase}
      removeAppointmentCallback={this.removeAppointmentCallback}
      //initialDay={new Date('2020-06-02 9:30')}
      initialDay={new Date(this.state.initailDayStr)}
      unitTime={1800000}
      days={this.state.days}
      maxReservableAppointments={2}
      alpha={true}
      visible={true}
      selectedByDefault={false}
      loading={this.state.loading}
      continuous={true}
      key={this.state.roomSelected}
    />

    </>
      }
    </div>  
    </div>
        );
    }
}

const mapStateToProps = state => {
    return {
    roomList: state.schedule.roomList,
    existingSchedules: state.schedule.existingSchedules
    };
  };
  
  const mapDispachToProps = dispatch => {
    return {
        roomListRequest: data =>dispatch({ type: "MODALITY_ROOM_LIST_REQUEST", formdata: data }),
        getExistingSchedule: data =>dispatch({ type: "GET_EXISTING_SCHEDULE_REQUEST", formdata: data }),
    };
  };
  
  export default connect(
    mapStateToProps,
    mapDispachToProps
  )(PatientScheduling);