import React, { Component } from 'react';
import { Row, Col, Form, Input, Button, Table, Badge, Alert, Card, CardHeader,CardBody } from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Calendar, momentLocalizer, Views } from 'react-big-calendar';
import "react-big-calendar/lib/css/react-big-calendar.css";
import moment from "moment";
const localizer = momentLocalizer(moment)


class Scheduling extends Component {
    constructor(props) {
        super(props);
        this.state = {events:[]}
    }
    handleSelect = ({ start, end }) => {
        const title = window.prompt('Procedure Name')
        if (title)
          this.setState({
            events: [
              ...this.state.events,
              {
                start,
                end,
                title,
              },
            ],
          })
      }


    render() {
        return (
            <div  className="animated fadeIn">
                <h5>Scheduling MR</h5>
                <Calendar
                selectable
                localizer={localizer}
                events={this.state.events}
                defaultView={Views.DAY}
                scrollToTime={new Date(1970, 1, 1, 6)}
                defaultDate={new Date()}
                //onSelectEvent={event => alert(event.title)}
                onSelectEvent={event => console.log("Event Selected")}
                onSelectSlot={this.handleSelect}
                views={['week', 'day', 'agenda']}
                />
                <div className="mt-4">
                <h5>Scheduling CT</h5>
                <Calendar
                selectable
                localizer={localizer}
                events={this.state.events}
                defaultView={Views.DAY}
                scrollToTime={new Date(1970, 1, 1, 6)}
                defaultDate={new Date()}
                onSelectEvent={event => alert(event.title)}
                onSelectSlot={this.handleSelect}
                views={['week', 'day', 'agenda']}
                />
                </div>
            </div>
        );
    }
}

export default Scheduling;