import React, { Component, Suspense } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import { Row, Col, Form, FormGroup,Input, Button,Label, Table, Badge, Alert, Card, CardHeader,CardBody } from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


const CenterList = React.lazy(() => import("./centerlist"));
//const CenterManagerlist = React.lazy(() => import("./centerManagerlist"));
const UserList = React.lazy(() => import("./userlist"));

class EditUser extends Component {
  constructor(props) {
    super(props);
    this.state = {center:[]};
  }
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );
 
  handleChange = e => {
    var targetName = e.target.id;  

    if( e.target.checked === true){           
      if(this.state.center.length > 0){
         // var joined = this.state.center.push({id: e.target.id});
          this.setState({ center: [ ...this.state.center,  {id: e.target.id}] , 
            [e.target.id]:e.target.checked,
          });
      }else{          
          this.setState({
          center: [ ...this.state.center,  {id: e.target.id}],
              [e.target.id]:e.target.checked,
          })
      }       
  }
  if( e.target.checked === false){   
      this.setState({      
          [e.target.id]: e.target.checked,
          center: this.state.center.filter(function(fact) { 
            console.log("Matched",e.target.id);
          return fact.id !== e.target.id
      })});
  } 
  if(e.target.name){
    this.setState({
      [e.target.name]:e.target.value,
  })
  }
  
  };

  handleSubmit = e => {
    e.preventDefault();
    if(this.state.center.length == 0){
      alert("Please select at least one center.");
      return false;
    }
    const formdata = {
        id: this.state.id,
        fname: this.state.fname,
        mname: this.state.mname,
        lname: this.state.lname,
        degree: this.state.degree, 
        designation: this.state.designation,  
        mobile: this.state.mobile, 
        email: this.state.email,
        password: this.state.password, 
        signature: this.state.signature,
        roles: this.state.roles,
        centers: this.state.center
    };
    console.log("formdata",formdata);
   this.props.updateUserRequest(formdata);
  };
  componentDidMount() {
    const query = new URLSearchParams(this.props.location.search);
    const id = query.get("id");
    this.setState({           
        id:id
        });
    this.props.getUserFromId(id);
    this.props.getCenterListRequest();
    
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.userData !== this.props.userData) {
        this.setState({
            // name:this.props.userData.name,
            // email:this.props.userData.email,
            // mobile:this.props.userData.mobile,
            // designation:this.props.userData.designation,
            // fname: this.state.fname,
        fname: this.props.userData.fname,
        mname: this.props.userData.mname,
        lname: this.props.userData.lname,
        degree: this.props.userData.degree, 
        designation: this.props.userData.designation,  
        mobile: this.props.userData.mobile, 
        email: this.props.userData.email,
        signature: this.props.userData.signature,
        roles: this.props.userData.roles,
       
        });
        var factories = [];
        
        this.props.userData.centers.map(fact=>{
           
            if(fact.id){
                console.log("centerID:",fact.id._id);
                factories.push({id: fact.id._id});
                this.setState({
                    [fact.id._id] : true,
                })
            }
            
        })
        
        this.setState({
            center : factories, 
        },()=>console.log("center:",this.state.center))

    }
    if (this.props.userUpdateSucess == true) {
        toast.info("User Updated Succesfuly");  
        this.props.resetUpdateUserSuccessRequest();
        var j =0;

        this.props.history.push("/userlist");
    }
    if (this.props.userUpdateError ) {
        toast.warning("Something Wrong. Please try again.");  
        this.props.resetUpdateUserFailRequest();
    }
  }
  
  render() {
    const { centerList } = this.props;
    return (
      <div className="animated fadeIn">
          <ToastContainer autoClose={2000} />
          <Card>
          <CardHeader>Edit User</CardHeader>
          <CardBody>
          <Form onSubmit={this.handleSubmit}>
          <Row>
            <Col md="6">
              <Row>
              <Col md="12" >
                    <Input
                    type="text"
                    placeholder="Enter First Name"
                    autoComplete="new-fname"
                    name="fname"
                    onChange={this.handleChange}
                    value={this.state.fname? this.state.fname : ""}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
                  <Col md="12" >
                    <Input
                    type="text"
                    placeholder="Enter Middle Name"
                    autoComplete="new-mname"
                    name="mname"
                    onChange={this.handleChange}
                    value={this.state.mname? this.state.mname : ""}
                    className="mb-2"
                    required="required"
                  />
                </Col>

                  <Col md="12" >
                    <Input
                    type="text"
                    placeholder="Enter Last Name"
                    autoComplete="new-lname"
                    name="lname"
                    onChange={this.handleChange}
                    value={this.state.lname? this.state.lname : ""}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
                  <Col md="12" >
                    <Input
                    type="text"
                    placeholder="Enter Degree"
                    autoComplete="new-degree"
                    name="degree"
                    onChange={this.handleChange}
                    value={this.state.degree? this.state.degree : ""}
                    className="mb-2"
                    required="required"
                  />
                  </Col>  
                  <Col md="12" >
                  <Input
                    type="text"
                    placeholder="Enter Designation"
                    autoComplete="designation"
                    name="designation"
                    value={this.state.designation? this.state.designation : ""}
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
              
                  <Col md="12" >
                  <Input
                    type="text"
                    placeholder="Enter Mobile Number"
                    autoComplete="mobile"
                    name="mobile"
                    value={this.state.mobile? this.state.mobile : ""}
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
             
                  
                  
                  <Col md="12" >
                  <Input
                    type="select"
                    autoComplete="new-roles"
                    name="roles"
                    value={this.state.roles ? this.state.roles : ""}
                    value={this.state.roles? this.state.roles : ""}
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  >
                    <option value="">Select Role</option>
                    <option value="frontdesk">Front Desk</option>
                    <option value="physician">Physician</option>
                    <option value="radiologist">Radiologist</option>
                    <option value="technician">Technician</option>
                    <option value="radiologist">Typist</option>
                  </Input>
                  </Col>
                  <Col md="12" >
                  <Input
                    type="text"
                    placeholder="Enter Email "
                    autoComplete="email"
                    name="email"
                    value={this.state.email? this.state.email : ""}
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
              </Row>
              </Col>
              <Col md="6" className="pl-4">
                <h4>Center</h4>
              {!centerList.length > 0
                  ? ""
                  : centerList.map((center, idx) => {
                      return (
                      <React.Fragment key={idx}>
                        {/* <FormGroup> */}
                         <span className="p-4"> 
                         <Input type="checkbox" id={center._id} onChange={this.handleChange} checked={ this.state[center._id] ? this.state[center._id] : false}  />{center.center}</span>
                        {/* </FormGroup> */}
                      </React.Fragment>
                      );
                  })}
               
              </Col>
            </Row>
            <Row>
            <Col md="12">
                  <Button color="primary" className="px-4 mt-2">
                        Update User
                      </Button>
                  </Col>
            </Row>
           </Form>   
          </CardBody>
          </Card>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    centerList: state.center.centerList,
    userData: state.user.userData,
    userUpdateSucess: state.user.userUpdateSucess,
    userUpdateError: state.user.userUpdateError
  };
};

const mapDispachToProps = dispatch => {
  return {
    getUserFromId: (Id) =>dispatch({ type: "GET_USER_REQUEST", Id: Id }),
    updateUserRequest: data =>dispatch({ type: "UPDATE_USER_REQUEST", formdata: data }),
    resetUpdateUserSuccessRequest: ()=>dispatch({ type: "RESET_UPDATE_USER_SUCESS_REQUEST"}),
    resetUpdateUserFailRequest: ()=>dispatch({ type: "RESET_UPDATE_USER_FAIL_REQUEST"}),
    getCenterListRequest: (locationId) =>dispatch({ type: "GET_CENTERLIST_REQUEST", data: locationId }),
      
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(EditUser);
