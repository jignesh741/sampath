import React, { Component } from 'react';
import { Row, Col, Form, Input, Button, Table, Badge, Alert, Card, CardHeader,CardBody } from "reactstrap";
import { connect } from "react-redux";
import { Redirect, Link } from "react-router-dom";
import moment from "moment";
import Constants from "../config";
import PaginationComponent from "react-reactstrap-pagination";
import FilterFieldsPatients from "../utils/filterFieldsPatients";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {Animated} from "react-animated-css";

const role = localStorage.getItem("roles");
console.log("Role Role", role)
class OrderList extends Component {
    constructor(props) {
        super(props);
        const centerCode = localStorage.getItem("centerCode");
        this.state = {
          centerCode: centerCode,
           selectedPage: 1,
           filterStr: {}
          }
    }
    componentDidMount() {
       const formdata = {
        centerId: localStorage.getItem("center"),
        filterStr: {}
        }
        this.props.orderListRequest(formdata);
    }
    schedulePatient = (patId,regId,orderId, scan, regDispId, dispPatId, dispOrderId)=>{
      const formdata = {
        patId: patId,
        regId: regId,
        orderId:orderId,
        scan:scan,
        centerId: localStorage.getItem("center"),
        enteredBy: localStorage.getItem("userid"),  
        regDispId: regDispId,
        dispPatId: dispPatId,
        dispOrderId: dispOrderId
      }
      this.props.sendPatientSchedule(formdata);
    }
    componentDidUpdate(prevProps, prevState) {
      if(this.props.scheduleSuccess === true){
        toast.success("Patient Schedule Successfuly");  
        this.props.resetPatientSchedule();
        const formdata = {
          centerId: localStorage.getItem("center"),
          filterStr: {}
          }
          this.props.orderListRequest(formdata);
      }
    }
    // handleSelected=(selectedPage, filterStr = this.state.filterStr) => {
    //   this.setState({ selectedPage: selectedPage, filterStr: this.state.filterStr }, () => {
    //     const data = {
    //      // executive: userdata._id,
    //       center : localStorage.getItem("center"),
    //       page: this.state.selectedPage ? this.state.selectedPage: 1,
    //       limit: Constants.PAGE_ITEM_SIZE,
    //       filterStr: this.state.filterStr? this.state.filterStr : {},
    //     };
    //     this.props.orderListRequest(data);
    //   });
    // }
    handleSelected = (selectedPage, filterStr = this.state.filterStr)=>{
      this.setState({ selectedPage: selectedPage, filterStr: filterStr }, () => {
        const data = {
         // executive: userdata._id,
         centerId: this.state.centerId,
          page: this.state.selectedPage ? this.state.selectedPage: 1,
          limit: Constants.PAGE_ITEM_SIZE,
          filterStr: this.state.filterStr? this.state.filterStr : {},
        };
        this.props.orderListRequest(data);
      });
    }
render() {
    const {orderList} = this.props;
    return (
        <div className="animated fadeIn">
        <ToastContainer autoClose={2000} />
        <h4>Order List</h4>
        <div className="text-right mr-2">Total Results: {orderList.totalDocs? orderList.totalDocs: null}</div>
        <div className="bg-secondary d-flex justify-content-between">
        <FilterFieldsPatients handleFilterSelected={this.handleSelected} />
        </div>
        <Table responsive striped  className="bg-white border text-center" style={{verticalAlign: "middle"}}>
        <thead className="bg-primary">
          <tr>
            <th className="text-center">Date</th>
            <th className="text-center">Order ID</th>
            <th className="text-center">Reg.Id</th>
            <th className="text-center">Patient Id</th>
            <th className="text-left">Patient</th>
            <th className="text-left">Modalities</th>
            <th className="text-left">Scan</th>
            <th className="text-center">Schedule Action</th>
          </tr>
        </thead>
        <tbody>
        { orderList.docs && !orderList.docs.length > 0
            ? <tr><td colSpan="9" className="text-center text-dark" style={{height: "100px", lineHeight:"100px"}}>Search parameters not matching any data. Please try again with different criteria</td></tr>
            : orderList.docs &&  orderList.docs.map((order, idx) => {
                const myid = order._id;
                //console.log("order.scheduleStatus", order.scheduleStatus)
                return (
                    <tr key={idx} style={{whiteSpace: "nowrap"}}>
                      <td style={{width:"100px"}} className="text-left">
                      { moment(order.createDate).format("DD-MM-YYYY h:m A") }
                      </td>
                      <td>
                      { this.state.centerCode+"-"+order.orderid } 
                      </td>
                      <td>
                      { this.state.centerCode+"-"+order.regDispId } 
                      </td>
                      <td>
                      { this.state.centerCode+"-"+order.dispPatId } 
                      </td>
                      <td className="text-left">
                      { order.patient[0].fname+" "+order.patient[0].mname+" "+order.patient[0].lname } 
                      </td>
                      <td className="text-left">
                      { order.modalities.join(", ") } 
                      </td>
                      <td className="text-left">
                      { 
                      //order.scan.join(", ") 
                        order.bodyparts.length > 0 && order.bodyparts.map((item, i)=>{
                            return(<div>{item.billingcode}--{item.bodypart}</div>)
                        })
                      } 
                      </td>
                      <td>
                     { order.scheduleStatus == true && 
                  //    <Link
                  //    to={{
                  //      pathname: "/viewSchedule",
                  //      query: {
                  //        orderInfo: JSON.stringify(order)
                  //      },
                  //    }}
                  //  >
                  //   <Button outline color="primary" size="sm"><i className="fa fa-check"></i></Button>
                  //  </Link>
                   <i className="fa fa-check fa-lg"></i>
                    }
                    { !order.scheduleStatus && role != "technician" && role != "radiologist" &&
                    // <Button color="warning" size="sm" className="btn-block" onClick={()=>this.schedulePatient(order.patientId._id,order.regId,order._id,  order.scan, order.regDispId,order.patientId.patientId,order.orderid )}>Schedule</Button>
                        order.scheduleStatus === false  && <Button outline color="primary" size="sm"><Link 
                          to={{
                            pathname: "/PatientScheduling",
                            query: {
                              orderInfo: JSON.stringify(order)
                            },
                          }}
                        >
                          <i className="fa fa-clock-o fa-lg" aria-hidden="true"> Schedule</i>                        
                        </Link></Button>
                      
                      
                    //   <Link
                  //   to={{
                  //     pathname: "/scheduling",
                  //     query: {
                  //       orderInfo: JSON.stringify(order)
                  //     },
                  //   }}
                  // >
                  //  <i className="fa fa-clock-o fa-lg"></i>
                  // </Link>
                    }
                  
                     {/* { order.scheduleStatus === false  && <p>[<Link
                          to={{
                            pathname: "/schedule",
                            query: {
                              orderInfo: JSON.stringify(order)
                            },
                          }}
                        >
                         <i className="fa fa-eye fa-lg">Schedule</i>
                        </Link>]</p>
                      } */}
                      </td>

                      </tr>
                      )
            
            })}
        </tbody>
        </Table>
        <PaginationComponent
        totalItems={orderList.totalDocs}
        pageSize={Constants.PAGE_ITEM_SIZE}
        onSelect={this.handleSelected}
        maxPaginationNumbers={10}
        activePage={orderList.page}
        />
       
        </div>
    );
    }
}

const mapStateToProps = state => {
    return {
     orderList: state.order.orderList,
     scheduleSuccess: state.schedule.scheduleSuccess
    };
  };
  
  const mapDispachToProps = dispatch => {
    return {
        orderListRequest: data =>dispatch({ type: "ORDER_LIST_REQUEST", formdata: data }),
        sendPatientSchedule: data =>dispatch({ type: "SEND_PATIENT_SCHEDULE_REQUEST", formdata: data }),
        resetPatientSchedule: () =>dispatch({ type: "RESET_SCHEDULE_SUCCESS_ASYNC"}),
    };
  };
  
  export default connect(
    mapStateToProps,
    mapDispachToProps
  )(OrderList);