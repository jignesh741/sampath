import React, { Component } from 'react';
import { Row, Col, Form, Input, Button, Table, Badge, Alert, Card, CardHeader,CardBody, Modal,
  ModalHeader,
  ModalBody,
  ModalFooter, } from "reactstrap";
import { connect } from "react-redux";
import moment from "moment";
import Constants from "../config";
import PaginationComponent from "react-reactstrap-pagination";
import FilterFieldsPatients from "../utils/filterFieldsPatients";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {Animated} from "react-animated-css";
import  ReactToPdf from 'react-to-pdf';
const ref = React.createRef();
const options = {
    orientation: 'landscape',
    unit: 'in',
    format: [4,2]
};

class Payments extends Component {
    constructor(props) {
        super(props);
        const centerCode = localStorage.getItem("centerCode");
        this.state={ 
          centerCode: centerCode,
          selectedPage: 1,
          filterStr: {},
          modal: false,
        }
    }
    componentDidMount() {
       const formdata = {
        centerId: localStorage.getItem("center"),
        filterStr: {}
        }
        this.props.paymentsRequest(formdata);
    }
    // handleSelected=(selectedPage, filterStr = this.state.filterStr) => {
    //   this.setState({ selectedPage: selectedPage, filterStr: this.state.filterStr }, () => {
    //     const data = {
    //      // executive: userdata._id,
    //       center : localStorage.getItem("center"),
    //       page: this.state.selectedPage ? this.state.selectedPage: 1,
    //       limit: Constants.PAGE_ITEM_SIZE,
    //       filterStr: this.state.filterStr? this.state.filterStr : {},
    //     };
    //     this.props.paymentsRequest(data);
    //   });
    // }
    handleSelected = (selectedPage, filterStr = this.state.filterStr)=>{
      this.setState({ selectedPage: selectedPage, filterStr: filterStr }, () => {
        const data = {
         // executive: userdata._id,
         centerId: this.state.centerId,
          page: this.state.selectedPage ? this.state.selectedPage: 1,
          limit: Constants.PAGE_ITEM_SIZE,
          filterStr: this.state.filterStr? this.state.filterStr : {},
        };
        this.props.paymentsRequest(data);
      });
    }
    openPaymentModal =(paymentDetails)=>{
      console.log("paymentDetails", paymentDetails)
      this.setState({
        modal: true,
        paymentDetails: paymentDetails,
      })
    }
    toggle =()=>{
      this.setState({
        modal: !this.state.modal
      })
    }
render() {
    const {payments} = this.props;
    return (
      <div className="animated fadeIn">
        <div>
        <ToastContainer autoClose={2000} />
        <h4>Payments</h4>
        <div className="text-right mr-2">Total Results: {payments.totalDocs? payments.totalDocs: null}</div>
        <div className="bg-secondary d-flex justify-content-between">
        <FilterFieldsPatients handleFilterSelected={this.handleSelected} />
        </div>
        <Table responsive striped  className="bg-white border" >
        <thead className="bg-primary">
          <tr>
            <th className="text-center">Patient Id</th> 
            <th className="text-center">Patient</th>
            <th className="text-left">Payment Date</th>
            <th className="text-left">Reg. Id</th>
            <th className="text-left">Payment Id</th>           
            <th className="text-left">Bill Id</th>
            <th className="text-center">Received Amount</th>
            <th className="text-center">Outstanding</th>
            <th className="text-center">Payment Mode</th>
            {/* <th className="text-center">Total Charges</th> */}
            <th className="text-center">Action</th> 

          </tr>
        </thead>
        <tbody>
        { payments.docs && !payments.docs.length > 0
            ? <tr><td colSpan="10" className="text-center text-dark" style={{height: "100px", lineHeight:"100px"}}>Search parameters not matching any data. Please try again with different criteria</td></tr>
            : payments.docs &&  payments.docs.map((payment, idx) => {
                const myid = payments._id;
                return (
                    <tr key={idx}>
                       <td>
                      { this.state.centerCode+"-"+payment.patient[0].patientId  } 
                      </td>
                      <td>
                      { payment.patient[0].fname+" "+payment.patient[0].mname+" "+payment.patient[0].lname } 
                      </td>
                      <td style={{width:"100px"}}  style={{whiteSpace: "nowrap"}}>
                      { moment(payment.paymentDateTime).format("DD-MM-YYYY h:m A")}
                      </td>
                      <td>
                      { this.state.centerCode+"-"+payment.regDispId } 
                      </td>
                      <td>
                      { this.state.centerCode+"-"+payment.paymentid } 
                      </td>
            
                      <td>
                      { this.state.centerCode+"-"+payment.dispBillId } 
                      </td>
                      <td className="text-center">
                      { payment.receivedAmount } 
                      </td>
                      <td className="text-center">
                      { payment.outstanding?  payment.outstanding: 0} 
                      </td>
                      <td className="text-center">
                      { payment.mode } 
                      </td>
                      {/* <td>
                      { payment.totalCharges } 
                      </td>*/}
                      <td>
                     <Button onClick={()=>this.openPaymentModal(payment)}>Receipt</Button>
                      </td> 

                      </tr>
                      )
            
            })}
        </tbody>
        </Table>
        <PaginationComponent
        totalItems={payments.totalDocs}
        pageSize={Constants.PAGE_ITEM_SIZE}
        onSelect={this.handleSelected}
        maxPaginationNumbers={10}
        activePage={payments.page}
        />
        </div>
        <Modal isOpen={this.state.modal} toggle={this.toggle} style={{width:"80%"}} size="lg" >
        <ModalHeader toggle={this.toggle}>Billing Details</ModalHeader>
        <ModalBody>
        <ReactToPdf filename="Receipt.pdf">
        {({toPdf, targetRef}) =>  (
            <div style={{width: "100%", height: 500, background: 'white', marginTop:"10px"}} onClick={toPdf} ref={targetRef}>
            <div className="text-center mt-2"><h1>SAKSHAM HEALTHCARE</h1></div>
            <div className="text-center">Amanora Business Plaza, 4th Floor, Office No. 421, Maharashtra 411028</div>
            <div className="text-center mt-2"><h3>Payment Receipt</h3></div>
            <div style={{marginLeft:"20px"}}>
            <div className="d-flex justify-content-between mt-4 ml-2" >
              <div>Patient Name: { this.state.paymentDetails.patient[0].fname+" "+this.state.paymentDetails.patient[0].mname+" "+this.state.paymentDetails.patient[0].lname }  </div>
              <div>Patient ID:  { this.state.centerCode+"-"+this.state.paymentDetails.patient[0].patientId  }</div>
            </div> 
            <div className="d-flex justify-content-between mt-0 ml-2" >
              <div>Receipt Number: { this.state.centerCode+"-"+this.state.paymentDetails.paymentid }  </div>
              <div>Date:  { moment(this.state.paymentDetails.paymentDateTime).format("DD-MM-YYYY h:m A")}</div>
            </div> 
            <div className="d-flex justify-content-between mt-0 ml-2" >
              <div>Bill Number: {this.state.centerCode+"-"+this.state.paymentDetails.dispBillId} </div>
              <div>Bill Date: {moment(this.state.paymentDetails.billDateTime).format("DD MMM YYYY")}</div>
            </div> 
            <div className="d-flex justify-content-center mt-1 ml-2" >
              <div>Amount Received: Rs.{this.state.paymentDetails.receivedAmount} </div>
            </div> 
            {this.state.paymentDetails.outstanding > 0 && 
            <div className="d-flex justify-content-center mt-1 ml-2" >
              <div>Outstanding: Rs.{this.state.paymentDetails.outstanding} </div>
            </div> 
            }
            </div>
            <div style={{marginLeft:"20px"}}>
            <Table responsive  className="bg-white border" >
            <thead className=""  style={{backgroundColor: "#53BCE2", color:"white"}}>
              <tr>
                <th className="text-center" >Scan Details</th>
                <th className="text-center">Amount in Rs.</th>
              </tr>
              </thead>
              <tbody className="text-center">
              <tr>
                <td>{ this.state.paymentDetails.billInfo.billingCodes.join(", ")  }</td>
                <td>{ this.state.paymentDetails.billInfo.scanAmount  }</td>
              </tr>
              {this.state.paymentDetails.billInfo.discount > 0 &&
              <tr>
                <td>Discount</td>
                <td>{ this.state.paymentDetails.billInfo.discount  }</td>
              </tr>
              }
              {this.state.paymentDetails.billInfo.additionalCharges > 0 &&
              <tr>
                <td>Additional Charges</td>
                <td>{ this.state.paymentDetails.billInfo.additionalCharges  }</td>
              </tr>
              }
               <tr className="font-weight-bold">
                <td>Total Charges</td>
                <td>{ this.state.paymentDetails.billInfo.totalCharges  }</td>
              </tr>
              </tbody>
            </Table>
            </div>
            </div>
        )}
      </ReactToPdf>
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.toggle}>Close</Button>
        </ModalFooter>
      </Modal> 
      </div>
    );
    }
}

const mapStateToProps = state => {
    return {
     payments: state.payment.payments,
    };
  };
  
  const mapDispachToProps = dispatch => {
    return {
        paymentsRequest: data =>dispatch({ type: "PAYMENTS_REQUEST", formdata: data }),
    };
  };
  
  export default connect(
    mapStateToProps,
    mapDispachToProps
  )(Payments);