import React, { Component } from 'react';
import { connect } from "react-redux";
import { Row, Col, Form, Input, Button, Table, Badge, Alert, Card, CardHeader,CardBody } from "reactstrap";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


class ModalityConfig extends Component {
    constructor(props) {
        super(props);
       
        const query = new URLSearchParams(this.props.location.query);
        const centerInfo = JSON.parse(query.get("center"));
        const centerId = centerInfo? centerInfo._id : "";
        const modalities= centerInfo? centerInfo.modalities : ""
        console.log("modalities",modalities)
        if(modalities.length == 0){
            alert("Please select at least one modality");
            this.props.history.push("/center")
        }
        this.state= {
            center: centerInfo,
            centerId: centerId,
            modalities: modalities,
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.modalitySettingsSucess){
            toast.info("Modality Settings Save Successfuly");
            this.props.resetModalitySettingFlag();
        }
    }

    setData = (e, index)=>{
        let modalities = Object.assign([],this.state.modalities);
        console.log("name", e.target.name)
        modalities[index][e.target.name] = e.target.value;
        this.setState({
            modalities: modalities
        })
        console.log("modalities", modalities)
    }
    handleSubmit =e=>{
        e.preventDefault();
        const formdata = {
            centerId: this.state.centerId,
            modalities: this.state.modalities
        }
        console.log("Formdata:", formdata);
        this.props.setModalitySettings(formdata);
    }

    handleChange = (e, index, roomInx) => {
        console.log("e.target.value", e.target.value)
        const selectedValue = e.target.value;
        let tempModalities = Object.assign([], this.state.modalities);
        if(tempModalities[index]["rooms"][roomInx] == undefined){
            tempModalities[index]["rooms"][roomInx] = {roomName:"",scheduleInterval:"", startTime:"",lunchTime:"", endTime:"", weeklyHoliday:"", active: true}
        }
        tempModalities[index]["rooms"][roomInx][e.target.name] = selectedValue;
       // tempModalities[index]["rooms"][roomInx] = selectedValue;
        console.log("tempModalities", tempModalities);
        this.setState({modalities: tempModalities});
    }
////////////////drawing modality room and schedul fields ///////////////////////
addRoomFieldsBox =(index)=>{   
    let tempModalities = Object.assign([], this.state.modalities);
    tempModalities[index]["rooms"].push({roomName:"",scheduleInterval:"", startTime:"", lunchTime:"", endTime:"", weeklyHoliday:"", active: true});
    this.setState({modalities: tempModalities});
}
removeRoomFieldsBox = (index, roomIdx)=>{
    let tempModalities = Object.assign([], this.state.modalities);
        tempModalities[index].rooms.splice(roomIdx, 1);
        this.setState({modalities: tempModalities});
}
 drawModalityFields =() =>{
    //console.log("this.state.modalities",this.state.modalities)
     if(this.state.modalities.length > 0 ){
       const modalityFields = this.state.modalities.map((modality, index)=>{
            return (
                <React.Fragment key={modality.modality+index}>
                 <div className="bg-primary p-1 border" >
                <div className="d-flex justify-content-between align-items-center">
            <div> Modality: {modality.modality}</div> 
                 <div><Button className="" color="info" onClick={()=>this.addRoomFieldsBox(index)}><i className="fa fa-plus"></i> Add Room </Button></div> 
                </div>                 
                 </div>                 
                {  modality.rooms && modality.rooms.map((rooms, roomIndx)=>{                    
                return (
                    <React.Fragment key={modality.modality+"rooms"+roomIndx}>                              
                    <div className="d-inline-flex bg-secondary justify-content-between align-items-center w-100 p-2 animated fadeIn">  
                    <div className="p-2 ">Room Name.:</div>                    
                    <div className="p-2 ">
                    <Input 
                    type="text" 
                    className="form-control" 
                    name="roomName" 
                    value={ rooms.roomName? rooms.roomName: "" } 
                    required="required" 
                    autoComplete="new-roomName"
                    onChange={(e)=>this.handleChange(e,index, roomIndx)} 
                        /></div>                  
                    <div className="p-2 ">Schedule Interval:</div>                    
                    <div className="p-2 ">
                    <Input 
                    type="text" 
                    className="form-control" 
                    name="scheduleInterval" 
                    value={rooms.scheduleInterval? rooms.scheduleInterval: "" } 
                    required="required" 
                    autoComplete="new-scheduleInterval" 
                    onChange={(e)=>this.handleChange(e,index, roomIndx)} 
                    //onBlur={(e)=>this.setData(e,index)}
                        /></div>

                    <div className="p-2 ">Start Time:</div>
                    <div className="p-2">
                    <Input 
                    type="text" 
                    className="form-control" 
                    name="startTime" 
                    value={rooms.startTime? rooms.startTime: ""} 
                    required="required" 
                    autoComplete="new-startTime" 
                    onChange={(e)=>this.handleChange(e,index, roomIndx)} 
                    //onBlur={(e)=>this.setData(e,index)}
                        /></div>
                    <div className="p-2 ">Lunch Time:</div>
                    <div className="p-2">
                    <Input 
                    type="text" 
                    className="form-control" 
                    name="lunchTime" 
                    value={rooms.lunchTime? rooms.lunchTime: ""}
                    required="required" 
                    autoComplete="new-lunchTime" 
                    onChange={(e)=>this.handleChange(e,index, roomIndx)} 
                    //onBlur={(e)=>this.setData(e,index)}
                        /></div>
                    <div className="p-2 ">End Time:</div>
                    <div className="p-2">
                        <Input 
                    type="text" 
                    className="form-control" 
                    name="endTime" 
                    value={rooms.endTime? rooms.endTime: ""} 
                    required="required" 
                    autoComplete="new-endTime" 
                    onChange={(e)=>this.handleChange(e,index, roomIndx)} 
                    //onBlur={(e)=>this.setData(e,index)}
                        /></div>
                    <div className="p-2 ">Weekly Holiday:</div>
                    <div className="p-2">
                    <Input 
                    type="select" 
                    className="form-control" 
                    name="weeklyHoliday" 
                    onChange={(e)=>this.handleChange(e,index, roomIndx)} 
                    value={rooms.weeklyHoliday? rooms.weeklyHoliday: ""}  
                    autoComplete="new-weeklyHoliday" 
                    //onBlur={(e)=>this.setData(e,index)}
                        >
                        <option value="">Please Select</option>
                        <option value="Sunday">Sunday</option>
                        <option value="Monday">Monday</option>
                        <option value="Tuesday">Tuesday</option>
                        <option value="Wednesday">Wednesday</option>
                        <option value="Thursday">Thursday</option>
                        <option value="Friday">Friday</option>
                        <option value="Saturday">Saturday</option>
                    </Input>     
                    </div> 
                    <div className="p-2 ">Active:</div>
                    <div className="p-2">
                    <Input 
                    type="select" 
                    className="form-control" 
                    name="active" 
                    onChange={(e)=>this.handleChange(e,index, roomIndx)} 
                    value={rooms.active? rooms.active: ""}  
                    autoComplete="new-weeklyHoliday" 
                    //onBlur={(e)=>this.setData(e,index)}
                        >
                        <option value="">Please Select</option>
                        <option value="true">Active</option>
                        <option value="false">Inactve</option>                       
                    </Input>     
                    </div> 
                    { roomIndx == 0 &&
                    <div><Button className="" color="info" onClick={()=>this.addRoomFieldsBox(index)}><i className="fa fa-plus"></i> </Button></div>
                    } 
                    { roomIndx > 0 &&
                    <div><Button className="" color="warning" onClick={()=>this.removeRoomFieldsBox(index, roomIndx)}><i className="fa fa-minus"></i></Button></div> 
                    } 
                                       
                </div>
                <div className="bg-warning">{" "}</div>
                </React.Fragment>
                     )
                 })
                 
                }
               </React.Fragment>
            )
        })
        return modalityFields;
     } 
    
 } 
 
    render() {
        const { modalities } = this.state;
        return (
            <div className="animated fadeIn">
                <h3>Modality Configuration</h3>
                <small>NOTE: Schedule Interval in Minutes; Use 24 hour clock to set Start and End Time. Enter 0 (zero) in Lunch Time if not applicable. </small>
                <ToastContainer autoClose={2000} />
                <Form onSubmit={this.handleSubmit} autoComplete="off">
                    <div className="w-100">
                        { this.drawModalityFields() }
                    </div>
                 <div className="pt-2 mb-2">
                <Button color="warning" size="sx" className="col-sm-4" >Save Modality Settings</Button>
                </div>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        centerList: state.center.centerList,
        modalitySettingsSucess: state.modality.modalitySettingsSucess,
        modalitySettingsError: state.modality.modalitySettingsError,
    };
  };
  
  const mapDispachToProps = dispatch => {
    return {
        setModalitySettings: data =>dispatch({ type: "SEND_MODALITY_SETTINGS_REQUEST", formdata: data }),
        resetModalitySettingFlag: () =>dispatch({ type: "RESET_MODALITY_SETTINGS_FLAG_REQUEST" }),
    };
  };
  
  export default connect(
    mapStateToProps,
    mapDispachToProps
  )(ModalityConfig);