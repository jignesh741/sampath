import React, { Component } from "react";
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Table,
  Badge,
  Alert,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import moment from "moment";
import Editable from "react-x-editable";
import AccessControl from "../utils/accessControl";
import { Link } from 'react-router-dom'
import Constants from "../config";
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import PaginationComponent from "react-reactstrap-pagination";
import FilterFieldsPatients from "../utils/filterFieldsPatients";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {Animated} from "react-animated-css";

var localData = localStorage.getItem('userdata');
const userdata = JSON.parse(localData);
const center = null;

class PatientList extends Component {
  constructor(props) {
    super(props);
    this.toggleDanger = this.toggleDanger.bind(this);
    this.handleSelected = this.handleSelected.bind(this);
    const centerCode = localStorage.getItem("centerCode");

    this.state = {
      modal: false,
      danger: false,
      delid: "",
      selectedPage: 1,
      allIds:[],
      checkall: false,
      primary: false,
      centerCode: centerCode
    };
  }

  handleSelected(selectedPage, filterStr = this.state.filterStr) {
    this.setState({ selectedPage: selectedPage, filterStr: filterStr }, () => {
      const data = {
       // executive: userdata._id,
        center : center,
        page: this.state.selectedPage ? this.state.selectedPage: 1,
        limit: Constants.PAGE_ITEM_SIZE,
        filterStr: this.state.filterStr? this.state.filterStr : {},
      };
      this.props.getPatientListRequest(data);
    });
  }
  
  // handleFilterSelected(filterdata) {
  //   this.setState({ selectedPage: 1 }, () => {
  //     const data = {
  //       center : center,
  //       page: this.state.selectedPage,
  //       limit: Constants.PAGE_ITEM_SIZE,
  //       filterStr: filterdata ? filterdata : "",
  //     };
  //     this.props.getPatientListRequest(data);
  //   });
  // }
  RegisterPatient = (patientId,dispPatId)=>{
    const formdata = {
      patientId: patientId,
      dispPatId: dispPatId,
      centerId: localStorage.getItem("center")
    }
    this.props.registerPatient(formdata);
  }
componentDidMount() {
 const data = {
  //executive: userdata._id,
  center: center,
  filterStr: {}
  }
  this.props.getPatientListRequest(data);
}
  componentDidUpdate(prevProps) {
    if (prevProps.patientList !== this.props.patientList) {
      // this.props.history.push("/dashboard");
      let patientArray = [];
      console.log("Component did update");
      if(this.props.patientList.docs && this.props.patientList.docs.length > 0){
        this.props.patientList.docs.map(patient=>{
          patientArray.push({id:patient._id, isChecked: false})
        });
      }
     this.setState({
      allIds: patientArray
     },()=>console.log("allIds", this.state.allIds))
     console.log("patientArray", patientArray);
    }

    if(prevProps.confirmed){
      toast.success("Data transfere confirmed successfuly"); 
      this.props.resetConfirmationFlag(); 
    }
    if(this.props.registrationSuccess){
      toast.success("Patient Registered successfuly"); 
      this.props.resetRegistrationFlag(); 
      this.intervalID = setInterval(()=>this.props.history.push({
        pathname:"/directRegBilling",
        state:{
          patid:this.props.patientId,
          pname: this.props.pname,
          regId: this.props.regId,
         }
       }), 2000 );
    }
  }
  handleChange = e => {
    const formdata = {
      patient: e.value,
      id: e.props.uniqid
    };
    //console.log("Formdata" + JSON.stringify(formdata));
    if (e.value == "") {
      alert("Please enter valid location");
      return false;
    }
    this.props.updateLocationRequest(formdata);
  };
  

  toggleDanger = delid => {
    this.setState({
      danger: !this.state.danger,
      delid: delid
    });
  };
  confirmDelete = (pid, doc) => {
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <div className='text-danger p-2'>
            <h1>Are you sure?</h1>
            <p>You want to delete this consent?</p>
            <button onClick={onClose} className="btn-primary">No</button>
            <button onClick={() => {
                this.handleClickDelete(pid, doc)
                onClose()
            }} className="btn-danger">Yes, Delete it!</button>
          </div>
        )
      }
    })  
  };

  handleChecked = (e, id)=>{
   console.log("e.target.checked", e.target.checked, "e.target.id", e.target.id);
   console.log("allIds", this.state.allIds);
   let patientArray = [];
   if(e.target.checked){
     patientArray.push(e.target.id);
   }else{
    for( var i = 0; i < patientArray.length; i++){ 
      if ( patientArray[i] == e.target.id) {
        patientArray.splice(i, 1); 
      }
   }
   }
   this.setState({allIds: patientArray});
   //e.target.checked = !e.target.checked;
  }

  handleAllChecked = (event) => {
  this.setState({checkall: !this.state.checkall}, 
    ()=>{
      let patientArray = [];
      if(this.state.checkall == true){
        this.props.patientList.docs.map(patient=>{
          patientArray.push(patient._id)
        });
        this.setState({allIds: patientArray})
      }else{
        this.setState({allIds: []})
      }
    })
  }
  handleSubmit = (e) =>{
    console.log("this.state.allIds", this.state.allIds);
    this.props.confirmMany(this.state.allIds);
    this.setState({allIds: [], checkall: false});
  }

  togglePrimary=() =>{
    this.setState({
      primary: !this.state.primary
    });
  }
  ResultModal = pid => {
    this.togglePrimary();
    this.props.getReportData(pid);
    this.setState({ pid: pid });
  };
  
  componentWillUnmount(){
    clearInterval(this.intervalID);
  }


  render() {
    const { patientList, reportData } = this.props;
    console.log("reportData", reportData);
    return (
    <div className="p-2 rounded" >
    <ToastContainer autoClose={2000} />
    <h4>Patient Master</h4>
    <div className="text-right mr-2">Total Results: {patientList.totalDocs? patientList.totalDocs: null}</div>

      <div className="bg-secondary d-flex justify-content-between">
      <FilterFieldsPatients handleFilterSelected = {this.handleSelected} />
        <div className="align-self-end mb-2 mr-2 justify-content-end">
          {this.state.checkall && 
          <Animated animationIn="fadeIn" animationOut="fadeOut" isVisible={true}>
          <Button className="bg-pink border border-warning text-white" onClick={this.handleSubmit}>Confirm selected</Button>
          </Animated>
          }
          </div>
        
      </div>
      <Table responsive  className="bg-white border" >
        <thead className="bg-primary">
          <tr>
            <th className="text-left">Patient Id</th>
            <th className="text-left">Name</th>
            <th className="text-center">DOB</th>
            <th className="text-center">Gender</th>
            <th className="text-center">Email</th>
            <th className="text-center">Mobile</th>
            <th className="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          { patientList.docs && !patientList.docs.length > 0
            ? <tr><td colSpan="9" className="text-center text-dark" style={{height: "100px", lineHeight:"100px"}}>Search parameters not matching any data. Please try again with different criteria</td></tr>
            : patientList.docs &&  patientList.docs.map((patient, idx) => {
                const myid = patient._id;
                // console.log("this.state.allIds[myid]", this.state.allIds[myid]);
                // let checkedMe = false;
                // if(this.state.allIds.includes(myid) ){
                //   checkedMe = true;
                // }
                return (
                  <tr key={idx} style={{whiteSpace: "nowrap"}}>
                    <td>
                    { this.state.centerCode+"-"+patient.patientId } 
                    </td>
                    <td>
                    { patient.name } 
                    { patient.fname+" "+patient.mname+" "+patient.lname } 
                    </td>
                    <td className="text-center">{ patient.dob? moment(patient.dob).format("DD-MM-YYYY") : ""}</td>
                    <td className="text-center">{ patient.gender }</td>
                    <td className="text-center">{ patient.email }</td>
                    <td className="text-center">{ patient.mobile }</td>
                    <td className="text-center">
                    <Button color="warning" size="sm" className="btn-block" onClick={()=>this.RegisterPatient(patient._id, patient.patientId)}>Register</Button>
                    </td>
                  </tr>
                );
              })}
        </tbody>
      </Table>
      <PaginationComponent
                  totalItems={patientList.totalDocs}
                  pageSize={Constants.PAGE_ITEM_SIZE}
                  onSelect={this.handleSelected}
                  maxPaginationNumbers={10}
                  activePage={patientList.page}
                />
      <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={"modal-primary " + this.props.className} size="lg">
            <ModalHeader toggle={this.togglePrimary}>Report Data: Imported from Spectrum</ModalHeader>
        <ModalBody>
          {reportData && reportData.map(report =>{
            var keyName = Object.keys(report.result);
            var valueName = Object.values(report.result);
            // console.log("Key Length",keyName.length);
            // console.log("Values Length",valueName.length);
            return (
              <div className="mb-4 bg-light p-2 shadow rounded">
              <div className="border-bottom d-flex justify-content-between align-items-center"> <h4 className="text-center text-primary "> Test : {report.test_name}</h4><small>Date: {moment(report.resultDate).format("DD-MM-YYYY")}</small></div>
              <div className="border-bottom d-flex justify-content-between align-items-center p-2"><strong>Parameter Name</strong><strong>Result Value</strong></div>
              {
                keyName.map((key,i)=>{
                  return ( <div className="d-flex justify-content-between align-items-center p-2 border-bottom" key={i}><div className="min-vw-50">{key.replace(/_/g, ' ')}:</div><div>{valueName[i]}</div></div>)
                })
              }
              </div>
            )
          })}
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.togglePrimary}>Close</Button>{' '}
        </ModalFooter>
      </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    patientList: state.patient.patientList,
    confirmed: state.patient.confirmed,
    reportData: state.patient.reportData,
    registrationSuccess: state.registration.registrationSuccess,
    patientId: state.registration.patientId,
    pname: state.registration.pname,
    regId: state.registration.regId,
  };
};

const mapDispachToProps = dispatch => {
  return {
    getPatientListRequest: (data) =>
      dispatch({ type: "GET_PATIENTLIST_REQUEST", data: data }),
    confirmMany: (data) =>dispatch({ type: "CONFIRM_MANY_REQUEST", data: data }),
    resetConfirmationFlag: (data) =>dispatch({ type: "RESET_CONFIRM_DATA_TRANSFER_SUCCESS_ASYNC" }),
    getReportData: (id) =>dispatch({ type: "GET_REPORT_DATA_REQUEST", data: id }),
    registerPatient: (data) =>dispatch({ type: "REGISTER_PATIENT_REQUEST", formdata: data }),
    resetRegistrationFlag: (data) =>dispatch({ type: "RESET_REGISTER_PATIENT_REQUEST" }),
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(PatientList);
