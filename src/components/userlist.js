import React, { Component } from "react";
import { Route } from "react-router-dom";

import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Table,
  Badge,
  Alert,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import Editable from "react-x-editable";
import AccessControl from "../utils/accessControl";
import EditableField from "../utils/editablefield";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

class Userlist extends Component {
  constructor(props) {
    super(props);
   
    this.state = {
      modal: false,
      danger: false,
      delid: ""
    };
  }
componentDidMount() {
  this.props.getUserListRequest();
}
  componentDidUpdate(prevProps, prevState) {
    
    if(this.props.factoryUpdateSucess){
        toast.success("Data updated successfuly");  
        this.props.resetUpdateRequest();      
      }
      if(this.props.factoryUpdateError){
        toast.warning("Something Wrong. Please try again.");  
        this.props.resetUpdateEmployeeRequest();
      }
  }
  handleChange = e => {
    const formdata = {
        efield: "factory",
        evalue: e.value,
        id: e.props.uniqid
    };
    //console.log("Formdata" + JSON.stringify(formdata));
    if (e.value == "") {
      alert("Please enter valid factory");
      return false;
    }
    this.props.updateUserRequest(formdata);
  };
  handleSubmit = (submitid, name, value) => {
     console.log("E Value",submitid)
    const formdata = {
      efield: name,
      evalue: value,
      id: submitid
    };
    //console.log("Formdata" + JSON.stringify(formdata));
    if (value == "") {
      alert("Please enter valid factory");
      return false;
    }
    this.props.updateUserRequest(formdata);
  };
  handleDelete = myid => {
    this.props.deleteUserRequest(myid);
    this.setState({
      danger: !this.state.danger
    });
  };

  gotoEdit =(id) => {
    this.props.history.push("/editUser?id=" + id);
    //this.Context.router.history.push("/editUser?id=" + id);
};

  render() {
    const { factoryList, locationList, userList } = this.props;
   
    return (
        <div className="animated fadeIn bg-white p-2 border rounded">
      <Table responsive striped>
          <ToastContainer autoClose={2000} />
        <thead>
          <tr>
            <th>Name</th>
            <th>Mobile</th>
            <th>Degree</th>
            <th>Designation</th>
            <th>Email</th>
            <th>Role</th>
            <th>Center</th>
            
            <th></th>
          </tr>
        </thead>
        <tbody>
          {!userList.length > 0
            ? <tr><td colSpan="2">No Data</td></tr>
            : userList.map((user, idx) => {
                const myid = user._id;
                let role = "";
                if(user.roles && user.roles == "frontdesk"){
                    role = "Front Desk"
                }
                if(user.roles && user.roles == "physician"){
                    role = "Physician"
                }
                if(user.roles && user.roles == "radiologist"){
                    role = "Radiologist"
                }
                if(user.roles && user.roles == "technician"){
                    role = "Technician"
                }
                if(user.roles && user.roles == "typist"){
                    role = "Typist"
                }
                if(user.roles && user.roles == "administrator"){
                    role = "Administrator"
                }
                return (
                  <tr key={idx}>
                    <td>
                      {user.fname} {user.mname} {user.lname}
                    </td>
                    <td>
                      {user.mobile ? user.mobile : ""}
                    </td>
                    <td>
                      {user.degree ? user.degree : ""}
                    </td>
                    <td>
                      {user.designation ? user.designation : ""}
                    </td>
                   
                    <td>
                    {user.email? user.email : ""}
                    </td>
                    <td>
                      {role}
                    </td>
                    <td>
                    {user.centers && user.centers.map(fc => {

                      return fc.id ? <p>{fc.id.center}</p> : ""
                    })}

                    </td>
                   
                    <td>       
                            <Button
                              color="primary"
                              onClick={() =>
                                this.gotoEdit(user._id)
                              }
                            >
                              Edit
                            </Button>
                    </td>
                  </tr>
                );
              })}
        </tbody>
      </Table>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    //locationList: state.location.locationList,
    centerList: state.center.centerList,
    userList: state.user.userList,
    userUpdateSucess: state.factory.userUpdateSucess,
    userUpdateError: state.factory.factoryUpdateError,
  };
};

const mapDispachToProps = dispatch => {
  return {
    getUserListRequest: () =>
      dispatch({ type: "GET_USER_LIST_REQUEST" }),
    getCenterListRequest: () =>
      dispatch({ type: "GET_CENTERLIST_REQUEST" }),
    updateUserRequest: data =>
      dispatch({ type: "UPDATE_USER_REQUEST", formdata: data }),
    deleteFactoryRequest: id =>
      dispatch({ type: "DELETE_USER_REQUEST", id: id }),
    resetUpdateRequest:()=>dispatch({ type: "RESET_USER_UPDATE_REQUEST" }),
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(Userlist);
