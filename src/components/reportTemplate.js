import React, { Component } from 'react';
import { Row, Col, Form, Input, Button, Table, Badge, Alert, Card, CardHeader, CardBody } from "reactstrap";
import { connect } from "react-redux";
import moment from "moment";
import Constants from "../config";
import PaginationComponent from "react-reactstrap-pagination";
import FilterFieldsPatients from "../utils/filterFieldsPatients";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Animated } from "react-animated-css";
import { convertFromRaw, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';


//const content = { "entityMap": {}, "blocks": [{ "key": "637gr", "text": "Initialized from content state.", "type": "unstyled", "depth": 0, "inlineStyleRanges": [], "entityRanges": [], "data": {} }] };
const content = { "entityMap": {}, "blocks": [{ "key": "637gr", "text": "Initialized from content state.", "type": "unstyled", "depth": 0, "inlineStyleRanges": [], "entityRanges": [], "data": {} }] };

class ReportTemplate extends Component {
    constructor(props) {
        super(props);
        const contentState = convertFromRaw(content);
        this.state = {
            contentState,
        }

    }
    onContentStateChange = (contentState) => {
       // const contentStateMe = contentState.getCurrentContent();
    //    console.log('content state', convertFromRaw(contentState));
    //    console.log('contentState', contentState);
        this.setState({
            contentState,
        });
    };
    loading = () => (
        <div className="animated fadeIn pt-1 text-center">Loading...</div>
    );

    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    saveReportTemplate = () => {
        // const blocks = this.state.editorState ? convertToRaw(this.state.editorState.getCurrentContent()).blocks : {};
        // const value = blocks.map(block => (!block.text.trim() && '\n') || block.text).join('\n');

        const formdata = {
            reportContent: JSON.stringify(this.state.contentState),
           // reportContent: value,
            title: this.state.title
        };
        // console.log("this.state.contentState",JSON.stringify(this.state.contentState), this.state.contentState._immutable);
        this.props.saveReportTemplate(formdata);
    };

    //   saveReport = () => {
    //          const formdata = {
    //          reportText: JSON.stringify(this.state.editorState),
    //          scheduleId: this.state.scheduleId 
    //     };
    //     console.log("formdata",formdata);
    //    this.props.saveReport(formdata);
    //   };

    componentDidMount() {
        // this.props.getCenterListRequest();
        // this.focusEditor();

    }
    componentDidUpdate(prevProps, prevState) {
        console.log("componentDidUpdate");
        if (this.props.saveReportTemplateSuccess == true) {
            toast.info("Template Saved Succesfuly");
            this.props.resetReportTemplateSuccessRequest();
        }

    }

    render() {
        // const { editorState, contentState} = this.state;
        return (
            <div className="animated fadeIn">
                <ToastContainer autoClose={2000} />
                <Card>
                    <CardHeader>Report Template</CardHeader>
                    <CardBody>
                        <div className="form-group row">
                            <label htmlFor="name" className="col-sm-3 col-form-label">Title:</label>
                            <div className="col-sm-9">
                                <Input type="text" className="form-control" name="title" placeholder="Title" onChange={this.handleChange} required="required" />
                            </div>
                        </div>
                        {/* <Editor
               // contentState={contentState}
                toolbarClassName="toolbarClassName"
                wrapperClassName="wrapperClassName"
                editorClassName="editorClassName"
                editorState={this.state.editorState}
                onEditorStateChange={this.onContentStateChange}
                />
             */}
                        <Editor
                            wrapperClassName="demo-wrapper"
                            editorClassName="demo-editor"
                            onContentStateChange={this.onContentStateChange}
                        />


                        <Button color="warning" size="sm" className="mb-2 mt-2" onClick={this.saveReportTemplate}>Save Report Template</Button>

                    </CardBody>
                </Card>

                {/* <Row>
        
              <Col md="12"> 
                <div className="card">
                  <div className="card-header">
                    <i className="icon-drop" />
                    User List
                  </div>
                  <div className="card-body">
                    <Suspense fallback={this.loading()}>
                      <Userlist {...this.props} />
                    </Suspense>
                  </div>
                </div>
              </Col>
            </Row> */}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        saveReportTemplateSuccess: state.report.saveReportTemplateSuccess,
    };
};

const mapDispachToProps = dispatch => {
    return {
        saveReportTemplate: data => dispatch({ type: "SAVE_REPORT_TEMPLATE_REQUEST", formdata: data }),
        resetReportTemplateSuccessRequest: () => dispatch({ type: "RESET_REPORT_TEMPLATE_REQUEST" }),
    };
};

export default connect(
    mapStateToProps,
    mapDispachToProps
)(ReportTemplate);