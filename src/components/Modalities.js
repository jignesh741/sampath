import React, { Component } from "react";
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Table,
  Badge,
  Alert,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import AccessControl from "../utils/accessControl";
import EditableField from "../utils/editablefield";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

class Modalities extends Component {
  constructor(props) {
    super(props);
   
    this.toggleDanger = this.toggleDanger.bind(this);
    this.state = {
      modal: false,
      danger: false,
      delid: ""
    };
  }
componentDidMount() {
  this.props.getModalityListRequest();
}
  componentDidUpdate(prevProps, prevState) {
    
    if(this.props.modalityUpdateSucess){
        toast.success("Data updated successfuly");  
        this.props.resetUpdateRequest();      
      }
      if(this.props.modalityUpdateError){
        toast.warning("Something Wrong. Please try again.");  
        this.props.resetUpdateModalityErrorRequest();
      }
     
  }
  
  handleSubmit = (submitid, name, value) => {
    //  console.log("E Value",submitid)
    const formdata = {
      efield: name,
      evalue: value,
      id: submitid
    };
    //console.log("Formdata" + JSON.stringify(formdata));
    if (value == "") {
      alert("Please enter valid text");
      return false;
    }
    this.props.updateModalityRequest(formdata);
  };
  handleDelete = myid => {
    this.props.deleteModalityRequest(myid);
    this.setState({
      danger: !this.state.danger
    });
  };
  handleCheboxChange = (modalityId, modality, e)=>{
    console.log(modalityId);
    console.log(e.target.name);
    const data = {
      modality: modalityId, 
      modality: modality, 
      status: e.target.checked,
    }
    this.props.updateModalityRequest(data);
    this.setState({
      [e.target.name]: e.target.checked
    });
  }
  toggleDanger = delid => {
    this.setState({
      danger: !this.state.danger,
      delid: delid
    });
  };

  render() {
    const { modalityList } = this.props;
   
    return (
      <div>
         <ToastContainer autoClose={2000} />
      
      <Table responsive striped>
         
        <thead>
          <tr>
            <th>Modality</th>         
            <th>Description</th>                 
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {!modalityList.length > 0
            ? <tr><td colSpan="3">No Data</td></tr>
            : modalityList.map((modality, idx) => {
                const myid = modality._id;
              
               
                return (
                  <tr key={idx}>
                    <td>
                     
                     <EditableField
                        type="text"
                        name="modality"
                        defaultValue={modality.modality}
                        handleSubmit={this.handleSubmit}
                        submitid={myid}
                    />
                    </td>
                    <td>
                     
                     <EditableField
                        type="text"
                        name="description"
                        defaultValue={modality.description}
                        handleSubmit={this.handleSubmit}
                        submitid={myid}
                    />
                    </td>
                   
                   
                   
                    
                    <td>
                      <div className="justify-content-right">
                        <a  onClick={() => this.toggleDanger(myid)}>
                          <i className="fa fa-trash fa-lg mt-1 text-danger" />
                        </a>

                        <Modal
                          isOpen={this.state.danger}
                          toggle={this.toggleDanger}
                          className={"modal-danger " + this.props.className}
                        >
                          <ModalHeader toggle={this.toggleDanger}>
                            Delete Item
                          </ModalHeader>
                          <ModalBody>
                            Do you really want to delete this item?
                          </ModalBody>
                          <ModalFooter>
                            <Button
                              color="danger"
                              onClick={() =>
                                this.handleDelete(this.state.delid)
                              }
                            >
                              Yes
                            </Button>{" "}
                            <Button
                              color="secondary"
                              onClick={() =>
                                this.toggleDanger(this.state.delid)
                              }
                            >
                              No
                            </Button>
                          </ModalFooter>
                        </Modal>
                      </div>
                    </td>
                  </tr>
                );
              })}
        </tbody>
      </Table>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    modalityList: state.modality.modalityList,
    modalityUpdateSucess: state.modality.modalityUpdateSucess,
    modalityUpdateError: state.modality.modalityUpdateError,
    modalityUpdateSucess: state.modality.modalityUpdateSucess,
  };
};

const mapDispachToProps = dispatch => {
  return {
    getModalityListRequest: () =>
      dispatch({ type: "GET_MODALITYLIST_REQUEST" }),
    updateModalityRequest: data =>
      dispatch({ type: "UPDATE_MODALITIES_MODALITY_REQUEST", formdata: data }),
    deleteModalityRequest: id =>
      dispatch({ type: "DELETE_MODALITY_REQUEST", id: id }),
      resetUpdateRequest:()=>dispatch({ type: "RESET_MODALITY_UPDATE_REQUEST" }),
            resetUpdateModalityErrorRequest: data =>
      dispatch({ type: "RESET_MODALITY_UPDATE_FAIL_REQUEST", formdata: data }),
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(Modalities);
