import React, { Component, Suspense } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import { Row, Col, Form, Input, Button, Table, Badge, Alert } from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const CenterList = React.lazy(() => import("./centerlist"));

class Center extends Component {
  constructor(props) {
    super(props);
    this.state = {location:null}
  }
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  
  handleSubmit = e => {
    e.preventDefault();
    const formdata = {
      center: this.state.center,
      code: this.state.code,
      address: this.state.address,
    };
    console.log("SubmitData=" + JSON.stringify(formdata));
    this.props.addCenterRequest(formdata);
  };
  componentDidMount() {
    this.props.getLocationListRequest();
  }
  componentDidUpdate(prevProps) {
    if(this.props.centerAddSucess){
      toast.info("Center Added Successfully");  
      this.props.resetCenterSuccessFlag();
    }
    if (this.props.centerList) {
      // this.props.history.push("/dashboard");
      console.log("Component did update");
    }
  }
  
  render() {
   
    return (
      <div className="animated fadeIn">
        <Row>
          <Col md="6">
            <div className="card">
              <div className="card-header">
                <i className="icon-drop" />
                Add Center
              </div>
              <div className="card-body">
               
                <Form onSubmit={this.handleSubmit}>
                  <Input
                    type="text"
                    placeholder="Enter Center"
                    autoComplete="Center"
                    name="center"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  <Input
                    type="text"
                    placeholder="Enter Center Code (Will be used as prefix with patient id)"
                    autoComplete="code"
                    name="code"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  <Input
                    type="textarea"
                    placeholder="Enter Center Address"
                    autoComplete="center-address"
                    name="address"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  
                  <Row>
                    <Col>
                      <Button color="primary" className="px-4 mt-2">
                        Add Center
                      </Button>
                    </Col>
                  </Row>
                </Form>
              </div>
            </div>
          </Col>
          </Row>
          <Row>
          <Col md="12">
            <div className="card">
              <div className="card-header">
                <i className="icon-drop" />
                Center List
              </div>
              <div className="card-body">
                <Suspense fallback={this.loading()}>
                  <CenterList {...this.props}/>
                </Suspense>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
     centerAddSucess: state.center.centerAddSucess,
    centerAddError: state.center.centerAddError
  };
};

const mapDispachToProps = dispatch => {
  return {
    getLocationListRequest: (data) =>
      dispatch({ type: "GET_LOCATIONLIST_REQUEST", locationIn: data }),
    addCenterRequest: data =>
      dispatch({ type: "ADD_CENTER_REQUEST", formdata: data }),
      resetCenterSuccessFlag: data =>
      dispatch({ type: "RESET_CENTER_SUCCESS_FLAG_REQUEST" })
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(Center);
