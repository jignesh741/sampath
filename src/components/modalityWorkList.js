import React, { Component } from 'react';
import { Row, Col, Form, Input, Button, Table, Badge, Alert, Card, CardHeader,CardBody } from "reactstrap";
import { connect } from "react-redux";
import { Redirect, Link } from "react-router-dom";
import moment from "moment";
import Constants from "../config";
import PaginationComponent from "react-reactstrap-pagination";
import FilterFieldsPatients from "../utils/filterFieldsPatients";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {Animated} from "react-animated-css";


class ModalityWorkList extends Component {
    constructor(props) {
        super(props);
        const centerCode = localStorage.getItem("centerCode");
        this.state={ selectedPage: 1,  
          centerCode: centerCode,
          filterStr: {},filterStr: {},
        }
    }
    componentDidMount() {
       const formdata = {
        centerId: localStorage.getItem("center"),
        filterStr: {}
        }
        this.props.scheduleListRequest(formdata);
    }
    schedulePatient = (patId,regId,orderId, scan)=>{
      const formdata = {
        patId: patId,
        regId: regId,
        orderId:orderId,
        scan:scan,
        centerId: localStorage.getItem("center"),
        enteredBy: localStorage.getItem("userid"),        
      }
      this.props.sendPatientSchedule(formdata);
    }
    componentDidUpdate(prevProps, prevState) {
      if(this.props.scheduleSuccess === true){
        toast.success("Patient Schedule Successfuly");  
        this.props.resetPatientSchedule();
        const formdata = {
          centerId: localStorage.getItem("center"),
          filterStr: {}
          }
          this.props.scheduleListRequest(formdata);
      }
      if(prevProps.scanfinisedSuccess !== this.props.scanfinisedSuccess){
        toast.success("Scan finished status saved.");  
        const formdata = {
          centerId: localStorage.getItem("center"),
          filterStr: {}
          }
          this.props.scheduleListRequest(formdata);
      }
      if(prevProps.imagesPushedSuccess !== this.props.imagesPushedSuccess){
        toast.success("Images pushed status saved.");  
        const formdata = {
          centerId: localStorage.getItem("center"),
          filterStr: {}
          }
          this.props.scheduleListRequest(formdata);
      }
    }
    handleSelected=(selectedPage)=>{   
        this.setState({ selectedPage: selectedPage },()=>{
        const data = {
            centerId: localStorage.getItem("center"),
            filterStr: {},
            page: this.state.selectedPage,
            limit:Constants.PAGE_ITEM_SIZE}
             this.props.scheduleListRequest(data);
        });   
      }
  scanFinished=(scheduleId)=>{
    this.props.scanFinished(scheduleId);
  }

  saveImagesPushed=(scheduleId)=>{
    this.props.imagesPushed(scheduleId);
  }
render() {
    const {scheduleList} = this.props;
    return (
        <div className="animated fadeIn">
        <ToastContainer autoClose={2000} />
        <h4>Modality Work List</h4>
        <Table responsive striped  className="bg-white border" >
        <thead className="bg-primary">
          <tr>
            <th className="text-center">Date</th>
            <th className="text-center">Time</th>
            <th className="text-center">Order ID</th>
            <th className="text-center">Reg.Id</th>
            <th className="text-center">Patient Id</th>
            <th className="text-center">Patient</th>
            <th className="text-center">Modalities</th>
            <th className="text-center">Scan</th>
            <th className="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
        { scheduleList.docs && !scheduleList.docs.length > 0
            ? <tr><td colSpan="9" className="text-center text-dark" style={{height: "100px", lineHeight:"100px"}}>Search parameters not matching any data. Please try again with different criteria</td></tr>
            : scheduleList.docs &&  scheduleList.docs.map((schedule, idx) => {
                const myid = schedule._id;
                console.log("order.scheduleStatus", schedule.scheduleStatus)
                return (
                    <tr key={idx} style={{whiteSpace: "nowrap"}}>
                      <td style={{width:"100px"}}>
                      { moment(schedule.date).format("DD-MM-YYYY")}
                      </td>
                      <td style={{width:"100px"}}>
                      { moment({ hour:schedule.time, minute:0 }).format("h:mm A")}
                      </td>
                      <td>
                      { this.state.centerCode+"-"+schedule.dispOrderId } 
                      </td>
                      <td>
                      { this.state.centerCode+"-"+schedule.regDispId } 
                      </td>
                      <td>
                      { this.state.centerCode+"-"+schedule.dispPatId } 
                      </td>
                      <td>
                      { schedule.patientId.fname+" "+schedule.patientId.mname+" "+schedule.patientId.lname } 
                      </td>
                      <td>
                      { schedule.modality } 
                      </td>
                      <td>
                      { schedule.scan } 
                      </td>
                      <td>
                    
                    { !schedule.scanFinished &&
                    <Button color="warning" size="sm" className="mb-4 btn-block" onClick={()=>this.scanFinished(schedule._id)}>Scan Finished</Button>
                   }
                   { schedule.scanFinished &&
                    <div className=""><i className="fa fa-check fa-lg mb-2"></i> Scan Finised</div>
                   }
                    { !schedule.imagesPushed &&
                    <Button color="warning" size="sm" className="mb-4 btn-block" onClick={()=>this.saveImagesPushed(schedule._id)}>Images Pushed</Button>
                   }
                   { schedule.imagesPushed &&
                     <div className=""><i className="fa fa-check fa-lg mb-2"></i> Images Pushed</div>
                   }
                  
                    
                      </td>

                      </tr>
                      )
            
            })}
        </tbody>
        </Table>
        <PaginationComponent
        totalItems={scheduleList.totalDocs}
        pageSize={Constants.PAGE_ITEM_SIZE}
        onSelect={this.handleSelected}
        maxPaginationNumbers={10}
        activePage={scheduleList.page}
        />
       
        </div>
    );
    }
}

const mapStateToProps = state => {
    return {
     scheduleList: state.schedule.scheduleList,
     scanfinisedSuccess: state.schedule.scanfinisedSuccess,
     imagesPushedSuccess: state.schedule.imagesPushedSuccess,
     };
  };
  
  const mapDispachToProps = dispatch => {
    return {
        scheduleListRequest: data =>dispatch({ type: "SCHEDULE_LIST_REQUEST", formdata: data }),
        scanFinished: data =>dispatch({ type: "SCAN_FINISHED_REQUEST", scheduleId: data }),
        imagesPushed: data =>dispatch({ type: "IMAGES_PUSHED_REQUEST", scheduleId: data }),
        // resetPatientSchedule: () =>dispatch({ type: "RESET_SCHEDULE_SUCCESS_ASYNC"}),
    };
  };
  
  export default connect(
    mapStateToProps,
    mapDispachToProps
  )(ModalityWorkList);