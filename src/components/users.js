import React, { Component, Suspense } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import { Row, Col, Form, FormGroup,Input, Button,Label, Table, Badge, Alert, Card, CardHeader,CardBody } from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


const CenterList = React.lazy(() => import("./centerlist"));
//const CenterManagerlist = React.lazy(() => import("./centerManagerlist"));
const Userlist = React.lazy(() => import("./userlist"));

class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {center:[]};
  }
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );
  handleChange = e => {
    var targetName = e.target.id;  

    if( e.target.checked === true){           
      if(this.state.center.length > 0){
         // var joined = this.state.center.push({id: e.target.id});
          this.setState({ center: [ ...this.state.center,  {id: e.target.id}] , [e.target.id]:e.target.checked,
          });
      }else{          
          this.setState({
          center: [ ...this.state.center,  {id: e.target.id}],
              [e.target.id]:e.target.checked,
          })
      }       
  }
  if( e.target.checked === false){   
      this.setState({      
          [e.target.id]: e.target.checked,
          center: this.state.center.filter(function(fact) { 
            console.log("Matched",e.target.id);
          return fact.id !== e.target.id
      })});
  } 
  if(e.target.name){
    this.setState({
      [e.target.name]:e.target.value,
  })
  }
  
  };
//   handleChangeState = e => {
//     this.setState({
//       [e.target.name]: e.target.value
//     },()=> this.props.getLocationListRequest(this.state.locationIn));
//   };
//   handleChangeLocation = e => {
//     this.setState({
//       [e.target.name]: e.target.value
//     },()=> this.props.getCenterListRequest(this.state.location));
//   };
  handleSubmit = e => {
    e.preventDefault();
    if(this.state.center.length == 0){
      alert("Please select at least one center.");
      return false;
    }
    var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
   var testPassword = strongRegex.test(this.state.password);
    if(!testPassword){
        toast.warning("Password should contain at least:- One uppercase character, One lowercase character, One number and special character like #,@,& etc and password should be minimum 8 characters long"); 
        return false;
       }
    const formdata = {
      
        fname: this.state.fname,
        mname: this.state.mname,
        lname: this.state.lname,
        degree: this.state.degree, 
        designation: this.state.designation,  
        mobile: this.state.mobile, 
        email: this.state.email,
        password: this.state.password, 
        signature: this.state.signature,
        roles: this.state.roles,
        centers: this.state.center
      
    };
    console.log("formdata",formdata);
   this.props.addUserRequest(formdata);
  };
  componentDidMount() {
    this.props.getCenterListRequest();
  }
  componentDidUpdate(prevProps, prevState) {
    console.log("componentDidUpdate");
    if (this.props.userAddSucess == true) {
        toast.info("User Added Succesfuly");  
        this.props.resetAddUserSuccessRequest();
    }
    if (this.props.userAddError ) {
        toast.warning("Something Wrong. Please try again.");  
        this.props.resetAddUserFailRequest();
    }
  }
 
  render() {
    const { locationList, centerList } = this.props;
    return (
      <div className="animated fadeIn">
          <ToastContainer autoClose={2000} />
          <Card>
          <CardHeader>Add User</CardHeader>
          <CardBody>
          <Form onSubmit={this.handleSubmit}>
          <Row>
            <Col md="6">
              <Row>
                  <Col md="12" >
                    <Input
                    type="text"
                    placeholder="Enter First Name"
                    autoComplete="new-fname"
                    name="fname"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
                  <Col md="12" >
                    <Input
                    type="text"
                    placeholder="Enter Middle Name"
                    autoComplete="new-mname"
                    name="mname"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                </Col>

                  <Col md="12" >
                    <Input
                    type="text"
                    placeholder="Enter Last Name"
                    autoComplete="new-lname"
                    name="lname"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
                  <Col md="12" >
                    <Input
                    type="text"
                    placeholder="Enter Degree"
                    autoComplete="new-degree"
                    name="degree"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>    
                  <Col md="12" >
                  <Input
                    type="text"
                    placeholder="Enter Designation"
                    autoComplete="designation"
                    name="designation"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
                  
                  <Col md="12" >
                  <Input
                    type="text"
                    placeholder="Enter Mobile Number"
                    autoComplete="mobile"
                    name="mobile"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
                  <Col md="12" >
                  <Input
                    type="select"
                    autoComplete="new-roles"
                    name="roles"
                    value={this.state.roles ? this.state.roles : ""}
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  >
                    <option value="">Select Role</option>
                    <option value="administrator">Administrator</option>
                    <option value="frontdesk">Front Desk</option>
                    <option value="physician">Physician</option>
                    <option value="radiologist">Radiologist</option>
                    <option value="technician">Technician</option>
                    <option value="typist">Typist</option>
                  </Input>
                  </Col>
                  <Col md="12" >
                  <Input
                    type="email"
                    placeholder="Enter Email (Username)"
                    autoComplete="email"
                    name="email"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
                  <Col md="12" >
                  <Input
                    type="password"
                    placeholder="Enter Password "
                    autoComplete="new-password"
                    name="password"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                   // pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})"
                  />
                  </Col>
              
                  
              </Row>
              </Col>
              <Col md="6" className="pl-4">
                <h5> Assign Center</h5>
              {!centerList.length > 0
                  ? ""
                  : centerList.map((center, idx) => {
                      return (
                      <React.Fragment key={idx}>
                        {/* <FormGroup> */}
                         <span className="p-4"> <Input type="checkbox" id={center._id} onChange={this.handleChange} checked={ this.state[center._id] ? this.state[center._id] : false}  />{center.center}</span>
                        {/* </FormGroup> */}
                      </React.Fragment>
                      );
                  })}
               
              </Col>
            </Row>
            <Row>
            <Col md="12">
                  <Button color="primary" className="px-4 mt-2">
                        Add User
                      </Button>
                  </Col>
            </Row>
           </Form>   
          </CardBody>
          </Card>

        {/* <Row>
    
          <Col md="12">
            <div className="card">
              <div className="card-header">
                <i className="icon-drop" />
                User List
              </div>
              <div className="card-body">
                <Suspense fallback={this.loading()}>
                  <Userlist {...this.props} />
                </Suspense>
              </div>
            </div>
          </Col>
        </Row> */}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    locationList: state.location.locationList,
    centerList: state.center.centerList,
    userAddSucess: state.user.userAddSucess,
    userAddError: state.user.userAddError
  };
};

const mapDispachToProps = dispatch => {
  return {
    getLocationListRequest: (locationIn) =>dispatch({ type: "GET_LOCATIONLIST_REQUEST", locationIn: locationIn }),
    getCenterListRequest: (locationId) =>dispatch({ type: "GET_CENTERLIST_REQUEST", data: locationId }),
    addUserRequest: data =>dispatch({ type: "ADD_USER_REQUEST", formdata: data }),
    resetAddUserSuccessRequest: ()=>dispatch({ type: "RESET_USER_SUCESS_REQUEST"}),
    resetAddUserFailRequest: ()=>dispatch({ type: "RESET_USER_FAIL_REQUEST"}),
      
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(Users);
