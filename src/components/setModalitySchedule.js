import React, { Component } from 'react';
import { connect } from "react-redux";
import { Row, Col, Form, Input, Button, Table, Badge, Alert, Card, CardHeader,CardBody } from "reactstrap";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


class SetModalitySchedule extends Component {
    constructor(props) {
        super(props);
       
        const query = new URLSearchParams(this.props.location.query);
        const centerInfo = JSON.parse(query.get("center"));
        const centerId = centerInfo? centerInfo._id : "";
        const modalities= centerInfo? centerInfo.modalities : ""
        console.log("modalities",modalities)
        this.state= {
            center: centerInfo,
            centerId: centerId,
            modalities: modalities,
        }
            // this.setState({
            //     center: centerInfo,
            //     centerId: centerId,
            //     modalities: modalities,
            // })
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.modalitySettingsSucess){
            toast.info("Modality Settings Save Successfuly");
        }
    }

    setData = (e, index)=>{
        let modalities = Object.assign([],this.state.modalities);
        console.log("name", e.target.name)
        modalities[index][e.target.name] = e.target.value;
        this.setState({
            modalities: modalities
        })
        console.log("modalities", modalities)
    }
    handleSubmit =e=>{
        e.preventDefault();
        const formdata = {
            centerId: this.state.centerId,
            modalities: this.state.modalities
        }
        console.log("Formdata:", formdata);
        this.props.setModalitySettings(formdata);
    }
    render() {
        const { modalities } = this.state;
        return (
            <div className="animated fadeIn">
                <h3>Modality Schedule</h3>
                <small>NOTE: Schedule Interval in Minutes; Use 24 hour clock to set Start and End Time</small>
                <ToastContainer autoClose={2000} />
                <Form onSubmit={this.handleSubmit} autoComplete="off">
                { modalities.length > 0 && modalities.map((modality, index)=>{
                    if(modality.status){
                        return (

                            <div className="w-100">
                             <div className="bg-primary p-1" ><h5 style={{verticalAlign:"middle"}}>{modality.modality}</h5></div> 
                             <div class="d-inline-flex bg-secondary justify-content-between align-items-center w-100">
                                <div class="p-2 ">Schedule Interval:</div>
                                
                                <div class="p-2 ">
                                     <Input 
                                type="text" 
                                className="form-control" 
                                name="scheduleInterval" 
                                value={modality.scheduleInterval} 
                                required="required" 
                                autoComplete="new-scheduleInterval" 
                                onBlur={(e)=>this.setData(e,index)}
                                 /></div>

                                <div class="p-2 ">Start Time:</div>
                                <div class="p-2">
                                    <Input 
                                type="text" 
                                className="form-control" 
                                name="startTime" 
                                value={modality.startTime} 
                                required="required" 
                                autoComplete="new-startTime" 
                                onBlur={(e)=>this.setData(e,index)}
                                 /></div>
                                <div class="p-2 ">End Time:</div>
                                <div class="p-2">
                                    <Input 
                                type="text" 
                                className="form-control" 
                                name="endTime" 
                                value={modality.endTime} 
                                required="required" 
                                autoComplete="new-endTime" 
                                onBlur={(e)=>this.setData(e,index)}
                                 /></div>
                                <div class="p-2 ">Weekly Holiday:</div>
                                <div class="p-2">
                                    <Input 
                                type="select" 
                                className="form-control" 
                                name="weeklyHoliday" 
                                value={modality.weeklyHoliday} 
                                autoComplete="new-weeklyHoliday" 
                                onBlur={(e)=>this.setData(e,index)}
                                 >
                                    <option value="">Please Select</option>
                                    <option value="Sunday">Sunday</option>
                                    <option value="Monday">Monday</option>
                                    <option value="Tuesday">Tuesday</option>
                                    <option value="Wednesday">Wednesday</option>
                                    <option value="Thursday">Thursday</option>
                                    <option value="Friday">Friday</option>
                                    <option value="Saturday">Saturday</option>
                                </Input>     
                                 </div>
                            </div>
                            </div>
                        )
                    }
                })}
                 <div class="pt-2 mb-2">
                <Button color="warning" size="sx" className="col-sm-4" >Save Modality Settings</Button>
                </div>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        centerList: state.center.centerList,
        modalitySettingsSucess: state.modality.modalitySettingsSucess,
        modalitySettingsError: state.modality.modalitySettingsError,
    };
  };
  
  const mapDispachToProps = dispatch => {
    return {
        setModalitySettings: data =>dispatch({ type: "SEND_MODALITY_SETTINGS_REQUEST", formdata: data }),
    };
  };
  
  export default connect(
    mapStateToProps,
    mapDispachToProps
  )(SetModalitySchedule);