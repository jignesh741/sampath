import React, { Component } from "react";
import { Route } from "react-router-dom";

import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Table,
  Badge,
  Alert,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import Editable from "react-x-editable";
import AccessControl from "../utils/accessControl";
import EditableField from "../utils/editablefield";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

class ExecutiveList extends Component {
  constructor(props) {
    super(props);
   
    this.state = {
      modal: false,
      danger: false,
      delid: ""
    };
  }
componentDidMount() {
  this.props.getExecutiveListRequest();
  // this.props.getLocationListRequest();
  // this.props.getFactoryListRequest();
}
  componentDidUpdate(prevProps, prevState) {
    
    if(this.props.factoryUpdateSucess){
        toast.success("Data updated successfuly");  
        this.props.resetUpdateRequest();      
      }
      if(this.props.factoryUpdateError){
        toast.warning("Something Wrong. Please try again.");  
        this.props.resetUpdateEmployeeRequest();
      }
  }
  handleChange = e => {
    const formdata = {
        efield: "factory",
        evalue: e.value,
        id: e.props.uniqid
    };
    //console.log("Formdata" + JSON.stringify(formdata));
    if (e.value == "") {
      alert("Please enter valid factory");
      return false;
    }
    this.props.updateExecutiveRequest(formdata);
  };
  handleSubmit = (submitid, name, value) => {
     console.log("E Value",submitid)
    const formdata = {
      efield: name,
      evalue: value,
      id: submitid
    };
    //console.log("Formdata" + JSON.stringify(formdata));
    if (value == "") {
      alert("Please enter valid factory");
      return false;
    }
    this.props.updateExecutiveRequest(formdata);
  };
  handleDelete = myid => {
    this.props.deleteExecutiveRequest(myid);
    this.setState({
      danger: !this.state.danger
    });
  };

  gotoEdit =(id) => {
    this.props.history.push("/editExecutive?id=" + id);
    //this.Context.router.history.push("/editExecutive?id=" + id);
};

  render() {
    const { factoryList, locationList, executiveList } = this.props;
   
    return (
    
      <Table responsive striped>
          <ToastContainer autoClose={2000} />
        <thead>
          <tr>
            <th>Name</th>
            <th>Designation</th>
            <th>Mobile</th>
            <th>Email</th>
            <th>Center</th>
            
            <th></th>
          </tr>
        </thead>
        <tbody>
          {!executiveList.length > 0
            ? <tr><td colSpan="2">No Data</td></tr>
            : executiveList.map((executive, idx) => {
                const myid = executive._id;
                return (
                  <tr key={idx}>
                    <td>
                      {executive.name}
                    </td>
                    <td>
                      {executive.designation ? executive.designation : ""}
                    
                    </td>
                    <td>
                      {executive.mobile? executive.mobile : ""}
                   
                    </td>
                    <td>
                    {executive.email? executive.email : ""}
                    </td>
                    
                    <td>
                    {executive.center && executive.center.map(fc => {

                      return fc.id ? <p>{fc.id.center}</p> : ""
                    })}

                    </td>
                   
                    <td>       
                            <Button
                              color="primary"
                              onClick={() =>
                                this.gotoEdit(executive._id)
                              }
                            >
                              Edit
                            </Button>
                    </td>
                  </tr>
                );
              })}
        </tbody>
      </Table>
    );
  }
}

const mapStateToProps = state => {
  return {
    //locationList: state.location.locationList,
    centerList: state.center.centerList,
    executiveList: state.executive.executiveList,
    executiveUpdateSucess: state.factory.executiveUpdateSucess,
    executiveUpdateError: state.factory.factoryUpdateError,
  };
};

const mapDispachToProps = dispatch => {
  return {
    getExecutiveListRequest: () =>
      dispatch({ type: "GET_EXECUTIVE_LIST_REQUEST" }),
    getCenterListRequest: () =>
      dispatch({ type: "GET_CENTERLIST_REQUEST" }),
    getFactoryListRequest: () =>
      dispatch({ type: "GET_FACTORYLIST_REQUEST" }),
    updateExecutiveRequest: data =>
      dispatch({ type: "UPDATE_EXECUTIVE_REQUEST", formdata: data }),
    deleteFactoryRequest: id =>
      dispatch({ type: "DELETE_EXECUTIVE_REQUEST", id: id }),
    resetUpdateRequest:()=>dispatch({ type: "RESET_EXECUTIVE_UPDATE_REQUEST" }),
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(ExecutiveList);
