import React, { Component } from 'react';
import { Button, Card, CardHeader, CardBody } from "reactstrap";
import "react-toastify/dist/ReactToastify.css";

import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
export const LOCAL_STORAGE_KEY = 'reportEditor';

class ReportEditor extends Component {
    state = {
        editorState: EditorState.createEmpty()
    };

    constructor(props) {
        super(props);
        const htmlContent = localStorage.getItem(LOCAL_STORAGE_KEY);
        const contentBlock = htmlToDraft(htmlContent || '');

        if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            const editorState = EditorState.createWithContent(contentState);
            this.state = {
                editorState,
            };
        }
    }

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
        });
    };

    saveReport = () => {
        const htmlContent = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()));
        localStorage.setItem(LOCAL_STORAGE_KEY, htmlContent);
    };

    render() {
        const { editorState } = this.state;
        return (
            <div className="animated fadeIn">
                <Card>
                    <CardHeader>Report Editor</CardHeader>
                    <CardBody>
                        <Editor
                            editorState={editorState}
                            onEditorStateChange={this.onEditorStateChange}
                        />
                        <Button color="warning" size="sm" className="mb-2 mt-2"
                            onClick={this.saveReport}>Update</Button>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default ReportEditor;