import React, { Component, Suspense } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import { Row, Col, Form, Input, Button, Table, Badge, Alert, Card, CardHeader,CardBody } from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


const CenterList = React.lazy(() => import("./centerlist"));
//const CenterManagerlist = React.lazy(() => import("./centerManagerlist"));
const ExecutiveList = React.lazy(() => import("./executiveList"));

class Executives extends Component {
  constructor(props) {
    super(props);
    this.state = {location:null}
  }
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  handleChangeState = e => {
    this.setState({
      [e.target.name]: e.target.value
    },()=> this.props.getLocationListRequest(this.state.locationIn));
  };
  handleChangeLocation = e => {
    this.setState({
      [e.target.name]: e.target.value
    },()=> this.props.getCenterListRequest(this.state.location));
  };
  handleSubmit = e => {
    e.preventDefault();
    const formdata = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      designation: this.state.designation,
      mobile: this.state.mobile,
      center: this.state.center,
      location: this.state.location,
      locationIn: this.state.locationIn,
    };
    this.props.addExecutiveRequest(formdata);
  };
  componentDidMount() {
    this.props.getLocationListRequest();
  }
  componentDidUpdate(prevProps, prevState) {
    console.log("componentDidUpdate");
    if (this.props.executiveAddSucess == true) {
        toast.info("Center Manager Added Succesfuly");  
        this.props.resetAddExecutiveSuccessRequest();
    }
    if (this.props.executiveAddError ) {
        toast.warning("Something Wrong. Please try again.");  
        this.props.resetAddExecutiveFailRequest();
    }
  }
 
  render() {
    const { locationList, centerList } = this.props;
    return (
      <div className="animated fadeIn">
          <ToastContainer autoClose={2000} />
          <Card>
          <CardHeader>Add Executive</CardHeader>
          <CardBody>
          <Form onSubmit={this.handleSubmit}>
              <Row>
                  <Col md="6" >
                    <Input
                    type="text"
                    placeholder="Enter Name"
                    autoComplete="Name"
                    name="name"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
                  <Col md="6" >
                  <Input
                    type="text"
                    placeholder="Enter Designation"
                    autoComplete="designation"
                    name="designation"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  {/* <Input
                    type="select"
                    autoComplete="mobile"
                    name="designation"
                    value={this.state.designation ? this.state.designation : ""}
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  >
                    <option value="">Select Designation</option>
                    <option value="Doer">Doer</option>
                    <option value="Center Manager">Center Manager</option>
                    <option value="General Manager">General Manager</option>
                  </Input> */}
                  </Col>
              </Row>
              <Row>
                  <Col md="6" >
                  <Input
                        type="select"
                        name="locationIn"
                        id="locationIn"
                        value={this.state.locationIn ? this.state.locationIn : ""}
                        onChange={this.handleChangeState}
                        required="required"
                        >
                        <option value="">Select State</option>
                        <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                        <option value="Andhra Pradesh">Andhra Pradesh</option>
                        <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                        <option value="Assam">Assam</option>
                        <option value="Bihar">Bihar</option>
                        <option value="Chandigarh">Chandigarh</option>
                        <option value="Chhattisgarh">Chhattisgarh</option>
                        <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                        <option value="Daman and Diu">Daman and Diu</option>
                        <option value="Delhi">Delhi</option>
                        <option value="Goa">Goa</option>
                        <option value="Gujarat">Gujarat</option>
                        <option value="Haryana">Haryana</option>
                        <option value="Himachal Pradesh">Himachal Pradesh</option>
                        <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                        <option value="Jharkhand">Jharkhand</option>
                        <option value="Karnataka">Karnataka</option>
                        <option value="Kerala">Kerala</option>
                        <option value="Lakshadweep">Lakshadweep</option>
                        <option value="Madhya Pradesh">Madhya Pradesh</option>
                        <option value="Maharashtra">Maharashtra</option>
                        <option value="Manipur">Manipur</option>
                        <option value="Meghalaya">Meghalaya</option>
                        <option value="Mizoram">Mizoram</option>
                        <option value="Nagaland">Nagaland</option>
                        <option value="Orissa">Orissa</option>
                        <option value="Pondicherry">Pondicherry</option>
                        <option value="Punjab">Punjab</option>
                        <option value="Rajasthan">Rajasthan</option>
                        <option value="Sikkim">Sikkim</option>
                        <option value="Tamil Nadu">Tamil Nadu</option>
                        <option value="Tripura">Tripura</option>
                        <option value="Uttaranchal">Uttaranchal</option>
                        <option value="Uttar Pradesh">Uttar Pradesh</option>
                        <option value="West Bengal">West Bengal</option>
                        
                        </Input>
                  </Col>
                  <Col md="6" >
                  <Input
                    type="text"
                    placeholder="Enter Mobile Number"
                    autoComplete="mobile"
                    name="mobile"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
              </Row>
              <Row>
                  <Col md="6" >
                  <Input
                        type="select"
                        name="location"
                        id="location"
                        value={this.state.location ? this.state.location : ""}
                        onChange={this.handleChangeLocation}
                        required="required"
                        >
                        <option value="">Select Location</option>

                        {!locationList.length > 0
                            ? ""
                            : locationList.map((location, idx) => {
                                return (
                                <React.Fragment key={idx}>
                                    <option value={location._id}>
                                    {location.location}
                                    </option>
                                </React.Fragment>
                                );
                            })}
                        </Input>
                  </Col>
                  <Col md="6" >
                  <Input
                    type="text"
                    placeholder="Enter Email "
                    autoComplete="email"
                    name="email"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
              </Row>
              <Row>
                  <Col md="6" >
                  <Input
                        type="select"
                        name="center"
                        id="center"
                        value={this.state.center ? this.state.center : ""}
                        onChange={this.handleChange}
                        required="required"
                        >
                        <option value="">Select Center/Unit</option>

                        {!centerList.length > 0
                            ? ""
                            : centerList.map((center, idx) => {
                                return (
                                <React.Fragment key={idx}>
                                    <option value={center._id}>
                                    {center.center}
                                    </option>
                                </React.Fragment>
                                );
                            })}
                        </Input>
                  </Col>
                  <Col md="6" >
                  <Input
                    type="password"
                    placeholder="Enter Password "
                    autoComplete="new-password"
                    name="password"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
              </Row>
              <Row>
                  <Col md="12">
                  <Button color="primary" className="px-4 mt-2">
                        Add Executive
                      </Button>
                  </Col>
              </Row>
           </Form>   
          </CardBody>
          </Card>

        <Row>
    
          <Col md="12">
            <div className="card">
              <div className="card-header">
                <i className="icon-drop" />
                Executive List
              </div>
              <div className="card-body">
                <Suspense fallback={this.loading()}>
                  <ExecutiveList />
                </Suspense>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    locationList: state.location.locationList,
    centerList: state.center.centerList,
    executiveAddSucess: state.executive.executiveAddSucess,
    executiveAddError: state.executive.executiveAddError
  };
};

const mapDispachToProps = dispatch => {
  return {
    getLocationListRequest: (locationIn) =>dispatch({ type: "GET_LOCATIONLIST_REQUEST", locationIn: locationIn }),
    getCenterListRequest: (locationId) =>dispatch({ type: "GET_FACTORYLIST_REQUEST", data: locationId }),
    addExecutiveRequest: data =>dispatch({ type: "ADD_EXECUTIVE_REQUEST", formdata: data }),
    resetAddExecutiveSuccessRequest: ()=>dispatch({ type: "RESET_EXECUTIVE_SUCESS_REQUEST"}),
    resetAddExecutiveFailRequest: ()=>dispatch({ type: "RESET_EXECUTIVE_FAIL_REQUEST"}),
      
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(Executives);
