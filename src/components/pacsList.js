import React, { Component } from 'react';
import { Row, Col, Form, Input, Button, Table, Badge, Alert, Card, CardHeader,CardBody } from "reactstrap";
import { connect } from "react-redux";
import { Redirect, Link } from "react-router-dom";
import moment from "moment";
import Constants from "../config";
import PaginationComponent from "react-reactstrap-pagination";
import FilterFieldsPatients from "../utils/filterFieldsPatients";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {Animated} from "react-animated-css";


class PacsList extends Component {
    constructor(props) {
        super(props);
        const centerCode = localStorage.getItem("centerCode");
        this.state={ selectedPage: 1,  
          centerCode: centerCode,
          filterStr: {},filterStr: {},
        }
    }
    componentDidMount() {
       const formdata = {
        centerId: localStorage.getItem("center"),
        filterStr: {}
        }
        this.props.pacsListRequest(formdata);
    }
    
    handleSelected = (selectedPage, filterStr = this.state.filterStr)=>{
        this.setState({ selectedPage: selectedPage, filterStr: filterStr }, () => {
          const data = {
           // executive: userdata._id,
           centerId: this.state.centerId,
            page: this.state.selectedPage ? this.state.selectedPage: 1,
            limit: Constants.PAGE_ITEM_SIZE,
            filterStr: this.state.filterStr? this.state.filterStr : {},
          };
          this.props.pacsListRequest(data);
        });
      }
  
render() {
    const {pacsList} = this.props;
    return (
        <div className="animated fadeIn">
        <ToastContainer autoClose={2000} />
        <h4>PACS List</h4>
        <div className="text-right mr-2">Total Results: {pacsList.totalDocs? pacsList.totalDocs: null}</div>
        <div className="bg-secondary d-flex justify-content-between">
        <FilterFieldsPatients handleFilterSelected={this.handleSelected} />
        </div>
        <Table responsive striped  className="bg-white border" >
        <thead className="bg-primary">
          <tr>
            <th className="text-center">Patient Name</th>
            <th className="text-center">Patient Id</th>
            <th className="text-center">Age</th>
            <th className="text-center">Sex</th>
            <th className="text-center">Bodypart</th>
            <th className="text-center">Modalities</th>
            <th className="text-center">Accession Number</th>
            <th className="text-center">Ref. Physician</th>
            <th className="text-center">Study Date</th>
            <th className="text-center">Study Time</th>
            <th className="text-center">Status</th>
          </tr>
        </thead>
        <tbody>
        { pacsList.docs && !pacsList.docs.length > 0
            ? <tr><td colSpan="11" className="text-center text-dark" style={{height: "100px", lineHeight:"100px"}}>Search parameters not matching any data. Please try again with different criteria</td></tr>
            : pacsList.docs &&  pacsList.docs.map((pacs, idx) => {
                const myid = pacs._id;
                const dicomLink = Constants.DICOM_VIEWER+"?studyInstanceUID="+pacs.studyInstanceUID[0];
                const redirectLink = Constants.DICOM_VIEWER+"redirect.html?studyInstanceUID="+pacs.studyInstanceUID[0];
               
                return (
                    <tr key={idx} style={{whiteSpace: "nowrap"}}>
                         <td>
                      { pacs.patientName } 
                      </td>
                      <td >
                      { pacs.patientID } 
                      </td>
                      <td >                    
                      { pacs.patientAge } 
                      </td>
                      <td>
                      { pacs.patientSex } 
                     
                      </td>
                      <td>
                      { pacs.bodyPartExamined } 
                      </td>
                      <td>
                      { pacs.modality } 
                      </td>                     
                      <td>
                      { pacs.accessionNumber } 
                      </td>
                      <td>
                      { pacs.referringPhysicianName } 
                      </td>
                      <td>
                      { moment(pacs.studyDate[0]).format("DD-MM-YYYY")}
                      </td>
                      <td>
                      { moment(pacs.studyTime[0]).format("h:mm A")}
                      </td>
                      <td>
                      <Button outline color="primary" size="sm">
                          {
                           
                          <a href={dicomLink} target="_blank" >
                          
                          <i className="fa fa-clock-o fa-lg" aria-hidden="true"> Dicom Viewer</i>                        
                        </a>
                        }
                    </Button>  
                      </td>
                      {/* <td>
                 
                   { pacs.scanFinished &&
                    <div className=""><i className="fa fa-check fa-lg mb-2"></i> Scan Finished</div>
                   }
                  
                   { schedule.imagesPushed &&
                     <div className=""><i className="fa fa-check fa-lg mb-2"></i> Images Pushed</div>
                   }
                    {  schedule.scanFinished && schedule.imagesPushed && !schedule.reportDone && !schedule.reportInProgress &&
                    <div>
                    <Button outline color="warning" size="sm">
                        <Link 
                          to={{
                            pathname: "/prepareReport",
                            query: {
                              scheduleId: schedule._id
                            },
                          }}
                        >
                          <i className="fa fa-clock-o fa-lg" aria-hidden="true"> Prepare Report</i>                        
                        </Link></Button>
                        <Button outline color="primary" size="sm">
                        <a href={dicomLink} target="_blank"
                        >
                          <i className="fa fa-clock-o fa-lg" aria-hidden="true"> Dicom Viewer</i>                        
                        </a>
                    </Button>
                    </div>
                    }
                    {  schedule.scanFinished && schedule.imagesPushed && schedule.reportInProgress &&
                    <div>
                    <Button outline color="primary" size="sm">
                        <Link 
                          to={{
                            pathname: "/reportview",
                            query: {
                              scheduleId: schedule._id
                            },
                          }}
                        >
                          <i className="fa fa-clock-o fa-lg" aria-hidden="true"> Complete Report</i>                        
                        </Link></Button>
                        <Button outline color="primary" size="sm">
                          {
                           
                          <a href={dicomLink} target="_blank" >
                          
                          <i className="fa fa-clock-o fa-lg" aria-hidden="true"> Dicom Viewer</i>                        
                        </a>
                        }
                    </Button>
                        </div>
                    }
                    {  schedule.scanFinished && schedule.imagesPushed && schedule.reportDone &&
                    <div>
                    <Button outline color="primary" size="sm">
                        <Link 
                          to={{
                            pathname: "/reportview",
                            query: {
                              scheduleId: schedule._id
                            },
                          }}
                          
                        >
                          <i className="fa fa-clock-o fa-lg" aria-hidden="true"> View Report</i>                        
                        </Link></Button>
                        <Button outline color="primary" size="sm">
                        <a href={dicomLink} target="_blank"
                        >
                          <i className="fa fa-clock-o fa-lg" aria-hidden="true"> Dicom Viewer</i>                        
                        </a>
                    </Button>
                    </div>
                    }
                      </td> */}

                      </tr>
                      )
            
            })}
        </tbody>
        </Table>
        <PaginationComponent
        totalItems={pacsList.totalDocs}
        pageSize={Constants.PAGE_ITEM_SIZE}
        onSelect={this.handleSelected}
        maxPaginationNumbers={10}
        activePage={pacsList.page}
        />
       
        </div>
    );
    }
}

const mapStateToProps = state => {
    return {
     pacsList: state.pacs.pacsList,
    //  scanfinisedSuccess: state.schedule.scanfinisedSuccess,
    //  imagesPushedSuccess: state.schedule.imagesPushedSuccess,
     };
  };
  
  const mapDispachToProps = dispatch => {
    return {
       pacsListRequest: data =>dispatch({ type: "PACS_LIST_REQUEST", formdata: data }),       
    };
  };
  
  export default connect(
    mapStateToProps,
    mapDispachToProps
  )(PacsList);