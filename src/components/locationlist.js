import React, { Component } from "react";
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Table,
  Badge,
  Alert,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import Editable from "react-x-editable";
import AccessControl from "../utils/accessControl";

class LocationList extends Component {
  constructor(props) {
    super(props);
   
    this.toggleDanger = this.toggleDanger.bind(this);
    this.state = {
      modal: false,
      danger: false,
      delid: ""
    };
  }
componentDidMount() {
  this.props.getLocationListRequest();
}
  componentDidUpdate(prevProps) {
    if (this.props.locationList) {
      // this.props.history.push("/dashboard");
      console.log("Component did update");
    }
  }
  handleChange = e => {
    const formdata = {
      location: e.value,
      id: e.props.uniqid
    };
    //console.log("Formdata" + JSON.stringify(formdata));
    if (e.value == "") {
      alert("Please enter valid location");
      return false;
    }
    this.props.updateLocationRequest(formdata);
  };
  handleDelete = myid => {
    this.props.deleteLocationRequest(myid);
    this.setState({
      danger: !this.state.danger
    });
  };

  toggleDanger = delid => {
    this.setState({
      danger: !this.state.danger,
      delid: delid
    });
  };

  render() {
    const { locationList } = this.props;
   
    return (
    
      <Table responsive striped>
        <thead>
          <tr>
            <th>Location</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {!locationList.length > 0
            ? <tr><td colSpan="2">No Data</td></tr>
            : locationList.map((location, idx) => {
                const myid = location._id;
                return (
                  <tr key={idx}>
                    <td>
                     
                        <Editable
                          value={location.location}
                          name="location"
                          dataType="text"
                          mode="inline"
                          showButtons="true"
                          uniqid={location._id}
                          emptyValueText={location.location}
                          handleSubmit={this.handleChange}
                        />
                     
                    </td>

                    <td>
                      <div className="justify-content-right">
                        <a href onClick={() => this.toggleDanger(myid)}>
                          <i className="fa fa-trash fa-lg mt-1 text-danger" />
                        </a>

                        <Modal
                          isOpen={this.state.danger}
                          toggle={this.toggleDanger}
                          className={"modal-danger " + this.props.className}
                        >
                          <ModalHeader toggle={this.toggleDanger}>
                            Delete Item
                          </ModalHeader>
                          <ModalBody>
                            Do you really want to delete this item?
                          </ModalBody>
                          <ModalFooter>
                            <Button
                              color="danger"
                              onClick={() =>
                                this.handleDelete(this.state.delid)
                              }
                            >
                              Yes
                            </Button>{" "}
                            <Button
                              color="secondary"
                              onClick={() =>
                                this.toggleDanger(this.state.delid)
                              }
                            >
                              No
                            </Button>
                          </ModalFooter>
                        </Modal>
                      </div>
                    </td>
                  </tr>
                );
              })}
        </tbody>
      </Table>
    );
  }
}

const mapStateToProps = state => {
  return {
    locationList: state.location.locationList
  };
};

const mapDispachToProps = dispatch => {
  return {
    getLocationListRequest: () =>
      dispatch({ type: "GET_LOCATIONLIST_REQUEST" }),
    updateLocationRequest: data =>
      dispatch({ type: "UPDATE_LOCATION_REQUEST", formdata: data }),
    deleteLocationRequest: id =>
      dispatch({ type: "DELETE_LOCATION_REQUEST", id: id })
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(LocationList);
