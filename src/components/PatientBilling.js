import React, { Component, Suspense } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import { Row, Col, Form, Input, Button, Table, Badge, Alert, Card, CardHeader,CardBody } from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import moment from "moment";
import AutoCompleteSelect from "../utils/autoCompleteSelect";

const CenterList = React.lazy(() => import("./centerlist"));


class PatientBilling extends Component {
    intervalID = 0;
  constructor(props) {
   
    super(props);
    this.state = {
      patientId: "", 
      selectedModalities:[],
      billingCodeInputs:[],
      totalCharges:0,
      discount:0,
      additionalCharges:0,
  }
  }
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );

  handleBillingCodeChange=(e, index)=>{
   
    const selectedValue = e.target.innerHTML.split("--")[0];
    let billingInputs = Object.assign([], this.state.billingCodeInputs);
    billingInputs[index]["value"] = selectedValue;
     this.setState({
      billingCodeInputs: billingInputs
    });
    const object = Object.fromEntries(
      Object.entries(this.props.filteredBillingCodes).filter(([key, value]) => [selectedValue].includes(key))
    );

        function hasSelectedValue(value) {
        return value[1].billingcode == selectedValue;
        }    
      let filtered = Object.entries(this.props.filteredBillingCodes).filter(hasSelectedValue);
      let price = filtered[0][1].price;
      let totalBillingCodePrice = this.props.totalBillingCodePrice;
      this.props.addBillingCodePrice(price);
      
      let finalAmount = (parseInt(totalBillingCodePrice) + parseInt(price) + parseInt( this.state.additionalCharges )) - parseInt( this.state.discount);
        this.setState({
          totalCharges: finalAmount
        });
  }


  handleChange = e => {
    if(e.target.name == "discount"){
      let finalAmount = this.state.totalCharges - e.target.value;
      this.setState({
        totalCharges: finalAmount
      });
    }
    if(e.target.name == "additionalCharges"){
     let finalAmount = this.state.totalCharges + e.target.value;
      this.setState({
        totalCharges: finalAmount
      });
    }
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  handleModalityChange = e =>{
    console.log(e.target.name);
    let mdalities = this.state.selectedModalities;
    let data = {};

    if(e.target.checked == true){
      console.log("handleModalityChange", e.target.name);
      mdalities.push(e.target.name);
      this.setState({selectedModalities: mdalities});
       data = {
        centerId: localStorage.getItem("center"),
        modalities: mdalities
      }      
    }else{
      const index = mdalities.indexOf(e.target.name);
      if (index > -1) {
        mdalities.splice(index, 1);
      }
       data = {
        centerId: localStorage.getItem("center"),
        modalities: mdalities
      } 
    }
    this.props.getFilteredCodesRequest(data);
    if(this.state.billingCodeInputs.length === 0){
      var randum = Math.random(1,50);
      var newInput = `input-${randum}`;
      let billingInputs = Object.assign([],[{name: newInput, value:""}]);
      // this.setState(prevState => ({ billingCodeInputs: prevState.billingCodeInputs.concat([newInput]) }));
      this.setState({billingCodeInputs: billingInputs});
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    const formdata = {
         
      aadhar: this.state.aadhar,    
      enteredBy: localStorage.getItem("userid")  
    };
    //console.log("Formdata", formdata);
    this.props.addPatientRequest(formdata);
  };
  componentDidMount() {
    const centerId= localStorage.getItem("center");
    this.props.getCenterListRequest(centerId);

    const query = new URLSearchParams(this.props.location.state);
    const patientId = query.get("patid");
   
    
    //console.log("patientId", patientId);
    if(patientId == undefined){
     // this.props.history.push("/#/home");
    }
    const pname = query.get("pname");

    this.props.setPatientinProps(pname);
    this.setState({patientId: patientId, pname: pname});
  }
  
  getbillcodeAmount = billingCode=>{
    const BillingCode = this.props.billingCodes.filter(billcode=>{
      if(billcode.billingcode == billingCode){
        return billcode;
      }
    });
    this.setState({
      totalCharges: BillingCode[0].price
    })
    //console.log("BillingCode", BillingCode);
  }

  addBillingCodeBox = e =>{
    e.preventDefault();
    var randum = Math.random(1,50);
    var newInput = `input-${randum}`;
    this.setState(prevState => ({ billingCodeInputs: prevState.billingCodeInputs.concat([{name: newInput, value: ""}]) }));
   }

   deleteBillingCodeBox = (index, selValue) =>{
    let billingInputs = Object.assign([], this.state.billingCodeInputs);
       billingInputs.splice(index, 1);
    //console.log("billingInputs", billingInputs);
     this.setState({
      billingCodeInputs: billingInputs
    });
     ///////////////////deduct price from /////////////
     const selectedValue = selValue;
     const object = Object.fromEntries(
      Object.entries(this.props.filteredBillingCodes).filter(([key, value]) => [selectedValue].includes(key))
    );

    function hasSelectedValue(value) {
     return value[1].billingcode == selectedValue;
    }    
     // console.log("Filtere Object", Object.entries(this.props.filteredBillingCodes));
      let filtered = Object.entries(this.props.filteredBillingCodes).filter(hasSelectedValue);
    // console.log("filtered", filtered[0][1]);
    if(selectedValue !== ""){
      let price = filtered[0][1].price;
      // console.log("PRICE", price)
      // let totalBillingCodePrice = parseInt( this.props.totalBillingCodePrice) - parseInt(price);
      this.props.deductBillingCodePrice(price);
      let finalAmount = this.state.totalCharges - price;
        this.setState({
          totalCharges: finalAmount
        });
    }
    
   }

   handleDiscountChange = e =>{
    const discount = e.target.value;
       this.setState({
      discount: discount
    });
   }
   handleBlurDiscountChange = e =>{
    const additionalCharges = parseInt(this.state.additionalCharges);
    const discount = parseInt(this.state.discount);
    let totalBillingCodePrice = parseInt( this.props.totalBillingCodePrice)
    let cost = totalBillingCodePrice + additionalCharges - discount;
    this.setState({
      totalCharges: cost,
    });
   }
   handleAdditionalChrgChange = e =>{
    const additionalCharges = parseFloat(e.target.value);
    this.setState({
      additionalCharges: additionalCharges
    });
   }
   handleBlurAdditionaChrg=()=>{
    const additionalCharges = parseInt(this.state.additionalCharges);
    const discount = parseInt(this.state.discount);
    let totalBillingCodePrice = parseInt( this.props.totalBillingCodePrice)
    let cost = parseInt(totalBillingCodePrice) + additionalCharges - discount;
    this.setState({
      totalCharges: cost,
    });
   }
  
   orderWithBill =()=>{
    this.setState({withBill: true});
     if(this.state.selectedModalities.length === 0 || this.state.totalCharges == 0){
         toast.warning("Please fill the form."); 
       return;
     }
     let billingCodes = [];
     this.state.billingCodeInputs.map(code=>{
      billingCodes.push(code.value);
     })
     const formdata = {
      withBill: true,
      modalities: this.state.selectedModalities,
      billingCodes: billingCodes,
      scanAmount: this.props.totalBillingCodePrice,
      discount: this.state.discount,
      additionalCharges: this.state.additionalCharges,
      patientId: this.state.patientId,
      referringDoc: this.state.referringDoc,
      totalCharges: this.state.totalCharges,
      regId: this.props.regId,
      regDispId:this.props.regDispId,
      dispPatId:this.props.dispPatId,
      enteredBy: localStorage.getItem("userid"),
      centerId: localStorage.getItem("center")
     }
     console.log("formdata", formdata);
     this.props.orderBill(formdata);
   }
   orderWithoutBill =()=>{
     this.setState({withBill: false});
    console.log("Order without Bill Submitted")

    if(this.state.selectedModalities.length === 0 || this.state.totalCharges == 0){
       toast.warning("Please fill the form."); 
       return;
     }
     let billingCodes = [];
     this.state.billingCodeInputs.map(code=>{
      billingCodes.push(code.value);
     })
     const formdata = {
      withBill: false,
      modalities: this.state.selectedModalities,
      billingCodes: billingCodes,
      scanAmount: this.props.totalBillingCodePrice,
      discount: this.state.discount,
      additionalCharges: this.state.additionalCharges,
      patientId: this.state.patientId,
      referringDoc: this.state.referringDoc,
      totalCharges: this.state.totalCharges,
      regId: this.props.regId,
      regDispId:this.props.regDispId,
      dispPatId:this.props.dispPatId,
      enteredBy: localStorage.getItem("userid"),
      centerId: localStorage.getItem("center")
     }
     console.log("formdata", formdata)
     this.props.orderBill(formdata);
   }
componentDidUpdate(prevProps, prevState) {
  if(this.props.sendBillSuccess){
    toast.info("Order Placed Successfully");  
        this.props.resetBillOrderSuccessFlag();
        if(this.state.withBill){
           this.intervalID = setInterval(()=>this.props.history.push({
            pathname:"/getPayment",
            state:{
              totalCharges:this.state.totalCharges
             }
           }), 2000 );
        }else{
          this.intervalID = setInterval(()=>this.props.history.push({
            pathname:"/orderList",
            state:{
              totalCharges:this.state.totalCharges
             }
           }), 2000 );
        }
  }
}

  componentWillUnmount(){
    clearInterval(this.intervalID);
    this.props.resetTotalBillingCodePrice();
  }

  render() {
    const { locationList, centerList, filteredBillingCodes} = this.props;
    let modalitiesArray = [];
    if(filteredBillingCodes.length > 0){
       filteredBillingCodes.map(item=>{
        //console.log("item", item);
        modalitiesArray.push({value: item.billingcode, name: item.billingcode+"--"+item.bodypart}) 
      })
    }
    let centerCode = "";
    if(centerList.length > 0 && this.props.regDispId !== null) {
      centerCode = centerList[0].code+"-"+this.props.regDispId;
      console.log("centerCode", centerCode);
    }

    console.log("this.state.billingCodeInputs", this.state.billingCodeInputs)

    return (
      <div className="animated fadeIn">
        <div>
             <h5>Patient: {this.state.pname? this.state.pname: ""}</h5> 
        </div>
          <ToastContainer autoClose={2000} />
          <div class="row">

          <Card style={{width:"500px"}}>
          <CardHeader>Billing Details</CardHeader>
          <CardBody>
            
          <Form onSubmit={this.handleSubmit} autoComplete="off">
         
            <div className="form-group row">
              <label htmlFor="name" className="col-sm-4 col-form-label">Registration Id:</label>
              <div className="col-sm-8">
                <Input type="text" className="form-control" name="regId" value={centerCode} required="required" autoComplete="new-mname" readOnly />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="name" className="col-sm-4 col-form-label">Modality:</label>
              <div className="col-sm-8">
              {
                  centerList.length > 0 && centerList[0].modalities.map((modality, key)=>{
                    //let mod = modalities.modality;
                    // let checked = false;
                  console.log("modality", modality)
                         return( <span className="p-4" key={key}>
                             <Input type="checkbox" name={modality.modality} onChange={(e)=>this.handleModalityChange(e)} value={modality.modality} checked={this.state[modality.modality]} />{modality.modality}
                             </span>)
                        })
                      }
             
              </div>
            </div>
            
              {/* <div className="col-sm-9">
                <Input type="text" className="form-control" name="billingcode" placeholder="Billing Code" onChange={this.handleChange} required="required" autoComplete="new-mname" onBlur={()=>this.getbillcodeAmount(this.state.billingcode)} />
              </div> */}
              {
                             this.state.billingCodeInputs.map((input,index) => {
                              console.log("name", input.name, "value", input.value, "index", index);
                             return (                            
                            <div className="form-group row" key={input.name}>
                            <label htmlFor="name" className="col-sm-4 col-form-label">Billing Code:</label>
                            <AutoCompleteSelect 
                            className="col-sm-6"                          
                            name={input.name}
                            required="true"
                            optionsValues={modalitiesArray} 
                            //defaultValue={input.value}  
                            handleChange ={(e)=>this.handleBillingCodeChange(e, index) } 
                            />
                            {/* <div className="col-sm-2 addMore">+</div> */}
                            {index === 0 ?  <Button className="col-sm-2" color="info" onClick={this.addBillingCodeBox}>
                            <i class="fa fa-plus"></i>
                            </Button> : <Button className="col-sm-2" color="warning" onClick={()=>this.deleteBillingCodeBox(index, input.value)}><i class="fa fa-minus"></i></Button>}
                            </div>                        
                             ) 
                            
                              })}
                        
            <div className="form-group row">
              <label htmlFor="name" className="col-sm-4 col-form-label">Referring Doctor:</label>
              <div className="col-sm-8">
                <Input type="text" className="form-control" name="referringDoc" value={this.state.referringDoc? this.state.referringDoc: ""} required="required" onChange={this.handleChange} autoComplete="new-referringDoc" />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="name" className="col-sm-4 col-form-label">Discount:</label>
              <div className="col-sm-8">
                <Input type="text" className="form-control" name="discount" value={this.state.discount? this.state.discount: ""} onChange={this.handleDiscountChange}  onBlur={this.handleBlurDiscountChange} autoComplete="new-referringDoc" />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="name" className="col-sm-4 col-form-label">Additional Charges:</label>
              <div className="col-sm-8">
                <Input type="text" className="form-control" name="additionalCharges" value={this.state.additionalCharges? this.state.additionalCharges: ""} onChange={this.handleAdditionalChrgChange} onBlur={this.handleBlurAdditionaChrg} autoComplete="new-referringDoc" />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="name" className="col-sm-4 col-form-label">Total:</label>
              <div className="col-sm-8">
                <Input type="text" className="form-control" name="totalCharges" value={this.state.totalCharges? this.state.totalCharges: ""} required="required"  autoComplete="new-referringDoc" />
              </div>
            </div>
           
            <div className="form-group row">
            <label htmlFor="name" className="col-sm-3 col-form-label">{} </label> 
            <Button color="primary" size="sx" className="col-sm-4" onClick={this.orderWithBill}>Order With Bill</Button>
            <Button color="primary" size="sx" className="col-sm-4 ml-1"  onClick={this.orderWithoutBill}>Order Without Bill</Button>
            {/* <Button color="primary" size="sx" className="col-sm-4 ml-1" >SubmittForm</Button> */}
            </div>
           </Form>   
          </CardBody>
          </Card>
</div>
        
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    regId: state.patient.regId,
    regDispId: state.patient.regDispId,
    dispPatId: state.patient.dispPatId,
    // dispBillId: state.registration.dispBillId,
    // dispOrderId: state.registration.dispOrderId,
    centerList: state.center.centerList,
    patientAddSucess: state.patient.patientAddSucess,
    patientAddError: state.patient.patientAddError,
    filteredBillingCodes: state.billingCode.filteredBillingCodes,
    billingCodes: state.billingCode.billingCodes,
    totalBillingCodePrice: state.billingCode.totalBillingCodePrice,
    sendBillSuccess: state.registration.sendBillSuccess,
  };
};

const mapDispachToProps = dispatch => {
  return {
    getFilteredCodesRequest: (data) =>dispatch({ type: "GET_FILTERED_BILLING_CODES_REQUEST", data: data }),   
    setPatientinProps: data =>dispatch({ type: "SET_PATIENT_NAME_REQUEST", pname: data }),
    addPatientRequest: data =>dispatch({ type: "ADD_PATIENT_REQUEST", formdata: data }),
    resetAddPatientSuccessRequest: ()=>dispatch({ type: "RESET_PATIENT_SUCESS_REQUEST"}),
    resetAddPatientFailRequest: ()=>dispatch({ type: "RESET_PATIENT_FAIL_REQUEST"}),
    getCenterListRequest: (data) =>dispatch({ type: "GET_CENTERLIST_REQUEST", data: data }),      
    addBillingCodePrice: (data) =>dispatch({ type: "ADD_BIILING_CODE_PRICE", price: data }),      
    deductBillingCodePrice: (data) =>dispatch({ type: "DEDUCT_BIILING_CODE_PRICE", price: data }),      
    resetTotalBillingCodePrice: (data) =>dispatch({ type: "RESET_BIILING_CODE_PRICE", price: data }),      
    orderBill: (data) =>dispatch({ type: "SEND_ORDER_BILL_REQUEST", formdata: data }),      
    resetBillOrderSuccessFlag: () =>dispatch({ type: "RESET_SEND_ORDER_BILL_REQUEST_SUCCESS_ASYNC" }),      
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(PatientBilling);
