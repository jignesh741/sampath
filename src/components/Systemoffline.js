import React, { Component } from "react";
import { Redirect, Link } from "react-router-dom";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Alert
} from "reactstrap";
import { connect } from "react-redux";
//import logo from "../../../assets/img/logo-login.png";
//import logo from "/favicon.png";

class Systemoffline extends Component {
  
  render() {
    const { LoginRequest, authenticated, errorMessage } = this.props;

    if (this.props.authenticated) return <Redirect to="/center" />;

    return (
      <div className="app flex-row align-items-center loginpage">
        <div className=" fixed-top mt-5px" fixedtop />
        
       
        <Container>
          {/* <Row className="justify-content-center ">
            <Col md="1" sm="12">
            <div className=""><img src={logo} className="rounded-circle" style={{height:"200px", width:"300"}}/></div>
            </Col>
          </Row> */}
          <Row className="justify-content-center ">
            {/* <Col md="1" className="ml-4"> </Col> */}          
            <Col md="5" sm="12">
              <CardGroup>
                <div className="border p-2 w-100" > 
                <Card className=" border bg-white mb-0">
                <CardHeader style={{backgroundColor:"#E4E5E5"}}>
                  <div className="d-flex align-items-center" style={{backgroundColor: "#E4E5E5"}} > 
                    <div className=""><img src="/assets/img/logo.png" className="img-thumbnail rounded-circle" style={{height:"65px"}}/></div>
                    {/* <div className="mt-1"><img src="/assets/img/logo-name.png" className="" style={{height:"30px"}}/></div>  */}
                    <div className="align-baseline ml-1" ><h1 className="ml-1" style={{color:"#2E61A1"}} >Intelli <small style={{fontSize:"20px"}}>PACS</small></h1></div>
                  </div>
                
                </CardHeader>
                  <CardBody>
                  <div ClassName="jumbotron jumbotron-fluid">
                    <div className="container">
                        <h1 className="display-4">Offline</h1>
                        <p className="lead">System is detected you are offline. Kindly connect to the internet.</p>
                    </div>
                    </div>
                  </CardBody>
                </Card>
                </div>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
export default Systemoffline;
