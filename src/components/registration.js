import React, { Component } from "react";
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Table,
  Badge,
  Alert,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import moment from "moment";
import Editable from "react-x-editable";
import AccessControl from "../utils/accessControl";
import { Link } from 'react-router-dom'
import Constants from "../config";
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import PaginationComponent from "react-reactstrap-pagination";
import FilterFieldsPatients from "../utils/filterFieldsPatients";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {Animated} from "react-animated-css";

var localData = localStorage.getItem('userdata');
const userdata = JSON.parse(localData);
const center = null;

class Registration extends Component {
  constructor(props) {
    super(props);
    const centerCode = localStorage.getItem("centerCode");
    const center = localStorage.getItem("center");
    this.state = {
      modal: false,
      danger: false,
      delid: "",
      selectedPage: 1,
      primary: false,
      centerCode: centerCode,
      centerId:center,
    };
  }

  handleSelected=(selectedPage, filterStr = this.state.filterStr)=>{
    this.setState({ selectedPage: selectedPage, filterStr: filterStr }, () => {
      const data = {
       // executive: userdata._id,
       centerId: this.state.centerId,
        page: this.state.selectedPage ? this.state.selectedPage: 1,
        limit: Constants.PAGE_ITEM_SIZE,
        filterStr: this.state.filterStr? this.state.filterStr : {},
      };
      this.props.getRegistrationsRequest(data);
    });
  }
  
componentDidMount() {
 const data = {
  centerId: this.state.centerId,
  filterStr: {}
  }
  this.props.getRegistrationsRequest(data);
}
  componentDidUpdate(prevProps) {
    if (prevProps.registrations !== this.props.registrations) {
      // this.props.history.push("/dashboard");
      let registrationArray = [];
      console.log("Component did update");
      if(this.props.registrations.docs && this.props.registrations.docs.length > 0){
        this.props.registrations.docs.map(registration=>{
          registrationArray.push({id:registration._id, isChecked: false})
        });
      }
     this.setState({
      allIds: registrationArray
     },()=>console.log("allIds", this.state.allIds))
     console.log("registrationArray", registrationArray);
    }

    if(prevProps.confirmed){
      toast.success("Data transfere confirmed successfuly"); 
      this.props.resetConfirmationFlag(); 
    }
  }
  handleChange = e => {
    const formdata = {
      registration: e.value,
      id: e.props.uniqid
    };
    //console.log("Formdata" + JSON.stringify(formdata));
    if (e.value == "") {
      alert("Please enter valid location");
      return false;
    }
    this.props.updateLocationRequest(formdata);
  };
  

  

  render() {
    const { registrations, reportData } = this.props;
   // console.log("registrations", registrations);
    return (
    <div className="p-2 rounded animated fadeIn" >
              <ToastContainer autoClose={2000} />
    <h4>Registration List</h4>
    <div className="text-right mr-2">Total Results: {registrations.totalDocs? registrations.totalDocs: null}</div>

      <div className="bg-secondary d-flex justify-content-between">
      <FilterFieldsPatients handleFilterSelected = {this.handleSelected} />
      </div>
      <Table responsive  className="bg-white border   text-center" >
        <thead className="bg-primary">
          <tr>
            <th className="text-center">Date</th>
            <th className="text-center">Reg.Id</th>
            <th className="text-center">Pat.Id</th>
            <th className="text-center">Patient</th>
            {/* <th className="text-center">DOB</th>
            <th className="text-center">Gender</th>
            <th className="text-center">Email</th>
            <th className="text-center">Mobile</th> */}
            <th className="text-center">Order Id</th>
            <th className="text-center">Bill Id</th>
            <th className="text-center">Payment Status</th>
            <th className="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          { registrations.docs && !registrations.docs.length > 0
            ? <tr><td colSpan="9" className="text-center text-dark" style={{height: "100px", lineHeight:"100px"}}>Search parameters not matching any data. Please try again with different criteria</td></tr>
            : registrations.docs &&  registrations.docs.map((registration, idx) => {
              if(registration.patId == null){
               return;
              }
                const myid = registration._id;
                console.log("registration", registration)
                return (
                  <tr key={idx} style={{whiteSpace: "nowrap"}}>
                    <td style={{width:"160px"}}>
                    { registration.createDate? moment(registration.createDate).format("DD-MM-YYYY h:m A") : ""}
                    </td>
                    <td>
                    { this.state.centerCode+"-"+registration.registration_number } 
                    </td>
                    <td>
                    { this.state.centerCode+"-"+registration.dispPatId } 
                    </td>
                    <td >
                    { registration.patient[0].fname? registration.patient[0].fname : ""} { registration.patient[0].mname? registration.patient[0].mname : ""} { registration.patient[0].lname? registration.patient[0].lname : ""} 
                    </td>
                    {/* <td className="text-center"  style={{width:"150px"}}>{ registration.patId.dob? moment(registration.patId.dob).format("DD-MM-YYYY") : ""}</td>
                    <td className="text-center">{ registration.patId.gender }</td>
                    <td className="text-center">{ registration.patId.email }</td>
                    <td className="text-center">{ registration.patId.mobile }</td> */}
                    <td className="text-center">{ registration.dispOrderId?  this.state.centerCode+"-"+registration.dispOrderId : "" }</td>
                    <td className="text-center">{ registration.dispBillId?  this.state.centerCode+"-"+registration.dispBillId : ""}</td>
                    <td className="text-center">{ registration.paymentStatus?  registration.paymentStatus : ""}</td>
                    <td>
                      {registration.paymentStatus === "Unpaid" && registration.bill.length > 0 && 
                      <div>
                      <Link to={
                          {     
                            pathname:"/getPayment",
                            state:{
                              totalCharges: registration.bill[0].totalCharges,
                              regId: registration._id,
                              billId: registration.billId,
                              regDispId: registration.registration_number,
                              dispPatId: registration.dispPatId,
                              dispBillId: registration.dispBillId,
                             }
                            }
                      }> 
                      <Button size="sm" color="primary">Get Payment</Button>
                      </Link>
                      </div>
                      }
                    </td>
                  </tr>
                );
              })}
        </tbody>
      </Table>
      <PaginationComponent
                  totalItems={registrations.totalDocs}
                  pageSize={Constants.PAGE_ITEM_SIZE}
                  onSelect={this.handleSelected}
                  maxPaginationNumbers={10}
                  activePage={registrations.page}
                  
                />
      <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={"modal-primary " + this.props.className} size="lg">
            <ModalHeader toggle={this.togglePrimary}>Report Data: Imported from Spectrum</ModalHeader>
        <ModalBody>
          {reportData && reportData.map(report =>{
            var keyName = Object.keys(report.result);
            var valueName = Object.values(report.result);
            // console.log("Key Length",keyName.length);
            // console.log("Values Length",valueName.length);
            return (
              <div className="mb-4 bg-light p-2 shadow rounded">
              <div className="border-bottom d-flex justify-content-between align-items-center"> <h4 className="text-center text-primary "> Test : {report.test_name}</h4><small>Date: {moment(report.resultDate).format("DD-MM-YYYY")}</small></div>
              <div className="border-bottom d-flex justify-content-between align-items-center p-2"><strong>Parameter Name</strong><strong>Result Value</strong></div>
              {
                keyName.map((key,i)=>{
                  return ( <div className="d-flex justify-content-between align-items-center p-2 border-bottom" key={i}><div className="min-vw-50">{key.replace(/_/g, ' ')}:</div><div>{valueName[i]}</div></div>)
                })
              }
              </div>
            )
          })}
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={this.togglePrimary}>Close</Button>{' '}
        </ModalFooter>
      </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    registrations: state.registration.registrations,
    };
};

const mapDispachToProps = dispatch => {
  return {
    getRegistrationsRequest: (data) =>
      dispatch({ type: "GET_REGISTRATIONS_REQUEST", data: data }),
 };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(Registration);
