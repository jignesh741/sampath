import React, { Component, Suspense } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import { Row, Col, Form, Input, Button, Table, Badge, Alert } from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";

const LocationList = React.lazy(() => import("./locationlist"));

class Locations extends Component {
  constructor(props) {
    super(props);
  }
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    const formdata = {
      location: this.state.location,
      locationIn:this.state.locationIn,
    };
    console.log("SubmitData=" + JSON.stringify(formdata));
    this.props.addLocationRequest(formdata);
  };
  componentDidUpdate(prevProps) {
    if (this.props.locationList) {
      // this.props.history.push("/dashboard");
      console.log("Component did update");
    }
  }
  alertMessage = alMessage => {
    if (alMessage == "success") {
      return <Alert color="primary">Location Added Successfully!</Alert>;
    }
    if (alMessage == "fail") {
      return (
        <Alert color="danger">Location Already Exist — Please try again!</Alert>
      );
    }
  };
  render() {
    const { locationAddError, locationAddSucess } = this.props;
    return (
      <div className="animated fadeIn">
        <Row>
          <Col md="6">
            <div className="card">
              <div className="card-header">
                <i className="icon-drop" />
                Add Location
              </div>
              <div className="card-body">
                {locationAddError ? this.alertMessage("fail") : null}
                {locationAddSucess ? this.alertMessage("success") : null}
                <Form onSubmit={this.handleSubmit}>
                  <Input
                    type="text"
                    placeholder="Enter Location"
                    autoComplete="Location"
                    name="location"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  <Input
                    type="select"
                    placeholder="Enter State"
                    autoComplete="new-locationIn"
                    name="locationIn"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  >
                    <option value="">Select State</option>
                    <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                    <option value="Andhra Pradesh">Andhra Pradesh</option>
                    <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                    <option value="Assam">Assam</option>
                    <option value="Bihar">Bihar</option>
                    <option value="Chandigarh">Chandigarh</option>
                    <option value="Chhattisgarh">Chhattisgarh</option>
                    <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                    <option value="Daman and Diu">Daman and Diu</option>
                    <option value="Delhi">Delhi</option>
                    <option value="Goa">Goa</option>
                    <option value="Gujarat">Gujarat</option>
                    <option value="Haryana">Haryana</option>
                    <option value="Himachal Pradesh">Himachal Pradesh</option>
                    <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                    <option value="Jharkhand">Jharkhand</option>
                    <option value="Karnataka">Karnataka</option>
                    <option value="Kerala">Kerala</option>
                    <option value="Lakshadweep">Lakshadweep</option>
                    <option value="Madhya Pradesh">Madhya Pradesh</option>
                    <option value="Maharashtra">Maharashtra</option>
                    <option value="Manipur">Manipur</option>
                    <option value="Meghalaya">Meghalaya</option>
                    <option value="Mizoram">Mizoram</option>
                    <option value="Nagaland">Nagaland</option>
                    <option value="Orissa">Orissa</option>
                    <option value="Pondicherry">Pondicherry</option>
                    <option value="Punjab">Punjab</option>
                    <option value="Rajasthan">Rajasthan</option>
                    <option value="Sikkim">Sikkim</option>
                    <option value="Tamil Nadu">Tamil Nadu</option>
                    <option value="Tripura">Tripura</option>
                    <option value="Uttaranchal">Uttaranchal</option>
                    <option value="Uttar Pradesh">Uttar Pradesh</option>
                    <option value="West Bengal">West Bengal</option>
                    </Input>
                  <Row>
                    <Col>
                      <Button color="primary" className="px-4">
                        Add Location
                      </Button>
                    </Col>
                  </Row>
                </Form>
              </div>
            </div>
          </Col>

          <Col md="6">
            <div className="card">
              <div className="card-header">
                <i className="icon-drop" />
                Location Name
              </div>
              <div className="card-body">
                <Suspense fallback={this.loading()}>
                  <LocationList />
                </Suspense>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    locationsAddSucess: state.location.locationsAddSucess,
    locationsAddError: state.location.locationsAddError
  };
};

const mapDispachToProps = dispatch => {
  return {
    addLocationRequest: data =>
      dispatch({ type: "ADD_LOCATION_REQUEST", formdata: data })
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(Locations);
