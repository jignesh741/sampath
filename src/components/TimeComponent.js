import React, { Component } from 'react';
import { Row, Col, Form, Input, Button, Table, Badge, Alert, Card, CardHeader,CardBody } from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import moment from "moment";

class TimeComponent extends Component {
        constructor(props) {
            super(props);
            this.state = {
                date: this.props.date,
                time: this.props.time,
                scan: this.props.scan,
                room: this.props.room,
                modality: this.props.modality,
                selected: this.props.selected,
                reserved: this.props.reserved,
                className: this.props.selected? "rounded timeSelected p-1 text-center mb-2" : this.props.reserved? "rounded timeReserved p-1 text-center mb-2": "rounded timeNormal p-1 text-center mb-2"
            }
        }
componentDidMount() {
    //console.log(" scan: ", this.props.scan)
    //console.log("scanningInfo: ", this.props.scanningInfo)
}
handleSelection = (e) =>{
   console.log("Selected!")
    this.setState({
        selected: !this.state.selected,    
    }, ()=>{
       //////////////check selected count is equal t saved modality count///////////
       let localSavedSlotsItem = localStorage.getItem("selectedSlots");
       
       if(localSavedSlotsItem){
          
        let localSavedSlots = JSON.parse(localStorage.getItem("selectedSlots"));
        let localCount = localSavedSlots.filter(item => item.modality == this.state.modality);
        console.log("localCount", localCount)
        if(localCount.length == this.props.counts[this.state.modality]){
            console.log("Maximum limit reached...");
            alert("Maximum limit reached for this modaliy.")
            return;
        }
       }

       //////////////////////////////////////////////////////////////////////////////
        let data = [...this.props.savedSlots];
        let miximumReached = false;
        this.setState({className: this.state.selected? "rounded timeSelected p-1 text-center mb-2":"rounded timeNormal p-1 text-center mb-2"});
        
        let scanningArr = Object.assign([], this.props.scanningInfo);           
        let scanningInfoStore = this.props.scanningInfo.filter(mod=>mod.modality === this.state.modality );
        let rmIdx = scanningArr.findIndex((item) => item.modality === this.state.modality);
        //console.log("scanningInfoStore[0].scans[0]:", scanningInfoStore[0].scans[0])
        if(scanningInfoStore[0].scans[0] === undefined){
           // console.log("Maximum limit reached!")
           // miximumReached = true;
        }

        if(this.state.selected){
            data.push({date: this.state.date, time: this.state.time, room: this.state.room, modality: this.state.modality, scan: scanningInfoStore[0].scans[0]});            
            scanningArr[rmIdx].scans.splice(0, 1);
            //console.log("scanningArr:", scanningArr);           
            this.props.saveScanningInfoinStore(scanningArr);
        }
        if(!this.state.selected){
        //let indexNum =  data.indexOf({date: this.state.date, time: this.state.time});
        let index = data.findIndex((item) => item.date === this.state.date && item.time === this.state.time && item.modality === this.state.modality);
        data.splice(index, 1);
        //scanningArr[rmIdx].scans.push()
        console.log("To be removed:", index)
        }

        localStorage.setItem('selectedSlots', JSON.stringify(data));
        this.props.saveTimeinStore(data);
        
        })
}

timeSlot =()=>{
    let HTMLSlot=[];
    if(this.state.reserved){
        HTMLSlot.push(<div className={this.state.className} key={"mt"+Math.random()*300}>
        {/* <ToastContainer autoClose={2000} /> */}
        {moment(this.props.time, "HH:mm").format("hh:mm a")}
    </div>  )
    }else{
        HTMLSlot.push(<div className={this.state.className} onClick={this.handleSelection} key={"mst"+Math.random()*300}>
                {/* <ToastContainer autoClose={2000} /> */}
                {moment(this.props.time, "HH:mm").format("hh:mm a")}
        </div>)
    }
   // console.log("HTMLSlot", HTMLSlot)
    return HTMLSlot;
}
    render() {
     // console.log(this.props.selected, this.props.reserved, this.props.time);
     //let finalTimeSlot = 
        return (
            this.timeSlot()
        );
    }
}
const mapStateToProps = state => {
    return {
    savedSlots: state.schedule.savedSlots,
    scanningInfo: state.schedule.scanningInfo,
    // existingSchedules: state.schedule.existingSchedules
    };
  };
  
  const mapDispachToProps = dispatch => {
    return {
        saveTimeinStore: data =>dispatch({ type: "SAVE_SLOT_IN_STORE_REQUEST", data: data }),        
        saveScanningInfoinStore: data =>dispatch({ type: "SAVE_SCANNING_INFO_IN_STORE_REQUEST", data: data }),        
    };
  };
  
  export default connect(
    mapStateToProps,
    mapDispachToProps
  )(TimeComponent);