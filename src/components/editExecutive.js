import React, { Component, Suspense } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import { Row, Col, Form, FormGroup,Input, Button,Label, Table, Badge, Alert, Card, CardHeader,CardBody } from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


const CenterList = React.lazy(() => import("./centerlist"));
//const CenterManagerlist = React.lazy(() => import("./centerManagerlist"));
const ExecutiveList = React.lazy(() => import("./executiveList"));

class EditExecutive extends Component {
  constructor(props) {
    super(props);
    this.state = {center:[]};
  }
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );
 
  handleChange = e => {
    var targetName = e.target.id;  

    if( e.target.checked === true){           
      if(this.state.center.length > 0){
         // var joined = this.state.center.push({id: e.target.id});
          this.setState({ center: [ ...this.state.center,  {id: e.target.id}] , 
            [e.target.id]:e.target.checked,
          });
      }else{          
          this.setState({
          center: [ ...this.state.center,  {id: e.target.id}],
              [e.target.id]:e.target.checked,
          })
      }       
  }
  if( e.target.checked === false){   
      this.setState({      
          [e.target.id]: e.target.checked,
          center: this.state.center.filter(function(fact) { 
            console.log("Matched",e.target.id);
          return fact.id !== e.target.id
      })});
  } 
  if(e.target.name){
    this.setState({
      [e.target.name]:e.target.value,
  })
  }
  
  };

  handleSubmit = e => {
    e.preventDefault();
    if(this.state.center.length == 0){
      alert("Please select at least one center.");
      return false;
    }
    const formdata = {
        id: this.state.id,
        name: this.state.name,
        email: this.state.email,
        designation: this.state.designation,
        mobile: this.state.mobile,
        center: this.state.center,
      
    };
    console.log("formdata",formdata);
   this.props.updateExecutiveRequest(formdata);
  };
  componentDidMount() {
    const query = new URLSearchParams(this.props.location.search);
    const id = query.get("id");
    this.setState({           
        id:id
        });
    this.props.getExecutiveFromId(id);
    this.props.getCenterListRequest();
    
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.executiveData !== this.props.executiveData) {
        this.setState({
            name:this.props.executiveData.name,
            email:this.props.executiveData.email,
            mobile:this.props.executiveData.mobile,
            designation:this.props.executiveData.designation,
        });
        var factories = [];
        
        this.props.executiveData.center.map(fact=>{
           
            if(fact.id){
                console.log("centerID:",fact.id._id);
                factories.push({id: fact.id._id});
                this.setState({
                    [fact.id._id] : true,
                })
            }
            
        })
        
        this.setState({
            center : factories, 
        },()=>console.log("center:",this.state.center))

    }
    if (this.props.executiveUpdateSucess == true) {
        toast.info("Executive Updated Succesfuly");  
        this.props.resetUpdateExecutiveSuccessRequest();
        var j =0;

        this.props.history.push("/executivesCenter");
    }
    if (this.props.executiveUpdateError ) {
        toast.warning("Something Wrong. Please try again.");  
        this.props.resetUpdateExecutiveFailRequest();
    }
  }
  
  render() {
    const { centerList } = this.props;
    return (
      <div className="animated fadeIn">
          <ToastContainer autoClose={2000} />
          <Card>
          <CardHeader>Edit Executive</CardHeader>
          <CardBody>
          <Form onSubmit={this.handleSubmit}>
          <Row>
            <Col md="6">
              <Row>
                  <Col md="12" >
                    <Input
                    type="text"
                    placeholder="Enter Name"
                    autoComplete="Name"
                    name="name"
                    value={this.state.name? this.state.name : ""}
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
                  <Col md="12" >
                  <Input
                    type="text"
                    placeholder="Enter Designation"
                    autoComplete="designation"
                    name="name"
                    value={this.state.designation? this.state.designation : ""}
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  {/* <Input
                    type="select"
                    autoComplete="mobile"
                    name="designation"
                    value={this.state.designation ? this.state.designation : ""}
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  >
                    <option value="">Select Designation</option>
                    <option value="Doer">Doer</option>
                    <option value="Center Manager">Center Manager</option>
                    <option value="General Manager">General Manager</option>
                  </Input> */}
                  </Col>
              
                  <Col md="12" >
                  <Input
                    type="text"
                    placeholder="Enter Mobile Number"
                    autoComplete="mobile"
                    name="mobile"
                    value={this.state.mobile? this.state.mobile : ""}
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
             
                  <Col md="12" >
                  <Input
                    type="text"
                    placeholder="Enter Email "
                    autoComplete="email"
                    name="email"
                    value={this.state.email? this.state.email : ""}
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
                  
                  {/* <Col md="12" >
                  <Input
                    type="password"
                    placeholder="Enter Password "
                    autoComplete="new-password"
                    name="password"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>                   */}
              </Row>
              </Col>
              <Col md="6" className="pl-4">
                <h4>Center</h4>
              {!centerList.length > 0
                  ? ""
                  : centerList.map((center, idx) => {
                      return (
                      <React.Fragment key={idx}>
                        {/* <FormGroup> */}
                         <span className="p-4"> 
                         <Input type="checkbox" id={center._id} onChange={this.handleChange} checked={ this.state[center._id] ? this.state[center._id] : false}  />{center.center}</span>
                        {/* </FormGroup> */}
                      </React.Fragment>
                      );
                  })}
               
              </Col>
            </Row>
            <Row>
            <Col md="12">
                  <Button color="primary" className="px-4 mt-2">
                        Update Executive
                      </Button>
                  </Col>
            </Row>
           </Form>   
          </CardBody>
          </Card>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    centerList: state.center.centerList,
    executiveData: state.executive.executiveData,
    executiveUpdateSucess: state.executive.executiveUpdateSucess,
    executiveUpdateError: state.executive.executiveUpdateError
  };
};

const mapDispachToProps = dispatch => {
  return {
    getExecutiveFromId: (Id) =>dispatch({ type: "GET_EXECUTIVE_REQUEST", Id: Id }),
    updateExecutiveRequest: data =>dispatch({ type: "UPDATE_EXECUTIVE_REQUEST", formdata: data }),
    resetUpdateExecutiveSuccessRequest: ()=>dispatch({ type: "RESET_UPDATE_EXECUTIVE_SUCESS_REQUEST"}),
    resetUpdateExecutiveFailRequest: ()=>dispatch({ type: "RESET_UPDATE_EXECUTIVE_FAIL_REQUEST"}),
    getCenterListRequest: (locationId) =>dispatch({ type: "GET_CENTERLIST_REQUEST", data: locationId }),
      
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(EditExecutive);
