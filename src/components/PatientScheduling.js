import React, { Component } from 'react';
import { Row, Col, Form, Input, Table, Badge, Alert, Card, CardHeader,CardBody } from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import TimeComponent from "./TimeComponent";
import { Container, Button, lightColors, darkColors } from 'react-floating-action-button';

import moment from "moment";

class PatientScheduling extends Component {
    constructor(props) {
        super(props);
        const query = new URLSearchParams(this.props.location.query);
        const orderInfo = JSON.parse(query.get("orderInfo"));
        const scanInfo = orderInfo.scan;
        localStorage.setItem("selectedSlots", []);

        console.log("orderInfo", orderInfo);
        console.log("scanInfo", scanInfo);
        this.state = {
           scanInfo: scanInfo,
           loading: false,
           days:[],
           daysActivate: false,
           initailDayStr: "",
           orderInfo: orderInfo,
           counts:{},
           scanning: orderInfo.scanning,
        }
    }
    componentDidMount() {
        let formdata = {
            center: localStorage.getItem("center"),
            scan: this.state.scanInfo
        };
        this.props.roomListRequest(formdata);
        this.props.setScanningInfo(this.state.scanning);
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.roomList !== prevProps.roomList){
        if(this.props.roomList.roomArryFilter.length > 0){
            let allrooms = [];
            this.props.roomList.roomArryFilter.map(room=>{
                allrooms.push(room.rooms)
            });
            allrooms = allrooms.flat();
            this.setState({
                counts: this.props.roomList.counts,
                allrooms: allrooms
                })
        } }
        if(this.props.scheduleTimeSlotsSuccess == true){
          console.log("Updating..")
          toast.success("Patient Scheduled Successfuly");  
          //this.props.resetPatientSchedule();
         // setTimeout(()=>this.props.resetPatientSchedule(), 3000);
          this.intervalID = setInterval(()=>{this.props.resetPatientSchedule();this.props.history.push("/scheduleList")}, 4000 )

        }
    }

 
  getDays=(room)=>{
      if(room == null && this.props.roomList){
          console.log("Default ROOM:", )
      }
  }

  handleModRoomChange = (e)=>{
      if(e.target.value == ""){
      return;
    }
    console.log("e.target.id:", e.target.id)
    const filterRoom = this.state.allrooms.filter(room=>room.roomName == e.target.value);
   let todayDate = moment().format("YYYY-MM-DD");
   let todayTime = moment().format("HH:mm");
   let startTime = moment(filterRoom[0].startTime, "HH:mm");
   let endTime = moment(filterRoom[0].endTime, "HH:mm").subtract(filterRoom[0].scheduleInterval,"minutes");
     // endTime = moment(filterRoom[0].endTime, "HH:mm").subtract(filterRoom[0].scheduleInterval,"minutes").format("HH:mm");
   let initailDay = moment().format("YYYY-MM-DD");
   let initailDayStr = initailDay +" "+ filterRoom[0].startTime;
   let lunchTime = filterRoom[0].lunchTime.split("-");
   let lunchStart = moment(lunchTime[0],"HH:mm").format("HH:mm");
   let lunchStartObj = moment(lunchTime[0],"HH:mm");
   let lunchEnd = moment(lunchTime[1],"HH:mm").format("HH:mm");
   let lunchEndObj = moment(lunchTime[1],"HH:mm");
   //let initialDayStart = moment(initailDayStr).format("YYYY-MM-DD HH:mm");
   
    let date = moment(todayDate).subtract(1, "day");
    let start = moment(startTime).subtract(filterRoom[0].scheduleInterval,"minutes").format("HH:mm");
    let newDays = [];   
    let dateArray = []; 

    for(let s=0; s <= 5; s++){
      date = moment(date).add(1,"day").format("YYYY-MM-DD");
      let dayname = moment(date).format("dddd");
      console.log("DayName", dayname);
      dateArray[date] = [];
 
      let i = 0;
    do{
      start = moment(start, "HH:mm").add(filterRoom[0].scheduleInterval,"minutes");
      let isReserved = false;
      let isSelected = false;
      if(moment(lunchStart, "HH:mm").format("HH:mm") === moment(start, "HH:mm").format("HH:mm") || (moment(start, "HH:mm").isAfter(lunchStartObj, "HH:mm") && moment(start, "HH:mm").isBefore(lunchEndObj, "HH:mm")) || dayname == filterRoom[0].weeklyHoliday){
        //|| ((moment(start, "HH:mm").isAfter(lunchStart, "HH:mm")) && moment(start, "HH:mm").isBefore(lunchEnd, "HH:mm"))        
        console.log("Found Lunch Time", moment(start, "HH:mm").format("HH:mm"));
        isReserved = true;
        isSelected = false;
      }
      if(todayDate == date && moment(start, "HH:mm").isBefore(moment(), "HH:mm")){
        isReserved = true;
        isSelected = false;
      }
      
        dateArray[date].push({ time: moment(start).format("HH:mm"), isSelected: isSelected, isReserved: isReserved, room: e.target.value})
        i++;
        //console.log("todayTime", moment(start).format("HH:mm"));
    } while(moment(start, "HH:mm").isBefore(endTime, "HH:mm"));

    start = moment(startTime).subtract(filterRoom[0].scheduleInterval,"minutes").format("HH:mm")
    }
    //console.log("dateArray", dateArray);
    this.setState({ 
      days: dateArray, 
      daysActivate: true,
      initailDayStr: initailDayStr, 
      roomSelected:e.target.value, 
      modalitySelected: e.target.id, 
    });
    
  }

getView = (days)=>{
  let i = 0;
  let newView = "";
  let date = moment().format("YYYY-MM-DD");
  date = moment(date).subtract(1, "day").format("YYYY-MM-DD");
  let view = "";
  let viewArray = [];
  
  for(let i = 0 ; i <= 5; i++){
    date = moment(date, "YYYY-MM-DD").add(1,"day").format("YYYY-MM-DD");  
    let dayArray = days[date] ;
    //console.log("dayArray", dayArray);
    let day = [];
    dayArray.map((dayitem,index)=>{
    day.push(<TimeComponent key={"st"+Math.random()*100} 
              selected={dayitem.isSelected}
              reserved={dayitem.isReserved}
              time={dayitem.time}  
              date={date}   
              scan={this.state.orderInfo.scan}   
              counts={this.state.counts}   
              room={this.state.roomSelected} 
              modality={this.state.modalitySelected}  
              {...this.props}
              />)
    })
    viewArray.push(<div className="d-flex flex-column p-2 border" key={"gt"+Math.random()*200}>
        <div className="bg-warning p-2 text-center mb-2">{moment(date).format("DD-MM-YYYY")} <br /> {moment(date).format("dddd")}</div>   
        { day }
        </div>
      )
       //console.log("todayTime", moment(start).format("HH:mm"));
  } 
return viewArray;
}

saveTimeSlots = ()=>{
    let localSavedSlots = JSON.parse(localStorage.getItem("selectedSlots"));
    console.log("localSavedSlots.length:", localSavedSlots.length );
    console.log("localSavedSlots", localSavedSlots);
    console.log("this.state.scanInfo.length", this.state.scanInfo.length);

    if(localSavedSlots.length == 0){
      toast.warning("Please select time slot."); 
      return;
    }
    let i=0;
    if(localSavedSlots.length !== this.state.scanInfo.length){
      toast.warning("Please select time slot for all modalities."); 
      console.log("Updateing", i+1)
      return;
    }
    
    let formdata = {
    schedule: localSavedSlots,
    centerId: this.state.orderInfo.centerId,
    regId: this.state.orderInfo.regId,
    regDispId: this.state.orderInfo.regDispId,
    dispPatId: this.state.orderInfo.dispPatId,
    patientId: this.state.orderInfo.patientId._id,
    dispOrderId: this.state.orderInfo.orderid,
    orderid: this.state.orderInfo._id,
    enteredBy: localStorage.getItem("userid"),
    }
    this.props.savePatientSchedule(formdata);
}

componentWillUnmount(){
  this.props.resetSavedSlots();
  localStorage.setItem("selectedSlots",[]);
  clearInterval(this.intervalID);

}

render() {
    const {roomList} = this.props;
    const {allrooms, days, daysActivate} = this.state;
    console.log("counts:", this.state.counts)
return (
  
    <div  className="animated fadeIn">
    <div className="bg-light-blue p-2">
    <h5>Patient: {this.state.orderInfo.patientId.fname+" "+this.state.orderInfo.patientId.mname+" "+this.state.orderInfo.patientId.lname}</h5>
    <div className="">SCAN: 
    {this.state.orderInfo.scan.map(sc=>{
         return <span className="p-2" key={sc}><i>{sc}</i></span>
       })}
    </div>
    </div>
    <ToastContainer autoClose={2000} key={"main"+Math.random()*100}/>
    <div className="d-md-flex align-items-center justify-content-start bg-white shadow rounded ">
        {
        roomList.roomArryFilter && roomList.roomArryFilter.map((modRooms, index)=>{
          return (
            <div className="p-2" key={index}>
                <label htmlFor="name" >Select {modRooms.modality} Room:</label>
                <Input
                type="select"
                name="modRoom"          
                autoComplete="new-modRoom"
                onChange={this.handleModRoomChange}
                id={modRooms.modality}
                >
                <option value="">Please Select</option>   
                {
                   modRooms.rooms.map(room=>{
                   return <option value={room.roomName} key={room.roomName} >{room.roomName}</option>
                   }) 
                }
                </Input>
            </div>
          )  
        })
        }
       
    </div>
    <div className="animated fadeIn mt-2 bg-white d-flex flex-row" style={{zIndex: "-100"}}>    
      { daysActivate && 
      <>
      {
        this.getView(days)
      } 
     
      <Container className="animated fadeIn">
      <Button
      tooltip="Save Schedule"
      styles={{backgroundColor: darkColors.lighterRed, color: lightColors.white}}
      onClick={this.saveTimeSlots}
      >SAVE</Button>
      </Container>
     
    </>
      }
    </div>  
    </div>
        );
    }
}

const mapStateToProps = state => {
    return {
    roomList: state.schedule.roomList,
    existingSchedules: state.schedule.existingSchedules,
    scheduleTimeSlotsSuccess: state.schedule.scheduleTimeSlotsSuccess,
   // scanningInfo: state.schedule.scanningInfo,
    };
  };
  
  const mapDispachToProps = dispatch => {
    return {
        roomListRequest: data =>dispatch({ type: "MODALITY_ROOM_LIST_REQUEST", formdata: data }),
        getExistingSchedule: data =>dispatch({ type: "GET_EXISTING_SCHEDULE_REQUEST", formdata: data }),
        savePatientSchedule: data =>dispatch({ type: "SAVE_PATIENT_SCHEDULE_REQUEST", formdata: data }),
        setScanningInfo: data =>dispatch({ type: "STORE_SCANNING_INFO", data: data }),
        resetSavedSlots: data =>dispatch({ type: "RESET_SAVED_SLOT", data: data }),
        resetPatientSchedule: () =>dispatch({ type: "RESET_SCHEDULE_SAVE_SUCCESS_ASYNC"}),
    };
  };
  
  export default connect(
    mapStateToProps,
    mapDispachToProps
  )(PatientScheduling);