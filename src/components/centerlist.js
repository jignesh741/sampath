import React, { Component } from "react";
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Table,
  Badge,
  Alert,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import AccessControl from "../utils/accessControl";
import EditableField from "../utils/editablefield";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Link, Redirect } from 'react-router-dom';

class CenterList extends Component {
  constructor(props) {
    super(props);
   
    this.toggleDanger = this.toggleDanger.bind(this);
    this.state = {
      modal: false,
      danger: false,
      delid: ""
    };
  }
componentDidMount() {
  this.props.getCenterListRequest();
  this.props.getModalityListRequest();
}
  componentDidUpdate(prevProps, prevState) {
    
    if(this.props.centerUpdateSucess){
        toast.success("Data updated successfuly");  
        this.props.resetUpdateRequest();      
      }
      if(this.props.centerUpdateError){
        toast.warning("Something Wrong. Please try again.");  
        this.props.resetUpdateEmployeeRequest();
      }
      if(this.props.modalityUpdateSucess){
        toast.success("Modality updated successfuly.");  
        this.props.resetModalityRequest();
      }
  }
  
  handleSubmit = (submitid, name, value) => {
    //  console.log("E Value",submitid)
    const formdata = {
      efield: name,
      evalue: value,
      id: submitid
    };
    //console.log("Formdata" + JSON.stringify(formdata));
    if (value == "") {
      alert("Please enter valid text");
      return false;
    }
    this.props.updateCenterRequest(formdata);
  };
  handleDelete = myid => {
    this.props.deleteCenterRequest(myid);
    this.setState({
      danger: !this.state.danger
    });
  };
  handleCheboxChange = (centerId, modality, e)=>{
    console.log(centerId);
    console.log(e.target.name);
    const data = {
      center: centerId, 
      modality: modality, 
      status: e.target.checked,
    }
    this.props.updateModalityRequest(data);
    this.setState({
      [e.target.name]: e.target.checked
    });
  }
  toggleDanger = delid => {
    this.setState({
      danger: !this.state.danger,
      delid: delid
    });
  };
  manageBillingCodes = centerId =>{
    console.log("Center", centerId);
    this.props.getBillingCodes(centerId);
    this.props.setWorkingCenter(centerId);
     this.props.history.replace("/billingCodes");
  //  console.log("History",this.props)
  // return <Redirect to="/billingCodes" />
   // this.props.history.push({pathname:"/billingCodes" })
  }
  manageModalitySchedule = center =>{
    console.log("Center", center);
    this.props.getBillingCodes(center);
    this.props.setWorkingCenter(center);
     this.props.history.replace("/setModalitySchedule");
  }
  render() {
    const { centerList, modalityList } = this.props;
   
    return (
      <div>
         <ToastContainer autoClose={2000} />
      
      <Table responsive striped>
         
        <thead>
          <tr>
            <th>Center</th>
            <th>Code</th>
            <th>Address</th>
            <th>Modalities</th>
            <th colSpan="2">Settings</th>
          </tr>
        </thead>
        <tbody>
          {!centerList.length > 0
            ? <tr><td colSpan="2">No Data</td></tr>
            : centerList.map((center, idx) => {
                const myid = center._id;
              
                return (
                  <tr key={idx}>
                    <td>
                     
                     <EditableField
                        type="text"
                        name="center"
                        defaultValue={center.center}
                        handleSubmit={this.handleSubmit}
                        submitid={myid}
                    />
                    </td>
                    <td>
                     
                     <EditableField
                        type="text"
                        name="code"
                        defaultValue={center.code? center.code: ""}
                        handleSubmit={this.handleSubmit}
                        submitid={myid}
                    />
                    </td>
                    <td>
                    <EditableField
                        type="text"
                        name="address"
                        defaultValue={center.address}
                        handleSubmit={this.handleSubmit}
                        submitid={myid}
                    />
                    
                    </td>
                    <td>
                      {
                        modalityList.length > 0 && modalityList.map((modality, key)=>{
                          let mod = center._id+modality.modality;
                          let checked = false;
                         // console.log("mod", mod);
                          // if(this.state[mod] == true){
                          //   console.log("Check", mod);
                          //   checked = true;
                          // }
                          center.modalities && center.modalities.map((item)=>{
                           // console.log("Item", item)
                            if(item.modality == modality.modality && item.status == true){
                             // if(this.state[center._id+"CT"])
                             checked = true;
                            }
                             //console.log(item);
                           })
                         return( <span className="p-4" key={key}>
                             <Input type="checkbox" name={center._id+modality.modality} onChange={(e)=>this.handleCheboxChange(center._id, modality.modality, e)} value={modality.modality} checked={checked} />{modality.modality}
                             </span>)
                        })
                      }
                    {/* <span className="p-4"> <Input type="checkbox" name={center._id+"CT"} onChange={(e)=>this.handleCheboxChange(center._id, "CT", e)} value="CT" checked={this.state[center._id+"CT"]? this.state[center._id+"CT"] : CT} />CT</span>
                    <span className="p-4"> <Input type="checkbox" name={center._id+"MR"} onChange={(e)=>this.handleCheboxChange(center._id, "MR", e)} value="MR" />MR</span>
                    <span className="p-4"> <Input type="checkbox" name={center._id+"CR"} onChange={(e)=>this.handleCheboxChange(center._id, "CR", e)} value="CR" />CR</span>
                    <span className="p-4"> <Input type="checkbox" name={center._id+"PX"} onChange={(e)=>this.handleCheboxChange(center._id, "PX", e)} value="PX" />PX</span> */}
                    </td>
                    <td>
                    <Link className="btn btn-primary" role="button"
                     to={{
                       pathname: "/ModalityConfig",
                       query: {
                         center: JSON.stringify(center)
                       },
                     }}
                   >
                    Modality Settings
                   </Link>
                    
                      <button  color="primary" className="ml-2" size="lg"
                              onClick={() =>
                                this.manageBillingCodes(center._id)
                              }>Billing Codes</button> 
                      </td>
                    <td>
                      <div className="justify-content-right">
                        <a  onClick={() => this.toggleDanger(myid)}>
                          <i className="fa fa-trash fa-lg mt-1 text-danger" />
                        </a>

                        <Modal
                          isOpen={this.state.danger}
                          toggle={this.toggleDanger}
                          className={"modal-danger " + this.props.className}
                        >
                          <ModalHeader toggle={this.toggleDanger}>
                            Delete Item
                          </ModalHeader>
                          <ModalBody>
                            Do you really want to delete this item?
                          </ModalBody>
                          <ModalFooter>
                            <Button
                              color="danger"
                              onClick={() =>
                                this.handleDelete(this.state.delid)
                              }
                            >
                              Yes
                            </Button>{" "}
                            <Button
                              color="secondary"
                              onClick={() =>
                                this.toggleDanger(this.state.delid)
                              }
                            >
                              No
                            </Button>
                          </ModalFooter>
                        </Modal>
                      </div>
                    </td>
                  </tr>
                );
              })}
        </tbody>
      </Table>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    centerList: state.center.centerList,
    centerUpdateSucess: state.center.centerUpdateSucess,
    centerUpdateError: state.center.centerUpdateError,
    modalityUpdateSucess: state.center.modalityUpdateSucess,
    modalityList: state.modality.modalityList,
  };
};

const mapDispachToProps = dispatch => {
  return {
    getCenterListRequest: () =>
      dispatch({ type: "GET_CENTERLIST_REQUEST" }),
    updateCenterRequest: data =>
      dispatch({ type: "UPDATE_CENTER_REQUEST", formdata: data }),
    deleteCenterRequest: id =>
      dispatch({ type: "DELETE_CENTER_REQUEST", id: id }),
      resetUpdateRequest:()=>dispatch({ type: "RESET_CENTER_UPDATE_REQUEST" }),
      updateModalityRequest: data =>
      dispatch({ type: "UPDATE_MODALITY_REQUEST", formdata: data }),
     resetModalityRequest: data =>
      dispatch({ type: "RESET_MODALITY_REQUEST", formdata: data }),
      getModalityListRequest: () =>
      dispatch({ type: "GET_MODALITYLIST_REQUEST" }),
      getBillingCodes: (data) =>
      dispatch({ type: "GET_BILLING_CODES_REQUEST", data: data }),
      setWorkingCenter: (data) =>
      dispatch({ type: "GET_WORKING_CENTER_REQUEST", data: data }),
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(CenterList);
