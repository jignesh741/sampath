import React, { Component } from 'react';
import { Row, Col, Form, Input, Button, Table, Badge, Alert, Card, CardHeader, CardBody } from "reactstrap";
import { connect } from "react-redux";
import moment from "moment";
import Constants from "../config";
import PaginationComponent from "react-reactstrap-pagination";
import FilterFieldsPatients from "../utils/filterFieldsPatients";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Animated } from "react-animated-css";
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const content = { "entityMap": {}, "blocks": [{ "key": "637gr", "text": "Initialized from content state.", "type": "unstyled", "depth": 0, "inlineStyleRanges": [], "entityRanges": [], "data": {} }] };

class PrepareReport extends Component {
  constructor(props) {
    super(props);
    const query = new URLSearchParams(this.props.location.query);
    const regId = query.get("regId");
    const htmlContent = "";
    const contentBlock = htmlToDraft(htmlContent || '');
    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      const editorState = EditorState.createWithContent(contentState);
      this.state = {
        editorState,
        regId: regId,
        reporttemplate: "",
      };
    }
  }
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );
  onEditorStateChange = (editorState) => {
    this.setState({
      editorState,
    });
  };

  saveDraft = () => {
    const htmlContent = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()));
    const formdata = {
      reportText: htmlContent,
      regId: this.state.regId
    };
    console.log("formdata", formdata);
    // console.log("this.state.contentState",JSON.stringify(this.state.contentState), this.state.contentState._immutable);
    this.props.saveReportDraft(formdata);
  };

  saveReport = () => {  
    const htmlContent = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()));
    const formdata = {
      reportText: htmlContent,
      regId: this.state.regId
    };
    console.log("formdata", formdata);
    this.props.saveReport(formdata);
  };
  handleChange = e => {
    const getTemplate = this.props.reporttemplates.filter((item) => item._id == e.target.value);
    console.log("templatefromDB", getTemplate[0].reportContent);
    const htmlContent = getTemplate[0].reportContent;
    const contentBlock = htmlToDraft(htmlContent || '');

    if (contentBlock) {
      console.log("contentBlock")
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      const editorState = EditorState.createWithContent(contentState);
      this.setState({
        editorState
      })
     
    }

    //   // const newcontentState = convertFromRaw(getTemplate[0].reportContent);
    //   console.log("templatefromDB", getTemplate);
    //   console.log("ContentDB", getTemplate[0].reportContent);

    //   const template = JSON.parse(getTemplate[0].reportContent);
    //   console.log("template", template);
    //   //const selectedTemplateText = { EditorState: convertFromRaw(JSON.parse(getTemplate[0].reportContent._immutable.currentContent)) };
    //  // const selectedTemplateText =  convertFromRaw(template);
    //   //const contentState = convertFromRaw(selectedTemplateText);
    //  //const selectedTemplateText = EditorState.createWithContent(template);
    //  //this.onContentStateChange(getTemplate[0].reportContent);
    //   this.setState({
    //     //contentState: EditorState.createWithContent(convertFromRaw(JSON.parse(getTemplate[0].reportContent)))   
    //     //contentState: convertFromRaw(JSON.parse(getTemplate[0].reportContent))  
    //     contentState: getTemplate[0].reportContent
    //   },()=>console.log("After Conversion:", this.state.contentState))
    //  // this.onContentStateChange(selectedTemplateText);
    //   //this.setState({ contentState:  selectedTemplateText})

  }
  componentDidMount() {
    console.log("this.state.contentStat:", this.state.contentState)
    // this.props.getCenterListRequest();
    this.props.getReportTemplatesRequest();
  }
  componentDidUpdate(prevProps, prevState) {
    console.log("componentDidUpdate");
    if (this.props.saveDraftReportSuccess == true) {
      toast.info("Draft Saved Succesfuly");
      this.props.resetReportDraftSuccessRequest();
    }
    if (this.props.saveReportSuccess) {
      toast.success("Report Saved Succesfuly");
      this.props.resetReportDraftSuccessRequest();
    }
  }


  render() {
    const { editorState } = this.state;
    const { reporttemplates } = this.props;
    return (
      <div className="animated fadeIn">
        <ToastContainer autoClose={2000} />
        <Card>
          <CardHeader>Prepare Report</CardHeader>
          <CardBody>
            <div className="d-md-flex align-items-center justify-content-start bg-white shadow rounded ">
              <div className="p-2">
                <label htmlFor="name" >Select Report Template:</label>
                <Input
                  type="select"
                  name="reporttemplate"
                  autoComplete="new-reporttemplate"
                  onChange={this.handleChange}
                //value={this.state.reporttemplate}
                >
                  <option value="">Please Select</option>
                  {
                    reporttemplates.map(reporttemplate => {
                      return <option value={reporttemplate._id} key={reporttemplate._id} >{reporttemplate.title}</option>
                    })
                  }
                </Input>
              </div>
            </div>
            <Editor
              editorState={editorState}
              onEditorStateChange={this.onEditorStateChange}
            />

            <Button color="warning" size="sm" className="mb-2 mt-2" onClick={this.saveDraft}>Save as a draft</Button> <Button color="warning" size="sm" className="mb-2 mt-2" onClick={this.saveReport}>Save and Confirm</Button>

          </CardBody>
        </Card>

        {/* <Row>
        
              <Col md="12">
                <div className="card">
                  <div className="card-header">
                    <i className="icon-drop" />
                    User List
                  </div>
                  <div className="card-body">
                    <Suspense fallback={this.loading()}>
                      <Userlist {...this.props} />
                    </Suspense>
                  </div>
                </div>
              </Col>
            </Row> */}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    saveDraftReportSuccess: state.report.saveDraftReportSuccess,
    saveReportSuccess: state.report.saveReportSuccess,
    reporttemplates: state.report.reporttemplates,
  };
};

const mapDispachToProps = dispatch => {
  return {
    saveReportDraft: data => dispatch({ type: "SAVE_REPORT_DRAFT_REQUEST", formdata: data }),
    saveReport: data => dispatch({ type: "SAVE_REPORT_REQUEST", formdata: data }),
    resetReportDraftSuccessRequest: () => dispatch({ type: "RESET_REPORT_DRAFT_REQUEST" }),
    getReportTemplatesRequest: () => dispatch({ type: "REPOR_TEMPLATE_LIST_REQUEST" }),
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(PrepareReport);