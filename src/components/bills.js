import React, { Component } from 'react';
import { 
        Button, 
        Table, 
        Badge, 
        Alert, 
        Card, 
        CardHeader,
        CardBody, 
        Modal,
        ModalHeader,
        ModalBody,
        ModalFooter, } from "reactstrap";
import { connect } from "react-redux";
import moment from "moment";
import Constants from "../config";
import PaginationComponent from "react-reactstrap-pagination";
import FilterFieldsPatients from "../utils/filterFieldsPatients";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {Animated} from "react-animated-css";
import  ReactToPdf from 'react-to-pdf';
import { ToWords } from 'to-words';
const toWords = new ToWords();

const ref = React.createRef();
const options = {
    orientation: 'landscape',
    unit: 'in',
    format: [4,2]
};
class Bills extends Component {
    constructor(props) {
        super(props);
        const centerCode = localStorage.getItem("centerCode");
        this.state={ 
          centerCode: centerCode,
          selectedPage: 1,
          filterStr: {},
          modal: false,
        }

    }
    componentDidMount() {
       const formdata = {
        centerId: localStorage.getItem("center"),
        filterStr: {}
        }
        this.props.billsRequest(formdata);
    }
    
    handleSelected = (selectedPage, filterStr = this.state.filterStr)=>{
      this.setState({ selectedPage: selectedPage, filterStr: filterStr }, () => {
        const data = {
         // executive: userdata._id,
         centerId: this.state.centerId,
          page: this.state.selectedPage ? this.state.selectedPage: 1,
          limit: Constants.PAGE_ITEM_SIZE,
          filterStr: this.state.filterStr? this.state.filterStr : {},
        };
        this.props.billsRequest(data);
      });
    }
    openBillModal =(billDetails)=>{
      console.log("billDetails", billDetails)
      this.setState({
        modal: true,
        billDetails: billDetails,
      })
    }
    toggle =()=>{
      this.setState({
        modal: !this.state.modal
      })
    }
render() {
    const {bills} = this.props;
    return (
        <div className="animated fadeIn">
        <ToastContainer autoClose={2000} />
        <h4>Bills</h4>
        <div className="text-right mr-2">Total Results: {bills.totalDocs? bills.totalDocs: null}</div>
        <div className="bg-secondary d-flex justify-content-between">
        <FilterFieldsPatients handleFilterSelected={this.handleSelected} />
        </div>
        <Table responsive striped  className="bg-white border" >
        <thead className="bg-primary">
          <tr>
            <th className="text-center"  style={{width:"200px"}}>Date</th>
            <th className="text-center">Reg. Id</th>
            <th className="text-center">Bill Id</th>
            <th className="text-center">Patient Id</th>
            <th className="text-center">Patient</th>
            <th className="text-center">Scan</th>
            <th className="text-center">Scan Amount</th>
            <th className="text-center">Discount</th>
            <th className="text-center">Additional Charges</th>
            <th className="text-center">Total Charges</th>
            <th className="text-center">PDF</th>

          </tr>
        </thead>
        <tbody>
        { bills.docs && !bills.docs.length > 0
            ? <tr><td colSpan="11" className="text-center text-dark" style={{height: "100px", lineHeight:"100px"}}>Search parameters not matching any data. Please try again with different criteria</td></tr>
            : bills.docs &&  bills.docs.map((bill, idx) => {
                const myid = bill._id;
                return (
                    <tr key={idx} style={{whiteSpace: "nowrap"}}>
                      <td style={{width:"200px"}}>
                      { moment(bill.billDateTime).format("DD-MM-YYYY h:m A")}
                      </td>
                      <td>
                      { this.state.centerCode+"-"+bill.billid } 
                      </td>
                      <td>
                      { this.state.centerCode+"-"+bill.regDispId } 
                      </td>
                      <td>
                      { this.state.centerCode+"-"+bill.dispPatId } 
                      </td>
                      <td>
                      { bill.patient[0].fname+" "+bill.patient[0].mname+" "+bill.patient[0].lname } 
                      </td>
                      <td>
                      { bill.billingCodes.join(", ")  } 
                      </td>
                      <td className="text-center">
                      { bill.scanAmount } 
                      </td>
                      <td className="text-center">
                      { bill.discount } 
                      </td>
                      <td className="text-center">
                      { bill.additionalCharges } 
                      </td>
                      <td className="text-center">
                      { bill.totalCharges } 
                      </td>
                      <td>
                      <Button onClick={()=>this.openBillModal(bill)}>View Bill</Button>
                      </td>

                      </tr>
                      )
            
            })}
        </tbody>
        </Table>
        <PaginationComponent
        totalItems={bills.totalDocs}
        pageSize={Constants.PAGE_ITEM_SIZE}
        onSelect={this.handleSelected}
        maxPaginationNumbers={10}
        activePage={bills.page}
        />

      
      <Modal isOpen={this.state.modal} toggle={this.toggle} style={{width:"80%"}} size="lg" >
        <ModalHeader toggle={this.toggle}>Billing Details</ModalHeader>
        <ModalBody>
        <ReactToPdf filename="bill.pdf">
        {({toPdf, targetRef}) =>  (
            <div style={{width: "100%", height: 500, background: 'white', marginTop:"10px"}} onClick={toPdf} ref={targetRef}>
            <div className="text-center mt-2"><h1>SAKSHAM HEALTHCARE</h1></div>
            <div className="text-center">Amanora Business Plaza, 4th Floor, Office No. 421, Maharashtra 411028</div>
            <div style={{marginLeft:"20px"}}>
            <div className="d-flex justify-content-between mt-4 ml-2" >
              <div>Patient Name: { this.state.billDetails.patient[0].fname+" "+this.state.billDetails.patient[0].mname+" "+this.state.billDetails.patient[0].lname }  </div>
              <div>Patient ID:  { this.state.centerCode+"-"+this.state.billDetails.patient[0].patientId  }</div>
            </div>
            </div>
            <div className="d-flex justify-content-between mt-4 ml-2" >
              <div style={{marginLeft:"20px"}}>Bill Number: {this.state.centerCode+"-"+this.state.billDetails.billid} </div>
              <div>Date: {moment(this.state.billDetails.billDateTime).format("DD MMM YYYY")}</div>
            </div> 
            <div style={{marginLeft:"20px"}}>
            <Table responsive  className="bg-white border" >
            <thead className=""  style={{backgroundColor: "#53BCE2", color:"white"}}>
              <tr>
                <th className="text-center" >Scan Details</th>
                <th className="text-center">Amount in Rs.</th>
              </tr>
              </thead>
              <tbody className="text-center">
              <tr>
                <td>{ this.state.billDetails.billingCodes.join(", ")  }</td>
                <td>{ this.state.billDetails.scanAmount  }</td>
              </tr>
              {this.state.billDetails.discount > 0 &&
              <tr>
                <td>Discount</td>
                <td>{ this.state.billDetails.discount  }</td>
              </tr>
              }
              {this.state.billDetails.additionalCharges > 0 &&
              <tr>
                <td>Additional Charges</td>
                <td>{ this.state.billDetails.additionalCharges  }</td>
              </tr>
              }
               <tr className="font-weight-bold">
                <td>Total Charges</td>
                <td>{ this.state.billDetails.totalCharges  }</td>
              </tr>
               {/* <tr className="text-left">           
                <td colspan="2">{ toWords.convert(this.state.billDetails.totalCharges, { currency: true })  }</td>
              </tr> */}
              </tbody>
            </Table>
            <div style={{marginLeft:"20px"}}>
            { toWords.convert(this.state.billDetails.totalCharges, { currency: true })  }
            </div>
            {/* <div style={{marginLeft:"20px"}} className="text-center d-flex align-items-end">
             Note: This is an electronically generated bill, hence does not require signature
            </div> */}
            </div>
            </div>
        )}
      </ReactToPdf>
        </ModalBody>
        <ModalFooter  className="d-flex justify-content-between border">           
          <Button color="danger" onClick={this.toggle}>Close</Button>
      </ModalFooter>
      </Modal>   
    </div>
    );
    }
}

const mapStateToProps = state => {
    return {
     bills: state.bill.bills,
    };
  };
  
  const mapDispachToProps = dispatch => {
    return {
        billsRequest: data =>dispatch({ type: "BILLS_REQUEST", formdata: data }),
    };
  };
  
  export default connect(
    mapStateToProps,
    mapDispachToProps
  )(Bills);