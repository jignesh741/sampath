import React, { Component, Suspense } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import { Row, Col, Form, Input, Button, Table, Badge, Alert } from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

class AddBillingCode extends Component {
    intervalID = 1;
  constructor(props) {
    super(props);
    this.state = {location:null}
  }
  loading = () => (
    <div className="animated fadeIn pt-1 text-billingCode">Loading...</div>
  );
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
 
  handleSubmit = e => {
    e.preventDefault();
    const formdata = {
      billingcode: this.state.billingcode,
      discription: this.state.discription,
      bodypart: this.state.bodypart,
      price: this.state.price,
      center: this.props.workingCenter,
      modality: this.state.modality,
      enteredBy: localStorage.getItem("userid")
    };
    console.log("SubmitData=" + JSON.stringify(formdata));
    this.props.addBillingCodeRequest(formdata);
  };
  componentDidMount() {
      console.log("this.props.workingCenter", this.props.workingCenter)
    if(this.props.workingCenter == ""){
       return this.props.history.push("/center");
    }
    this.props.getCenterListRequest(this.props.workingCenter);
  }
  componentDidUpdate(prevProps) {
    
    if (this.props.billingCodeAddSucess == true) {
        this.props.resetAddBillingCodeRequest();
        toast.info("Billing Code Added Succesfuly");  
              console.log("redirecting...", this.props.billingCodeAddSucess );
             this.intervalID = setInterval(()=>this.props.history.push("/billingCodes"), 4000 );
    }
  }
 
  //WARNING! To be deprecated in React v17. Use componentDidUpdate instead.
  componentWillUnmount(){
    clearInterval(this.intervalID);
  }
  render() {
    const { centerList } = this.props;
    let modalities = "";
    if( centerList.length > 0 ){
       modalities =  centerList[0].modalities.map((modality, key)=>{    
            if(modality.status) {
                return( 
                    <option value={modality.modality} key={key}>{modality.modality}</option>
                    )
            }    
         })
        }
    return (
      <div className="animated fadeIn">
            <ToastContainer autoClose={2000} />
        <Row>
          <Col md="6">
            <div className="card">
              <div className="card-header">
                <i className="icon-drop" />
                Add Billing Code
              </div>
              <div className="card-body">
                
                <Form onSubmit={this.handleSubmit}>
                <Input
                    type="select"
                    autoComplete="modality-new"
                    name="modality"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  >
                      <option value="">Please Select Modality</option>
                      {modalities}
                  </Input>
                  <Input
                    type="text"
                    placeholder="Enter Billing Code"
                    autoComplete="BillingCode"
                    name="billingcode"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  <Input
                    type="text"
                    placeholder="Enter Description"
                    autoComplete="discription"
                    name="discription"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  <Input
                    type="text"
                    placeholder="Enter Bodypart"
                    autoComplete="bodypart-new"
                    name="bodypart"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  <Input
                    type="text"
                    placeholder="Enter Price"
                    autoComplete="price-new"
                    name="price"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                 
                  <Row>
                    <Col>
                      <Button color="primary" className="px-4 mt-2">
                        Add Billing Code
                      </Button>
                    </Col>
                  </Row>
                </Form>
              </div>
            </div>
          </Col>
          </Row>         
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    billingCodeAddSucess: state.billingCode.billingCodeAddSucess,
    billingCodesAddError: state.billingCode.billingCodesAddError,
    workingCenter: state.center.workingCenter,
    centerList: state.center.centerList,
  };
};

const mapDispachToProps = dispatch => {
  return {
    addBillingCodeRequest: data =>
      dispatch({ type: "ADD_BILLING_CODE_REQUEST", formdata: data }),
      resetAddBillingCodeRequest: () =>
      dispatch({ type: "RESET_ADD_BILLING_CODE_REQUEST" }),
      getCenterListRequest: (data) =>
      dispatch({ type: "GET_CENTERLIST_REQUEST", data: data }),
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(AddBillingCode);
