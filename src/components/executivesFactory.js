import React, { Component, Suspense } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import { Row, Col, Form, FormGroup,Input, Button,Label, Table, Badge, Alert, Card, CardHeader,CardBody } from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


const FactoryList = React.lazy(() => import("./factorylist"));
const FactoryManagerlist = React.lazy(() => import("./factoryManagerlist"));
const ExecutiveList = React.lazy(() => import("./executiveList"));

class ExecutivesFactory extends Component {
  constructor(props) {
    super(props);
    this.state = {factory:[]};
  }
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );
  handleChange = e => {
    var targetName = e.target.id;  

    if( e.target.checked === true){           
      if(this.state.factory.length > 0){
         // var joined = this.state.factory.push({id: e.target.id});
          this.setState({ factory: [ ...this.state.factory,  {id: e.target.id}] , [e.target.id]:e.target.checked,
          });
      }else{          
          this.setState({
          factory: [ ...this.state.factory,  {id: e.target.id}],
              [e.target.id]:e.target.checked,
          })
      }       
  }
  if( e.target.checked === false){   
      this.setState({      
          [e.target.id]: e.target.checked,
          factory: this.state.factory.filter(function(fact) { 
            console.log("Matched",e.target.id);
          return fact.id !== e.target.id
      })});
  } 
  if(e.target.name){
    this.setState({
      [e.target.name]:e.target.value,
  })
  }
  
  };
//   handleChangeState = e => {
//     this.setState({
//       [e.target.name]: e.target.value
//     },()=> this.props.getLocationListRequest(this.state.locationIn));
//   };
//   handleChangeLocation = e => {
//     this.setState({
//       [e.target.name]: e.target.value
//     },()=> this.props.getFactoryListRequest(this.state.location));
//   };
  handleSubmit = e => {
    e.preventDefault();
    if(this.state.factory.length == 0){
      alert("Please select at least one factory.");
      return false;
    }
    const formdata = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      designation: this.state.designation,
      mobile: this.state.mobile,
      factory: this.state.factory,
      
    };
    console.log("formdata",formdata);
   this.props.addExecutiveRequest(formdata);
  };
  componentDidMount() {
    this.props.getFactoryListRequest();
  }
  componentDidUpdate(prevProps, prevState) {
    console.log("componentDidUpdate");
    if (this.props.executiveAddSucess == true) {
        toast.info("Factory Manager Added Succesfuly");  
        this.props.resetAddExecutiveSuccessRequest();
    }
    if (this.props.executiveAddError ) {
        toast.warning("Something Wrong. Please try again.");  
        this.props.resetAddExecutiveFailRequest();
    }
  }
 
  render() {
    const { locationList, factoryList } = this.props;
    return (
      <div className="animated fadeIn">
          <ToastContainer autoClose={2000} />
          <Card>
          <CardHeader>Add Executive</CardHeader>
          <CardBody>
          <Form onSubmit={this.handleSubmit}>
          <Row>
            <Col md="6">
              <Row>
                  <Col md="12" >
                    <Input
                    type="text"
                    placeholder="Enter Name"
                    autoComplete="Name"
                    name="name"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
                  <Col md="12" >
                  <Input
                    type="select"
                    autoComplete="mobile"
                    name="designation"
                    value={this.state.designation ? this.state.designation : ""}
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  >
                    <option value="">Select Designation</option>
                    <option value="Doer">Doer</option>
                    <option value="Factory Manager">Factory Manager</option>
                    <option value="General Manager">General Manager</option>
                  </Input>
                  </Col>
              
                  <Col md="12" >
                  <Input
                    type="text"
                    placeholder="Enter Mobile Number"
                    autoComplete="mobile"
                    name="mobile"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
             
                  <Col md="12" >
                  <Input
                    type="text"
                    placeholder="Enter Email "
                    autoComplete="email"
                    name="email"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
             
                  
                  <Col md="12" >
                  <Input
                    type="password"
                    placeholder="Enter Password "
                    autoComplete="new-password"
                    name="password"
                    onChange={this.handleChange}
                    className="mb-2"
                    required="required"
                  />
                  </Col>
              
                  
              </Row>
              </Col>
              <Col md="6" className="pl-4">
                <h4>Factory</h4>
              {!factoryList.length > 0
                  ? ""
                  : factoryList.map((factory, idx) => {
                      return (
                      <React.Fragment key={idx}>
                        {/* <FormGroup> */}
                         <span className="p-4"> <Input type="checkbox" id={factory._id} onChange={this.handleChange} checked={ this.state[factory._id] ? this.state[factory._id] : false}  />{factory.factory}</span>
                        {/* </FormGroup> */}
                      </React.Fragment>
                      );
                  })}
               
              </Col>
            </Row>
            <Row>
            <Col md="12">
                  <Button color="primary" className="px-4 mt-2">
                        Add Executive
                      </Button>
                  </Col>
            </Row>
           </Form>   
          </CardBody>
          </Card>

        <Row>
    
          <Col md="12">
            <div className="card">
              <div className="card-header">
                <i className="icon-drop" />
                Executive List
              </div>
              <div className="card-body">
                <Suspense fallback={this.loading()}>
                  <ExecutiveList {...this.props} />
                </Suspense>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    locationList: state.location.locationList,
    factoryList: state.factory.factoryList,
    executiveAddSucess: state.executive.executiveAddSucess,
    executiveAddError: state.executive.executiveAddError
  };
};

const mapDispachToProps = dispatch => {
  return {
    getLocationListRequest: (locationIn) =>dispatch({ type: "GET_LOCATIONLIST_REQUEST", locationIn: locationIn }),
    getFactoryListRequest: (locationId) =>dispatch({ type: "GET_FACTORYLIST_REQUEST", data: locationId }),
    addExecutiveRequest: data =>dispatch({ type: "ADD_EXECUTIVE_REQUEST", formdata: data }),
    resetAddExecutiveSuccessRequest: ()=>dispatch({ type: "RESET_EXECUTIVE_SUCESS_REQUEST"}),
    resetAddExecutiveFailRequest: ()=>dispatch({ type: "RESET_EXECUTIVE_FAIL_REQUEST"}),
      
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(ExecutivesFactory);
