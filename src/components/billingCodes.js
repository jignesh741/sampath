import React, { Component } from "react";
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Table,
  Badge,
  Alert,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";
import { rgbToHex } from "@coreui/coreui/dist/js/coreui-utilities";
import { connect } from "react-redux";
import AccessControl from "../utils/accessControl";
import EditableField from "../utils/editablefield";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

class BillingCodes extends Component {
  constructor(props) {
    super(props);
   
    this.toggleDanger = this.toggleDanger.bind(this);
    this.state = {
      modal: false,
      danger: false,
      delid: ""
    };
  }
componentDidMount() {
   
    console.log("this.props.workingCenter", this.props.workingCenter)
    if(this.props.workingCenter == ""){
        this.props.history.push("/center")
    }
    this.props.getBillingCodes(this.props.workingCenter);
    this.props.getCenterListRequest(this.props.workingCenter);
}
 
  
  handleSubmit = (submitid, name, value) => {
    //  console.log("E Value",submitid)
    const formdata = {
      efield: name,
      evalue: value,
      id: submitid
    };
    //console.log("Formdata" + JSON.stringify(formdata));
    if (value == "") {
      alert("Please enter valid text");
      return false;
    }
    this.props.updateBillingCodeRequest(formdata);
  };
  handleDelete = myid => {
    this.props.deleteBillingCodeRequest(myid);
    this.setState({
      danger: !this.state.danger
    });
  };
   toggleDanger = delid => {
    this.setState({
      danger: !this.state.danger,
      delid: delid
    });
  };
  manageBillingCodes = centerId =>{
    console.log("Center", centerId);
    this.props.getBillingCodes(centerId);
    this.props.history.push("/addBillingCode")
  }
  componentDidUpdate(prevProps, prevState) {
      if(this.props.billingCodeDeleteSucess){
        toast.info("Billing Code Deleted Successfuly");  
        this.props.resetDeleteBillingCodeRequest();
      }
  }
  render() {
    const { billingCodes, modalityList, centerList } = this.props;
    let modalities = "";
    if( centerList.length > 0 ){
       modalities =  centerList[0].modalities.map((modality, key)=>{    
            if(modality.status) {
                return( 
                    <option value={modality.modality} key={key}>{modality.modality}</option>
                    )
            }    
         })
        }
    return (
      <div className="animated fadeIn">
          <Button className="bg-gray text-warning" 
            onClick={() =>
            this.manageBillingCodes()
            }>Add Billing Code</Button> 
         <ToastContainer autoClose={2000} />
      
      <Table responsive striped className="bg-white border">
         
        <thead className="bg-primary">
          <tr>
            <th>Billing Code</th>
            <th>Description</th>
            <th>Body Part</th>
            <th>Modality</th>
            <th>Price</th>          
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {!billingCodes.length > 0
            ? <tr><td colSpan="6" className="text-center">No Data</td></tr>
            : billingCodes.map((billingcode, idx) => {
                const myid = billingcode._id;
              
                return (
                  <tr key={idx}>
                    <td>
                     
                     <EditableField
                        type="text"
                        name="billingcode"
                        defaultValue={billingcode.billingcode}
                        handleSubmit={this.handleSubmit}
                        submitid={myid}
                    />
                    </td>
                    <td>
                    <EditableField
                        type="text"
                        name="discription"
                        defaultValue={billingcode.discription}
                        handleSubmit={this.handleSubmit}
                        submitid={myid}
                    />
                    
                    </td>
                    <td>
                    <EditableField
                        type="text"
                        name="bodypart"
                        defaultValue={billingcode.bodypart}
                        handleSubmit={this.handleSubmit}
                        submitid={myid}
                    />                    
                    </td>
                    <td>
                    <EditableField
                        type="select"
                        name="modality"
                        defaultValue={billingcode.modality}
                        handleSubmit={this.handleSubmit}
                        submitid={myid}
                    >  
                    <option>Please Select Modality</option>
                    { modalities }
                    </EditableField>                  
                    </td>
                    <td>
                    <EditableField
                        type="text"
                        name="price"
                        defaultValue={billingcode.price}
                        handleSubmit={this.handleSubmit}
                        submitid={myid}
                    />                    
                    </td>            
                    <td>
                      <div className="justify-content-right">
                        <a  onClick={() => this.toggleDanger(myid)}>
                          <i className="fa fa-trash fa-lg mt-1 text-danger" />
                        </a>

                        <Modal
                          isOpen={this.state.danger}
                          toggle={this.toggleDanger}
                          className={"modal-danger " + this.props.className}
                        >
                          <ModalHeader toggle={this.toggleDanger}>
                            Delete Item
                          </ModalHeader>
                          <ModalBody>
                            Do you really want to delete this item?
                          </ModalBody>
                          <ModalFooter>
                            <Button
                              color="danger"
                              onClick={() =>
                                this.handleDelete(this.state.delid)
                              }
                            >
                              Yes
                            </Button>{" "}
                            <Button
                              color="secondary"
                              onClick={() =>
                                this.toggleDanger(this.state.delid)
                              }
                            >
                              No
                            </Button>
                          </ModalFooter>
                        </Modal>
                      </div>
                    </td>
                  </tr>
                );
              })}
        </tbody>
      </Table>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    workingCenter: state.center.workingCenter,
    billingCodes: state.billingCode.billingCodes,
    modalityList: state.modality.modalityList,
    centerList: state.center.centerList,
    billingCodeDeleteSucess: state.billingCode.billingCodeDeleteSucess,
  };
};

const mapDispachToProps = dispatch => {
  return {
      getBillingCodes: (data) =>
      dispatch({ type: "GET_BILLING_CODES_REQUEST", data: data }),
      updateBillingCodeRequest: (data) =>
      dispatch({ type: "UPDATE_BILLING_CODE_REQUEST", formdata: data }),
      deleteBillingCodeRequest: (data) =>
      dispatch({ type: "DELETE_BILLING_CODE_REQUEST", id: data }),
      getModalityListRequest: () =>
      dispatch({ type: "GET_MODALITYLIST_REQUEST" }),
      getCenterListRequest: (data) =>
      dispatch({ type: "GET_CENTERLIST_REQUEST", data: data }),
      resetDeleteBillingCodeRequest: () =>
      dispatch({ type: "RESET_DELETE_BILLING_CODE_REQUEST" }),
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(BillingCodes);
