import React, { Component } from 'react';
import { Row, Col, Form, Input, Button, Table, Badge, Alert, Card, CardHeader,CardBody } from "reactstrap";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


class GetPayment extends Component {
    intervalID = 0;
    constructor(props) {
        super(props);
        const centerCode = localStorage.getItem("centerCode")
        this.state = {centerCode: centerCode}
    }
    componentDidMount() {
    const query = new URLSearchParams(this.props.location.state);
   
    const totalCharges = query.get("totalCharges");
    const regId = query.get("regId");    
    const billId = query.get("billId");
    const regDispId = query.get("regDispId");
    const dispPatId = query.get("dispPatId");
    const dispBillId = query.get("dispBillId");
    console.log("query.regId", regId);
    this.setState({
      totalCharges: totalCharges,
      regId: regId,
      billId: billId,
      regDispId: regDispId,
      dispPatId: dispPatId,
      dispBillId: dispBillId,
    })
    }
    handleChange = e =>{
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    modeChange = e=>{
        console.log("Value", e.target.value)
        this.setState({
            mode: e.target.value 
        })
    }
    handleSubmit =(e)=>{
        e.preventDefault();
        const formdata = {
            enteredBy: localStorage.getItem("userid"),
            centerId: localStorage.getItem("center"),
            regId: this.props.regId?  this.props.regId: this.state.regId,
            billId: this.props.billId? this.props.billId: this.state.billId,
            regDispId:this.props.regDispId ? this.props.regDispId: this.state.regDispId,
            dispPatId:this.props.dispPatId ? this.props.dispPatId: this.state.dispPatId,
            dispBillId:this.props.dispBillId ? this.props.dispBillId: this.state.dispBillId,
            totalCharges: this.state.totalCharges,
            receivedAmount: this.state.receivedAmount,
            mode: this.state.mode,
            bankReferenceNum: this.state.bankReferenceNum,
            chequeNum: this.state.chequeNum,
            chequeDate: this.state.chequeDate,
            chequeBank: this.state.chequeBank,
            chequeBranch: this.state.chequeBranch,
            outstanding: parseInt(this.state.totalCharges) - parseInt(this.state.receivedAmount),
        }

        console.log("getPayment formdata:", formdata);
        this.props.submitPayment(formdata);
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.paymentSentSuccess){
            this.props.resetPaymentSuccessFlag();
            toast.info("Payment Submitted Successfully");  
            this.intervalID = setInterval(()=>this.props.history.push({pathname:"/orderList"}), 2000 );
        }
    }
    componentWillUnmount(){
        clearInterval(this.intervalID);
    }

    render() {
        return (
            <div className="animated fadeIn">
                <ToastContainer autoClose={2000} />
                <h3>GET PAYMENT</h3>
        <Card style={{ width: '30rem' }}>
        <CardHeader>Patient: {this.props.pname}</CardHeader>
          <CardBody>
            <Form onSubmit={this.handleSubmit} autoComplete="off">
            <div className="form-group row">
              <label htmlFor="name" className="col-sm-4 col-form-label">Registration Id:</label>
              <div className="col-sm-8">
                <Input type="text" className="form-control" name="regId" value={this.props.regDispId? this.state.centerCode+"-"+this.props.regDispId: this.state.centerCode+"-"+this.state.regDispId} required="required" autoComplete="new-mname" readOnly />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="billId" className="col-sm-4 col-form-label">Bill Id:</label>
              <div className="col-sm-8">
                <Input type="text" className="form-control" name="billId" value={this.props.dispBillId? this.state.centerCode+"-"+this.props.dispBillId: this.state.centerCode+"-"+this.state.dispBillId} required="required" autoComplete="new-billId" readOnly />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="totalCharges" className="col-sm-4 col-form-label">Total Charges:</label>
              <div className="col-sm-8">
                <Input type="text" className="form-control" name="regId" value={this.state.totalCharges? this.state.totalCharges: ""} required="required" autoComplete="new-totalCharges" readOnly />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="receivedAmount" className="col-sm-4 col-form-label">Amount Received:</label>
              <div className="col-sm-8">
                <Input type="number" className="form-control" name="receivedAmount" value={this.state.receivedAmount? this.state.receivedAmount: ""} required="required" autoComplete="new-receivedAmount" onChange={this.handleChange} max={this.state.totalCharges} />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="receivedAmount" className="col-sm-4 col-form-label">Payment Mode:</label>
              <div className="col-sm-8">
                <Input type="select" 
                className="form-control" 
                name="mode" 
                value={this.state.mode? this.state.mode: ""} 
                required="required" 
                autoComplete="new-mode"
                onChange={this.modeChange} >
                    <option value="">Please Select</option>
                    <option value="Cash">Cash</option>
                    <option value="Online">Online</option>
                    <option value="Cheque">Cheque</option>
                </Input>
              </div>
            </div>
            { this.state.mode === "Online" && 
            <div className="form-group row">
              <label htmlFor="bankReferenceNum" className="col-sm-4 col-form-label">Bank Reference No.:</label>
              <div className="col-sm-8">
                <Input type="text" className="form-control" name="bankReferenceNum" value={this.state.bankReferenceNum? this.state.bankReferenceNum: ""} required="required" autoComplete="new-bankReferenceNum" onChange={this.handleChange} />
              </div>
            </div>
            }
            {
               this.state.mode === "Cheque" && 
               <React.Fragment>
               <div className="form-group row">
                 <label htmlFor="chequeNum" className="col-sm-4 col-form-label">Cheque Number:</label>
                 <div className="col-sm-8">
                   <Input type="text" className="form-control" name="chequeNum" value={this.state.chequeNum? this.state.chequeNum: ""} required="required" autoComplete="new-chequeNum" onChange={this.handleChange} />
                 </div>
               </div> 
               <div className="form-group row">
                 <label htmlFor="chequeDate" className="col-sm-4 col-form-label">Cheque Date:</label>
                 <div className="col-sm-8">
                   <Input type="text" className="form-control" name="chequeNum" value={this.state.chequeDate? this.state.chequeDate: ""} required="required" autoComplete="new-chequeDate" onChange={this.handleChange} />
                 </div>
               </div> 
               <div className="form-group row">
                 <label htmlFor="chequeBank" className="col-sm-4 col-form-label">Cheque Bank Name:</label>
                 <div className="col-sm-8">
                   <Input type="text" className="form-control" name="chequeBank" value={this.state.chequeBank? this.state.chequeBank: ""} required="required" autoComplete="new-chequeBank" onChange={this.handleChange} />
                 </div>
               </div> 
               <div className="form-group row">
                 <label htmlFor="chequeBranch" className="col-sm-4 col-form-label">Bank Branch Name:</label>
                 <div className="col-sm-8">
                   <Input type="text" className="form-control" name="chequeBranch" value={this.state.chequeBranch? this.state.chequeBranch: ""} required="required" autoComplete="new-chequeBranch" onChange={this.handleChange} />
                 </div>
               </div> 
               </React.Fragment>
            }
             <div className="form-group row">
            <label htmlFor="name" className="col-sm-4 col-form-label">{} </label> 
            <Button color="primary" size="sx" className="col-sm-4">Submit</Button>
            </div>
            </Form>
          </CardBody>
        </Card>
            </div>
        );
    }
}


const mapStateToProps = state => {
    return {
    pname: state.patient.pname,
    regId: state.patient.regId !== ""? state.patient.regId: state.registration.regId,
    regDispId: state.patient.regDispId !== ""? state.patient.regDispId: state.registration.regDispId,
    dispPatId: state.patient.dispPatId,
    billId: state.registration.billId,
    dispBillId: state.registration.dispBillId,
    dispOrderId: state.registration.dispOrderId,
    paymentSentSuccess: state.payment.paymentSentSuccess,
    };
  };
  
  const mapDispachToProps = dispatch => {
    return {
        submitPayment: data =>dispatch({ type: "SUBMIT_PAYMENT_REQUEST", formdata: data }),
        resetPaymentSuccessFlag: () =>dispatch({ type: "RESET_PAYMENT_REQUEST_SUCCESS_ASYNC" }),
    };
  };
export default connect(
    mapStateToProps,
    mapDispachToProps
  )(GetPayment);
  