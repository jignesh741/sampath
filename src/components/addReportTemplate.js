import React, { Component } from 'react';
import { connect } from "react-redux";
import { Button, Card, CardHeader, CardBody, Input } from "reactstrap";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
export const LOCAL_STORAGE_KEY = 'reportEditor';

class AddReportTemplate extends Component {
    state = {
        editorState: EditorState.createEmpty()
    };

    constructor(props) {
        super(props);
        const htmlContent = localStorage.getItem(LOCAL_STORAGE_KEY);
        const contentBlock = htmlToDraft(htmlContent || '');

        if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            const editorState = EditorState.createWithContent(contentState);
            this.state = {
                editorState,
            };
        }
    }

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
        });
    };
    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };
    saveReport = () => {
        const htmlContent = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()));
       // localStorage.setItem(LOCAL_STORAGE_KEY, htmlContent);
        const formdata = {
            reportContent: htmlContent,
            title: this.state.title
        };
        this.props.saveReportTemplate(formdata);
    };
    componentDidUpdate(prevProps, prevState) {
        console.log("componentDidUpdate");
        if (this.props.saveReportTemplateSuccess == true) {
            toast.info("Template Saved Succesfuly");
            this.props.resetReportTemplateSuccessRequest();
        }

    }
    render() {
        const { editorState } = this.state;
        return (
            <div className="animated fadeIn">
                <ToastContainer autoClose={2000} />
                <Card>
                    <CardHeader> Add Report Template</CardHeader>
                    <CardBody>
                    <div className="form-group row">
                            <label htmlFor="name" className="col-sm-1 col-form-label">Title:</label>
                            <div className="col-sm-5 text-left">
                                <Input type="text" className="form-control" name="title" placeholder="Title" onChange={this.handleChange} required="required" />
                            </div>
                        </div>
                        <Editor
                            editorState={editorState}
                            onEditorStateChange={this.onEditorStateChange}
                        />
                        <Button color="warning" size="sm" className="mb-2 mt-2"
                            onClick={this.saveReport}>Save</Button>
                    </CardBody>
                </Card>
            </div>
        );
    }
}


const mapStateToProps = state => {
    return {
        saveReportTemplateSuccess: state.report.saveReportTemplateSuccess,
    };
};

const mapDispachToProps = dispatch => {
    return {
        saveReportTemplate: data => dispatch({ type: "SAVE_REPORT_TEMPLATE_REQUEST", formdata: data }),
        resetReportTemplateSuccessRequest: () => dispatch({ type: "RESET_REPORT_TEMPLATE_REQUEST" }),
    };
};

export default connect(
    mapStateToProps,
    mapDispachToProps
)(AddReportTemplate);