
const permissions = localStorage.getItem('permissions');
const Permissions = JSON.parse(permissions)

const dam = {
  items:[
    {
      title: true,
      name: "DAM",
      wrapper: {
        element: "",
        attributes: {}
      },
      class: "text-info"
    },
    {
      name: "View DAM",
      url: "/masters/dam/damList",
      icon: "icon-user"
    },
    {
      name: "Add DAM",
      url: "/masters/dam/addDAM",
      icon: "icon-user"
    },
   
  ]
}

const dsr = {
  items:[
    {
      title: true,
      name: "DSR",
      wrapper: {
        element: "",
        attributes: {}
      },
      class: "text-info"
    },
    {
      name: "View DSR",
      url: "/masters/DSRlist",
      icon: "icon-calculator"
    },
    {
      name: "Add DSR",
      url: "/masters/addDSR",
      icon: "icon-calculator"
    },
    {
      name: "Update Status",
      url: "/masters/dsrstatuslist",
      icon: "icon-calculator"
    }]
  }



const HR = {
  items:[
    {
      title: true,
      name: "HR",
      wrapper: {
        element: "",
        attributes: {}
      },
      class: "text-info"
    },
    {
      name: "Employee",
      url: "/masters/addEmployee",
      icon: "icon-user"
    },
    {
      name: "Employee List",
      url: "/masters/employeeList",
      icon: "icon-user"
    }
  ]
}

const dashboard = {
  items:[
    {
      name: "Dashboard",
      url: "/dashboard",
      icon: "icon-speedometer",
      badge: {
        variant: "info",
        text: "NEW"
      }
    }
  ]
}
const masters = {
  items: [    
    {
      title: true,
      name: "Masters",
      wrapper: {
        // optional wrapper object
        element: "", // required valid HTML5 element tag
        attributes: {} // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: "text-info" // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: "Business",
      url: "/masters/business",
      icon: "fa fa-empire"
    },
    {
      name: "Departments",
      url: "/masters/department",
      icon: "fa fa-codepen"
    },
    {
      name: "Designations",
      url: "/masters/designations",
      icon: "fa fa-gg"
    },
    {
      name: "Company",
      url: "/masters/company",
      icon: "fa fa-gg"
    },
    {
      name: "Company Type",
      url: "/masters/companytype",
      icon: "fa fa-gg"
    },
    {
      name: "Category",
      url: "/masters/category",
      icon: "fa fa-gg"
    },
    {
      name: "Meeting Type",
      url: "/masters/meetingtype",
      icon: "fa fa-gg"
    },
    {
      name: "Customers",
      url: "/masters/dam/damcustomer",
      icon: "icon-user"
    },
    {
      name: "Agency",
      url: "/masters/dam/damagency",
      icon: "icon-user"
    }
   
  //   {
  //     title: true,
  //     name: "Components",
  //     wrapper: {
  //       element: "",
  //       attributes: {}
  //     },
  //     class: "text-info"
  //   },
  //   {
  //     name: "Base",
  //     url: "/base",
  //     icon: "icon-puzzle",
  //     children: [
  //       {
  //         name: "Breadcrumbs",
  //         url: "/base/breadcrumbs",
  //         icon: "icon-puzzle"
  //       },
  //       {
  //         name: "Cards",
  //         url: "/base/cards",
  //         icon: "icon-puzzle"
  //       },
  //       {
  //         name: "Carousels",
  //         url: "/base/carousels",
  //         icon: "icon-puzzle"
  //       },
  //       {
  //         name: "Collapses",
  //         url: "/base/collapses",
  //         icon: "icon-puzzle"
  //       },
  //       {
  //         name: "Dropdowns",
  //         url: "/base/dropdowns",
  //         icon: "icon-puzzle"
  //       },
  //       {
  //         name: "Forms",
  //         url: "/base/forms",
  //         icon: "icon-puzzle"
  //       },
  //       {
  //         name: "Jumbotrons",
  //         url: "/base/jumbotrons",
  //         icon: "icon-puzzle"
  //       },
  //       {
  //         name: "List groups",
  //         url: "/base/list-groups",
  //         icon: "icon-puzzle"
  //       },
  //       {
  //         name: "Navs",
  //         url: "/base/navs",
  //         icon: "icon-puzzle"
  //       },
  //       {
  //         name: "Paginations",
  //         url: "/base/paginations",
  //         icon: "icon-puzzle"
  //       },
  //       {
  //         name: "Popovers",
  //         url: "/base/popovers",
  //         icon: "icon-puzzle"
  //       },
  //       {
  //         name: "Progress Bar",
  //         url: "/base/progress-bar",
  //         icon: "icon-puzzle"
  //       },
  //       {
  //         name: "Switches",
  //         url: "/base/switches",
  //         icon: "icon-puzzle"
  //       },
  //       {
  //         name: "Tables",
  //         url: "/base/tables",
  //         icon: "icon-puzzle"
  //       },
  //       {
  //         name: "Tabs",
  //         url: "/base/tabs",
  //         icon: "icon-puzzle"
  //       },
  //       {
  //         name: "Tooltips",
  //         url: "/base/tooltips",
  //         icon: "icon-puzzle"
  //       }
  //     ]
  //   },
  //   {
  //     name: "Buttons",
  //     url: "/buttons",
  //     icon: "icon-cursor",
  //     children: [
  //       {
  //         name: "Buttons",
  //         url: "/buttons/buttons",
  //         icon: "icon-cursor"
  //       },
  //       {
  //         name: "Button dropdowns",
  //         url: "/buttons/button-dropdowns",
  //         icon: "icon-cursor"
  //       },
  //       {
  //         name: "Button groups",
  //         url: "/buttons/button-groups",
  //         icon: "icon-cursor"
  //       },
  //       {
  //         name: "Brand Buttons",
  //         url: "/buttons/brand-buttons",
  //         icon: "icon-cursor"
  //       }
  //     ]
  //   },
  //   {
  //     name: "Charts",
  //     url: "/charts",
  //     icon: "icon-pie-chart"
  //   },
  //   {
  //     name: "Icons",
  //     url: "/icons",
  //     icon: "icon-star",
  //     children: [
  //       {
  //         name: "CoreUI Icons",
  //         url: "/icons/coreui-icons",
  //         icon: "icon-star",
  //         badge: {
  //           variant: "info",
  //           text: "NEW"
  //         }
  //       },
  //       {
  //         name: "Flags",
  //         url: "/icons/flags",
  //         icon: "icon-star"
  //       },
  //       {
  //         name: "Font Awesome",
  //         url: "/icons/font-awesome",
  //         icon: "icon-star",
  //         badge: {
  //           variant: "secondary",
  //           text: "4.7"
  //         }
  //       },
  //       {
  //         name: "Simple Line Icons",
  //         url: "/icons/simple-line-icons",
  //         icon: "icon-star"
  //       }
  //     ]
  //   },
  //   {
  //     name: "Notifications",
  //     url: "/notifications",
  //     icon: "icon-bell",
  //     children: [
  //       {
  //         name: "Alerts",
  //         url: "/notifications/alerts",
  //         icon: "icon-bell"
  //       },
  //       {
  //         name: "Badges",
  //         url: "/notifications/badges",
  //         icon: "icon-bell"
  //       },
  //       {
  //         name: "Modals",
  //         url: "/notifications/modals",
  //         icon: "icon-bell"
  //       }
  //     ]
  //   },
  //   {
  //     name: "Widgets",
  //     url: "/widgets",
  //     icon: "icon-calculator",
  //     badge: {
  //       variant: "info",
  //       text: "NEW"
  //     }
  //   },
  //   {
  //     divider: true
  //   },
  //   {
  //     title: true,
  //     name: "Extras"
  //   },
  //   {
  //     name: "Pages",
  //     url: "/pages",
  //     icon: "icon-star",
  //     children: [
  //       {
  //         name: "Login",
  //         url: "/login",
  //         icon: "icon-star"
  //       },
  //       {
  //         name: "Register",
  //         url: "/register",
  //         icon: "icon-star"
  //       },
  //       {
  //         name: "Error 404",
  //         url: "/404",
  //         icon: "icon-star"
  //       },
  //       {
  //         name: "Error 500",
  //         url: "/500",
  //         icon: "icon-star"
  //       }
  //     ]
  //   },
  //   {
  //     name: "Disabled",
  //     url: "/dashboard",
  //     icon: "icon-ban",
  //     attributes: { disabled: true }
  //   },
  //   {
  //     name: "Download CoreUI",
  //     url: "https://coreui.io/react/",
  //     icon: "icon-cloud-download",
  //     class: "mt-auto",
  //     variant: "success",
  //     attributes: { target: "_blank", rel: "noopener" }
  //   },
  //   {
  //     name: "Try CoreUI PRO",
  //     url: "https://coreui.io/pro/react/",
  //     icon: "icon-layers",
  //     variant: "danger",
  //     attributes: { target: "_blank", rel: "noopener" }
  //   }
   ]
};

let itemArray = {items:[]};
itemArray = Object.assign({},dashboard);
if(Permissions.dsrview){
   itemArray.items = [...itemArray.items, ...dsr.items];  
}
if(Permissions.damview){
 // itemArray = Object.assign({},dashboard);
  itemArray.items = [...itemArray.items, ...dam.items];  
}
if(Permissions.hrview){
  //itemArray = Object.assign({},dashboard);
  itemArray.items = [...itemArray.items, ...HR.items];  
}
if(Permissions.masterenter){
 // itemArray = Object.assign({},dashboard);
  itemArray.items = [...itemArray.items, ...masters.items];  
}
console.log("ItemArray",itemArray);
export default itemArray;