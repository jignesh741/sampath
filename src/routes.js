import React from "react";
import DefaultLayout from "./containers/DefaultLayout";

const Home = React.lazy(() => import("./components/home"));
const Breadcrumbs = React.lazy(() => import("./views/Base/Breadcrumbs"));
const Cards = React.lazy(() => import("./views/Base/Cards"));
const Carousels = React.lazy(() => import("./views/Base/Carousels"));
const Collapses = React.lazy(() => import("./views/Base/Collapses"));
const Dropdowns = React.lazy(() => import("./views/Base/Dropdowns"));
const Forms = React.lazy(() => import("./views/Base/Forms"));
const Jumbotrons = React.lazy(() => import("./views/Base/Jumbotrons"));
const ListGroups = React.lazy(() => import("./views/Base/ListGroups"));
const Navbars = React.lazy(() => import("./views/Base/Navbars"));
const Navs = React.lazy(() => import("./views/Base/Navs"));
const Paginations = React.lazy(() => import("./views/Base/Paginations"));
const Popovers = React.lazy(() => import("./views/Base/Popovers"));
const ProgressBar = React.lazy(() => import("./views/Base/ProgressBar"));
const Switches = React.lazy(() => import("./views/Base/Switches"));
const Tables = React.lazy(() => import("./views/Base/Tables"));
const Tabs = React.lazy(() => import("./views/Base/Tabs"));
const Tooltips = React.lazy(() => import("./views/Base/Tooltips"));
const BrandButtons = React.lazy(() => import("./views/Buttons/BrandButtons"));
const ButtonDropdowns = React.lazy(() =>
  import("./views/Buttons/ButtonDropdowns")
);
const ButtonGroups = React.lazy(() => import("./views/Buttons/ButtonGroups"));
const Buttons = React.lazy(() => import("./views/Buttons/Buttons"));
const Charts = React.lazy(() => import("./views/Charts"));
const Dashboard = React.lazy(() => import("./views/Dashboard"));
const CoreUIIcons = React.lazy(() => import("./views/Icons/CoreUIIcons"));
//const Flags = React.lazy(() => import("./views/Icons/Flags"));
const FontAwesome = React.lazy(() => import("./views/Icons/FontAwesome"));
const SimpleLineIcons = React.lazy(() =>
  import("./views/Icons/SimpleLineIcons")
);
const Alerts = React.lazy(() => import("./views/Notifications/Alerts"));
const Badges = React.lazy(() => import("./views/Notifications/Badges"));
const Modals = React.lazy(() => import("./views/Notifications/Modals"));
const Colors = React.lazy(() => import("./views/Theme/Colors"));
const Typography = React.lazy(() => import("./views/Theme/Typography"));
const Widgets = React.lazy(() => import("./views/Widgets/Widgets"));
//const Users = React.lazy(() => import("./views/Users/Users"));
//const User = React.lazy(() => import("./views/Users/User"));

const Locations = React.lazy(() => import("./components/locations"));

const Center = React.lazy(() => import("./components/center"));
const Executives = React.lazy(() => import("./components/executives"));
const EditUser = React.lazy(() => import("./components/editUser"));
const Users = React.lazy(() => import("./components/users"));
const Userlist = React.lazy(() => import("./components/userlist"));

const PatientList = React.lazy(() => import("./components/patientlist"));
const SearchPatientCounts = React.lazy(() => import("./components/searchPatientCounts"));
const Regmenu = React.lazy(() => import("./components/Regmenu"));
const PatientRegistration = React.lazy(() => import("./components/PatientRegistration"));
const PatientBilling = React.lazy(() => import("./components/PatientBilling"));
const DirectRegBilling = React.lazy(() => import("./components/directRegBilling"));
const Modality = React.lazy(() => import("./components/Modality"));
const BillingCodes = React.lazy(() => import("./components/billingCodes"));
const AddBillingCode = React.lazy(() => import("./components/addBillingCode"));
const Registration = React.lazy(() => import("./components/registration"));
const GetPayment = React.lazy(() => import("./components/getPayment"));
const OrderList = React.lazy(() => import("./components/orderList"));
const Bills = React.lazy(() => import("./components/bills"));
const Payments = React.lazy(() => import("./components/payments"));
const SetModalitySchedule = React.lazy(() => import("./components/setModalitySchedule"));
const ModalityConfig = React.lazy(() => import("./components/ModalityConfig"));
const Scheduling = React.lazy(() => import("./components/Scheduling"));
const PatientScheduling = React.lazy(() => import("./components/PatientScheduling"));
const ScheduleList = React.lazy(() => import("./components/scheduleList"));
const ScanList = React.lazy(() => import("./components/scanList"));
const RadiologistWorkList = React.lazy(() => import("./components/radiologistWorkList"));
const TimeComponent = React.lazy(() => import("./components/TimeComponent"));
const Systemoffline = React.lazy(() => import("./components/Systemoffline"));
const PrepareReport = React.lazy(() => import("./components/prepareReport"));
const Reports = React.lazy(() => import("./components/reports"));
const ReportTemplate = React.lazy(() => import("./components/reportTemplate"));
const PacsList = React.lazy(() => import("./components/pacsList"));
const ReportEditor = React.lazy(() => import("./components/reportEditor"));
const AddReportTemplate = React.lazy(() => import("./components/addReportTemplate"));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: "/", exact: true, name: "Home", component: DefaultLayout },
  { path: "/home", exact: true, name: "Main Page", component: Home },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  { path: "/theme", exact: true, name: "Theme", component: Colors },
  { path: "/theme/colors", name: "Colors", component: Colors },
  { path: "/theme/typography", name: "Typography", component: Typography },
  { path: "/base", exact: true, name: "Base", component: Cards },
  { path: "/base/cards", name: "Cards", component: Cards },
  { path: "/base/forms", name: "Forms", component: Forms },
  { path: "/base/switches", name: "Switches", component: Switches },
  { path: "/base/tables", name: "Tables", component: Tables },
  { path: "/base/tabs", name: "Tabs", component: Tabs },
  { path: "/base/breadcrumbs", name: "Breadcrumbs", component: Breadcrumbs },
  { path: "/base/carousels", name: "Carousel", component: Carousels },
  { path: "/base/collapses", name: "Collapse", component: Collapses },
  { path: "/base/dropdowns", name: "Dropdowns", component: Dropdowns },
  { path: "/base/jumbotrons", name: "Jumbotrons", component: Jumbotrons },
  { path: "/base/list-groups", name: "List Groups", component: ListGroups },
  { path: "/base/navbars", name: "Navbars", component: Navbars },
  { path: "/base/navs", name: "Navs", component: Navs },
  { path: "/base/paginations", name: "Paginations", component: Paginations },
  { path: "/base/popovers", name: "Popovers", component: Popovers },
  { path: "/base/progress-bar", name: "Progress Bar", component: ProgressBar },
  { path: "/base/tooltips", name: "Tooltips", component: Tooltips },
  { path: "/buttons", exact: true, name: "Buttons", component: Buttons },
  { path: "/buttons/buttons", name: "Buttons", component: Buttons },
  {
    path: "/buttons/button-dropdowns",
    name: "Button Dropdowns",
    component: ButtonDropdowns
  },
  {
    path: "/buttons/button-groups",
    name: "Button Groups",
    component: ButtonGroups
  },
  {
    path: "/buttons/brand-buttons",
    name: "Brand Buttons",
    component: BrandButtons
  },
  { path: "/icons", exact: true, name: "Icons", component: CoreUIIcons },
  { path: "/icons/coreui-icons", name: "CoreUI Icons", component: CoreUIIcons },
  // { path: "/icons/flags", name: "Flags", component: Flags },
  { path: "/icons/font-awesome", name: "Font Awesome", component: FontAwesome },
  {
    path: "/icons/simple-line-icons",
    name: "Simple Line Icons",
    component: SimpleLineIcons
  },
  {
    path: "/notifications",
    exact: true,
    name: "Notifications",
    component: Alerts
  },
  { path: "/notifications/alerts", name: "Alerts", component: Alerts },
  { path: "/notifications/badges", name: "Badges", component: Badges },
  { path: "/notifications/modals", name: "Modals", component: Modals },
  { path: "/widgets", name: "Widgets", component: Widgets },
  { path: "/charts", name: "Charts", component: Charts },
  // { path: "/users", exact: true, name: "Users", component: Users },
  // { path: "/users/:id", exact: true, name: "User Details", component: User },
  
  {
    path: "/locations",
    exact: true,
    name: "Locations",
    component: Locations
  },
  {
    path: "/center",
    exact: true,
    name: "Center",
    component: Center
  },
  
  {
    path: "/users",
    exact: true,
    name: "Users",
    component: Users
  },
  {
    path: "/userlist",
    exact: true,
    name: "User List",
    component: Userlist
  },
  
  {
    path: "/editUser",
    exact: true,
    name: "Edit User",
    component: EditUser
  },
  
  {
    path: "/patientlist",
    exact: true,
    name: "Patient Master",
    component: PatientList
  },
  {
    path: "/searchPatientCounts",
    exact: true,
    name: "Patient Counts from Medical History",
    component: SearchPatientCounts
  },
  {
    path: "/Regmenu",
    exact: true,
    name: "Registration Menu",
    component: Regmenu
  },
  {
    path: "/PatientRegistration",
    exact: true,
    name: "Patient Registration",
    component: PatientRegistration
  },
  {
    path: "/PatientBilling",
    exact: true,
    name: "Patient Billing",
    component: PatientBilling
  },
  {
    path: "/directRegBilling",
    exact: true,
    name: "Patient Billing",
    component: DirectRegBilling
  },
  {
    path: "/Modality",
    exact: true,
    name: "Modality",
    component: Modality
  },
  {
    path: "/BillingCodes",
    exact: true,
    name: "Billing Codes",
    component: BillingCodes
  },
  {
    path: "/addBillingCode",
    exact: true,
    name: "Add Billing Code",
    component: AddBillingCode
  },
  {
    path: "/Registration",
    exact: true,
    name: "Registration",
    component: Registration
  },
  {
    path: "/getPayment",
    exact: true,
    name: "Get Payment",
    component: GetPayment
  },
  {
    path: "/orderList",
    exact: true,
    name: "Order List",
    component: OrderList
  },
  {
    path: "/bills",
    exact: true,
    name: "Bills",
    component: Bills
  },
  {
    path: "/payments",
    exact: true,
    name: "Payments",
    component: Payments
  },
  {
    path: "/setModalitySchedule",
    exact: true,
    name: "Set Modality Schedule",
    component: SetModalitySchedule
  },
  {
    path: "/ModalityConfig",
    exact: true,
    name: "Modality Configuration",
    component: ModalityConfig
  },
  {
    path: "/Scheduling",
    exact: true,
    name: "Scheduling",
    component: Scheduling
  },
  {
    path: "/PatientScheduling",
    exact: true,
    name: "Patient Scheduling",
    component: PatientScheduling
  },
  {
    path: "/scheduleList",
    exact: true,
    name: "Schedule List",
    component: ScheduleList
  },
  {
    path: "/ScanList",
    exact: true,
    name: "Scan List",
    component: ScanList
  },
  {
    path: "/TimeComponent",
    exact: true,
    name: "TimeComponent Work List",
    component: TimeComponent
  },
  {
    path: "/Systemoffline",
    exact: true,
    name: "System is offlien",
    component: Systemoffline
  },
  {
    path: "/RadiologistWorkList",
    exact: true,
    name: "Radiologist Work List",
    component: RadiologistWorkList
  },
  {
    path: "/pacsList",
    exact: true,
    name: "PACS List",
    component: PacsList
  },
  {
    path: "/PrepareReport",
    exact: true,
    name: "Prepare Report",
    component: PrepareReport
  },
  {
    path: "/pacsList",
    exact: true,
    name: "PACS List",
    component: PacsList
  },
  {
    path: "/reports",
    exact: true,
    name: "Reports",
    component: Reports
  },
  {
    path: "/reportTemplate",
    exact: true,
    name: "Report Template",
    component: ReportTemplate
  },
  {
    path: "/reportEditor",
    exact: true,
    name: "Report Editor",
    component: ReportEditor
  },
  {
    path: "/addReportTemplate",
    exact: true,
    name: "Add Report Template",
    component: AddReportTemplate
  },
];

export default routes;
