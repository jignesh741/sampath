import React from "react";
import { connect } from "react-redux";

export const NavObjects = (props) => {
  
const roles = localStorage.getItem("roles");
let dashboard = {};
let adminMenu = {};
let techMenu = {};
let techMenuList = {};
let finaldashboard = {};
let finaldashboardItems = [];

if(roles == "frontdesk" || roles == "administrator" ){
  dashboard = {
    items:[
      {
        title: true,
        name: 'Menu',
        wrapper: {            
          element: '',       
          attributes: {}        
        },
        class: 'bg-primary'      
      },
       
      {
        name: "New Registration ",
        url: "/Regmenu",
        icon: "fa fa-empire"
      },
      {
        name: "Registration List ",
        url: "/registration",
        icon: "fa fa-empire"
      },
      {
        name: "Order List ",
        url: "/orderList",
        icon: "fa fa-empire"
      },
      {
        name: "Schedule List ",
        url: "/scheduleList",
        icon: "fa fa-empire"
      },
      {
        name: "Patient Master ",
        url: "/patientList",
        icon: "fa fa-empire"
      },
      {
        name: "Bills",
        url: "/bills",
        icon: "fa fa-empire"
      },
      {
        name: "Payments",
        url: "/payments",
        icon: "fa fa-empire"
      },
      // {
      //   name: "Patient Report",
      //   url: "/reportList",
      //   icon: "fa fa-empire"
      // },
      
      // {
      //   name: "Scheduling",
      //   url: "/scheduling",
      //   icon: "fa fa-empire"
      // },
    ]
}

}

if(roles == "technician" || roles == "administrator" ){
  techMenu = {
    items:[
      {
        title: true,
        name: 'Technician Menu',
        wrapper: {            
          element: '',       
          attributes: {}        
        },
        class: 'bg-primary'      
      },
      {
        name: "Order List ",
        url: "/orderList",
        icon: "fa fa-empire"
      },
      {
        name: "Scan List",
        url: "/scanList",
        icon: "fa fa-empire"
      },
      
    ]
}

}
if(roles == "radiologist" || roles == "administrator" ){
  techMenu = {
    items:[
      {
        title: true,
        name: 'Reporting',
        wrapper: {            
          element: '',       
          attributes: {}        
        },
        class: 'bg-primary'      
      },
      {
        name: "Scan List",
        url: "/scanList",
        icon: "fa fa-empire"
      },
      {
        name: "PACS List",
        url: "/pacsList",
        //url: "http://164.52.206.6:8080/dcm4chee-web3/",
        icon: "fa fa-empire"
      },
      {
        name: "Reports",
        url: "/reports",
        icon: "fa fa-empire"
      },
      {
        name: "Add Report Template",
        url: "/addReportTemplate",
        icon: "icon-speedometer",      
      }, 
      
    ]
}

}



if(roles == "administrator"){
  adminMenu={ 
   items:[
    {
      title: true,
      name: 'Masters',
      wrapper: {            
        element: '',       
        attributes: {}        
      },
      class: 'bg-primary'      
    },
    {
      name: "Modality",
      url: "/Modality",
      icon: "icon-speedometer",
      badge: {
        variant: "info",
        text: ""
      }
    }, 
    // {
    //   name: "Set Modality Schedule",
    //   url: "/setModalitySchedule",
    //   icon: "icon-speedometer",
    //   badge: {
    //     variant: "info",
    //     text: ""
    //   }
    // }, 
    {
      name: "Add Template",
      url: "/reportTemplate",
      icon: "icon-speedometer",      
    }, 
  ]}
}
    
  if(dashboard.items !== undefined){
     finaldashboard.items = [...dashboard.items]
  }
  if(techMenu.items !== undefined){
     finaldashboard.items = [...techMenu.items]
  }
  if(adminMenu.items !== undefined){
    finaldashboard.items = [...dashboard.items,...techMenu.items, ...adminMenu.items]
  }
  return finaldashboard;
};

// const mapStateToProps = state => {
//   return {
//     userPermissions: state.auth.userpermissions,
//   };
// };

